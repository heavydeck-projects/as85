#ifndef __PARSER_EXTERNS_HXX
#define __PARSER_EXTERNS_HXX

#include <i8085.tab.h>
#include <i8085.yy.h>

#include <map>
#include <memory>
#include <ast/ast.hxx>

/* --- Symbols defined on parser / lexer, used elsewhere --- */

/// @brief BYACC parser entry point.
/// @return 0 on success.
///
/// This function launches the parser. Before calling this, the function
/// i8085_parser_reset() must be called with an open file descriptor. If
/// parsing succeds, the resulting tree root will be available at
/// i8085_ast_root. 
int i8085_yyparse();

/// @brief Prepares lexer & parser to parse a new file.
/// @param f A file descriptor ready to read from.
void i8085_parser_reset(FILE* f);

/// @brief get the parser version string
std::string i8085_parser_version();

/* --- Symbols used by parser / lexer, defined elsewhere --- */

/// @brief Map is used as an object pool during parsing.
///
/// After a successful parsing, this pool must be empty; After a failed parsing
/// it may contain some objects which should be disposed calling clear().
/// Object pool is also cleared when calling i8085_parser_reset().
///
/// @note This object is defined on i8085.y.
extern std::map<void*, std::shared_ptr<as85::ast::Ast>> i8085_parser_nodes;

/// @brief After a successful parse, this reference holds the tree root.
///
/// Calling i8085_parser_reset() wil clear this reference so you may wanna save
/// it somewhere else before parsing a new file.
///
/// @note This object is used by the parser but **MUST** be defined elsewhere.
extern std::shared_ptr<as85::ast::Ast> i8085_ast_root;

#endif
