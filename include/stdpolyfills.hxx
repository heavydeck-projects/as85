#ifndef __STD_POLYFILLS
#define __STD_POLYFILLS

/// @file stdpolyfills.hxx
/// @brief Provide a way to replace C++ standard classes with Boost.
///
/// Some C++ standard feature support is bad, especially for certain libraries
/// like std::filesystem. Boost provides a more or less drop-in replacement for
/// them but ifdefs are a pain. so we have them all together here.
///
/// For using any available version, boost or C++, use the `STD` namespace, for
/// example `STD::filesystem` for using the C++ version use the regular `std`
/// namespace, for example `std::string`.

#ifndef USE_BOOST
#include <filesystem>
#define STD std
#else
#include <boost/filesystem.hpp>
#define STD boost
#endif

#endif
