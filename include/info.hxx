#ifndef __INFO_HXX
#define __INFO_HXX

//Some functions to gather the build info and some
//platform-dependant information

#include <string>

#ifndef AS85_VERSION_MAJOR
#define AS85_VERSION_MAJOR 0
#endif

#ifndef AS85_VERSION_MINOR
#define AS85_VERSION_MINOR 0
#endif

#ifndef AS85_VERSION_PATCH
#define AS85_VERSION_PATCH 0
#endif

namespace as85{

std::string version_as85();
std::string version_yacc();

}

#endif
