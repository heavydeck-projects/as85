#ifndef __AS85_HXX
#define __AS85_HXX

#include <ostream>
#include <memory>
#include <exception>

#include <vector>
#include <string>
#include <stdpolyfills.hxx>

/// All the project classes must lie under this namespace.
namespace as85{
    // Some helper classes for the whole of the assembler

    /// @brief Base class for all of this project specific exceptions.
    class As85Exception : public std::exception {
        public:
        As85Exception(const std::string &message = "", const std::string &file = "", int line = -1);
        virtual const char* what() const noexcept;

        protected:
        std::string message;
        std::string file;
        int line;
        /// @brief Returns a location string to append to the message.
        /// If location data is not provided, an empty string is returned. 
        std::string location_str() const;
    };

    enum ret_value{
        /// @brief Function returned with success.
        RET_OK = 0,
        /// @brief Function returned with and error (unknown or undefined).
        RET_NOK = 1,
    };

    ///@brief Verbosity levels for logging.
    enum verbosity {
        /// @brief Same as V_QUIET.
        V_MIN   = 0,
        /// @brief No mesages will be shown on this level.
        V_QUIET = 0,

        /// @brief Severe errors that terminate the assembly process.
        V_CRITICAL,
        /// @brief Warning, assembly will try to continue.
        V_WARNING,
        /// @brief Extra information, not required for debugging.
        V_INFO,
        /// @brief Extra debugging information.
        V_DEBUG,
        /// @brief Extensive internal information, rarely needed.
        V_TRACE,

        /// @brief All messages will be shown on this level.
        V_MAX
    };
    void operator++(verbosity &v);
    void operator--(verbosity &v);

    /// @brief Program configuration resulting from argument parsing.
    class Config
    {
        public:
        /// @brief True by default on Debug builds.
        static const bool debug;
        /// @brief List of input assembly files
        std::vector<STD::filesystem::path> input_files;
        /// @brief Output file path
        STD::filesystem::path out_file;
        /// @brief Verbosity level @see verbosity
        verbosity verbose_level;

        static std::shared_ptr<Config> getMutableInstance();
        static std::shared_ptr<const Config> getInstance();

        private:
        Config();
        static std::shared_ptr<Config> instance;
    };

    //Some exported functions

    //Logging
    
    /// @brief Provides an output stream for logging .
    ///
    /// If the current log level is above or equal to log_level, it will
    /// return a logging stream, by default it will be std::cerr. If current
    /// log level is below log_level, it will return a dummy stream as-if
    /// you were writting to /dev/null.
    /// @param log_level logging level.
    /// @return A writeable stream for logging.
    std::ostream& log(verbosity log_level);
    /// @brief Same as log(V_CRITICAL)
    std::ostream& critical();
    /// @brief Same as log(V_WARNING)
    std::ostream& warning();
    /// @brief Same as log(V_INFO)
    std::ostream& info();
    /// @brief Same as log(V_DEBUG)
    std::ostream& debug();
    /// @brief Same as log(V_TRACE)
    std::ostream& trace();

    ///Inner main
    int as85_main();
};

#endif
