#ifndef __REGISTER_HXX
#define __REGISTER_HXX

#include "ast.hxx"

namespace as85 {
namespace ast {

/// @brief Class for any register to derive from
class Register : public AstValue {
    public:
    Register(const YYSTYPE* token);
    virtual uint8_t getMask() const = 0;
    virtual std::string getRegisterName() const = 0;
    virtual std::string toString() const = 0;
};

/// @brief Class for 8-bit register syntax rule.
class Register8 : public Register{
    public:
    Register8(YYSTYPE* token);
    virtual std::int32_t getValue() const;
    virtual std::string toString() const;
    virtual uint8_t getMask() const;
    virtual std::string getRegisterName() const;

    private:
    uint8_t reg;
};

/// @brief Class for 16-bit register syntax rule.
class Register16 : public Register{
    public:
    Register16(YYSTYPE* token);
    virtual std::int32_t getValue() const;
    virtual std::string toString() const;
    virtual uint8_t getMask() const;
    virtual std::string getRegisterName() const;

    private:
    uint8_t reg;
};

} //ast
} //as85

#endif
