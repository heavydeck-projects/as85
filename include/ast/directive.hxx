#ifndef __AST_DIRECTIVE_HXX
#define __AST_DIRECTIVE_HXX

#include <ast/ast.hxx>
#include <sstream>

namespace as85{
namespace ast{

class String;

/// @brief Base class for all assembler directives.
///
/// Assembler directives are instructions given to the assembler itself
/// ans _will not_ translate to bytes on the program.
class Directive : public Ast {
    public:
    Directive();
    Directive(const YYSTYPE* token);
};


/// @brief Forces alignment of the following instruction.
///
/// Forces alignment to integer multiples of the given argument.
class Align : public Directive {
    public:
    Align(YYSTYPE* token);
    void setAlignment(const std::shared_ptr<Ast> node);
    void setAlignment(const std::shared_ptr<AstValue> node);
    virtual std::string toString() const;

    private:
    void setAlignment(const Ast *node);
    void setAlignment(const AstValue *node);
    int32_t alignment;
};

/// @brief Forces the address of the following instruction.
class Absolute : public Directive {
    public:
    Absolute(YYSTYPE* token);
    virtual std::string toString() const;
    void setAddress(const std::shared_ptr<Ast>node);
    void setAddress(const std::shared_ptr<AstValue>node);

    private:
    void setAddress(const Ast *node);
    void setAddress(const AstValue *node);
    int32_t address;
};

/// @brief Switches assembly process to a given memory section.
class Section : public Directive {
    public:
    Section(YYSTYPE* token);
    Section(YYSTYPE* token, const String &name);
    virtual std::string toString() const;

    private:
    std::string section_name;
};

/// @brief Makes symbol available on linker.
class Export : public Directive {
    public:
    Export(YYSTYPE* token);
    Export(YYSTYPE* token, const Label &l);
    virtual std::string toString() const;

    private:
    std::string symbol;
};

/// @brief Expect symbol to be provided by linker.
class Extern : public Directive {
    public:
    Extern(YYSTYPE* token);
    Extern(YYSTYPE* token, const Label &l);
    virtual std::string toString() const;

    private:
    std::string symbol;
};

}
}

#endif
