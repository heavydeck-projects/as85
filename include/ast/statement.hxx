#ifndef __STATEMENT_HXX
#define __STATEMENT_HXX

#include "ast.hxx"

namespace as85{
namespace ast {

// --- Ast classes ---

/// @brief Class for statement syntax rule.
class Statement : public Ast{
    public:
    Statement();
    virtual std::string toString() const;
};

} //ast
} //as85

#endif
