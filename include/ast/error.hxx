#ifndef __ERROR_HXX
#define __ERROR_HXX

#include "ast.hxx"
#include "visitor/visitor.hxx"

namespace as85{
namespace ast{

enum ErrorId {
    ///Default error message.
    ERRID_UNDEF,

    /* --- Big errors, whole programs or statements got dropped out --- */
    /// Detected unsupported UTF16/32 encoding.
    ERRID_BAD_ENCODING,
    /// Bad statement; Not even partially recognized.
    ERRID_BAD_STATEMENT,

    /* --- Expectations subverted! An statement has incorrect arguments --- */

    /// Expected an IDENTIFIER token.
    ERRID_EXPECTED_IDENTIFIER,
    /// Expected an immediate or label.
    ERRID_EXPECTED_VALUE,
    /// Expected a value or identifier
    ERRID_EXPECTED_VALUE_OR_ID,
    /// Expected an value list.
    ERRID_EXPECTED_VALUE_LIST,
    /// Expected A/B/C/D/E/H/L or MEM
    ERRID_EXPECTED_REG8,
    /// Expected BC/DE/HL/SP
    ERRID_EXPECTED_REG16,
    /// Expected BC/DE/HL/AF
    ERRID_EXPECTED_REG_STACK,
    /// Expected a string literal
    ERRID_EXPECTED_STRING,
    /// Expected a string literal for a section name
    ERRID_EXPECTED_SECTION_NAME,
    /// Expected an RST vector
    ERRID_EXPECTED_RST_VECTOR,
    /// Expected a word size [1-4]
    ERRID_EXPECTED_WORD_SIZE
};

/// @brief An error node.
///
/// This node type shall be used to annotate parsing errors as they happen
/// during the parsing process. Instantiated on productions using `error`
/// terminals.
class Error : public AstAnnotation {
    public:
    Error(std::string text = "[ERROR]");
    Error(YYSTYPE *token, ErrorId id = ErrorId::ERRID_UNDEF);
    virtual std::string toString() const;

    static std::string getErrorString(ErrorId id);

    private:
    std::string token_text;
};

}//ast
}//as85

#endif
