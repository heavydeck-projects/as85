#ifndef __OPCODE_HXX
#define __OPCODE_HXX

#include "ast.hxx"
#include <string>
#include <vector>
#include <cstdint>
#include <map>

#include "bytes.hxx"
#include "register.hxx"

namespace as85{
namespace ast{

/// @brief Byte code that comes from actual Opcodes
class Opcode : public Bytes {
    public:
    Opcode();
    Opcode(YYSTYPE* token);
    virtual std::string toString() const;
    virtual std::vector<std::uint8_t> getBytes() const;

    protected:
    static const std::map<const int, const std::string> opcode_name;
    static const std::map<const int, const std::uint8_t> opcode_byte;
    static const std::map<const int, const std::uint8_t> opcode_arg_mask;

    private:
    int token_value;

    //Allow test class to access privates
    friend class TestClass_Opcode;
};

/// @brief RST is a bit special, has its own opcode class.
class OpRST : public Opcode {
    public:
    OpRST(YYSTYPE* token, std::uint8_t rst_vector);
    virtual std::string toString() const;
    virtual std::vector<std::uint8_t> getBytes() const;

    private:
    std::uint8_t rst_vector;
};

/// @brief Adds a register argument to an opcode.
class OpcodeRegisterDecorator : public Opcode {
    public:
    OpcodeRegisterDecorator(std::shared_ptr<Opcode> inner_op, std::shared_ptr<Register> reg, int bit_position);
    virtual std::string toString() const;
    virtual std::vector<std::uint8_t> getBytes() const;
    virtual Position getPosition() const;

    static const int MOV_SRC = 0;
    static const int MOV_DST = 3;
    static const int ALU_SRC = 0;
    static const int ALU_DST = 3;
    static const int LS_16BIT = 4;
    static const int PUSH_POP = 4;

    private:
    OpcodeRegisterDecorator(Opcode* inner_op, Register* reg, int bit_position);
    std::shared_ptr<Register> reg_node;
    std::shared_ptr<Opcode> opcode_node;
    std::uint8_t bit_position;

    void inner_constructor();
};

}//ast
}//as85

#endif
