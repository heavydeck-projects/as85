#ifndef __AST_EXCEPTION_CXX
#define __AST_EXCEPTION_CXX

#include <as85.hxx>

//Forward declaration of BYACC type YYSTYPE.
//Type definition can be found at i8085.tab.hxx
union YYSTYPE;

namespace as85{
namespace ast{

class Ast;

/// @brief Exception class for critcal errors during AST processing.
class AstException : public As85Exception {
    public:
    AstException(const YYSTYPE* token, const std::string &message, const std::string &file = "", int line = -1);
    AstException(const Ast& node, const std::string &message, const std::string &file = "", int line = -1);
};

}
}

#endif
