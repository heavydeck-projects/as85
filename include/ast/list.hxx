#ifndef __AST_LIST_HXX
#define __AST_LIST_HXX

#include "ast.hxx"

namespace as85{
namespace ast {

// --- Ast classes ---

/// @brief Base class for all list-like syntax productions.
class AstList : public Ast {
    public:
    AstList();
    virtual std::string toString() const;
};

/// @brief Class for the statement_list syntax rule.
class StatementList : public AstList {
    public:
    StatementList();
    virtual std::string toString() const;
};

}
}

#endif