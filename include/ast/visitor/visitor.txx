/***
 * @file visitor.tpp
 * @brief Implementation of template methods declared on visitor.hxx
 */
#ifndef __VISITOR_TPP
#define __VISITOR_TPP

//--------------------------------------
//For intelisense highlighting.
//This does nothing in real compilation.
#ifndef __VISITOR_HXX
#include "visitor.hxx"
#endif
//--------------------------------------

namespace as85{
namespace ast{
namespace visitor{

template <class T>
CollectNodes<T>::CollectNodes(bool extract_nodes) : 
        Visitor(),
        extract_nodes(extract_nodes),
        shallow(false)
{

}

template <class T>
std::vector<std::shared_ptr<Ast>> CollectNodes<T>::getNodes() const
{
    return this->nodes;
}

template <class T>
CollectNodes<T>& CollectNodes<T>::setExtract(bool v)
{
    extract_nodes = v; return *this;
}

template<class T>
CollectNodes<T>& CollectNodes<T>::setShallow(bool v)
{
    shallow = v;
    return *this;
}

template <class T>
void CollectNodes<T>::visit(Ast& ast){
    const T* node = dynamic_cast<const T*>(&ast);
    if(node){
        //Collect error
        nodes.push_back(ast.getSelfRef().lock());
        //Delete errors from AST, if required
        if(this->extract_nodes){
            ast.extract();
        }
        if(!shallow){
            for (auto a : ast.getChildren()){
                a->accept(*this);
            }
        }
    }
    //Traverse tree if not a shallow collection
    else
    {
        for (auto a : ast.getChildren()){
            a->accept(*this);
        }
    }
}

} //visitor
} //ast
} //as85

#endif
