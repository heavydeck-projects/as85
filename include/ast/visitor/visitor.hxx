#ifndef __VISITOR_HXX
#define __VISITOR_HXX

#include <ast/ast.hxx>
#include <ast/list.hxx>

#include <vector>

namespace as85{
namespace ast {
/// @brief Visitors for the Ast.
namespace visitor{

/// @brief Base class for AST visitors.
///
/// Implements the visitor pattern for mtraversing and manipulating the
/// AST. Visitors are expected to use the slower RTTI and dynamic_cast inside
/// of their visit() method instead of pure static double-dispatch.
/// [Get a refresher on Visitor here](https://refactoring.guru/design-patterns/visitor).
/// Pure virtual class, its subclasses must implement the visit() method.
class Visitor {
public:
    Visitor();
    virtual ~Visitor();
    
    virtual void visit(Ast& ast) = 0;
};

/// @brief Base class for AST visitors that do not modify the AST.
///
/// Pure virtual class, its subclasses must implement the visit() method.
class ConstVisitor{
public:
    ConstVisitor();
    virtual ~ConstVisitor();
    virtual void visit(const Ast& ast) = 0;
};

/// dumps an AST onto a graphviz document.
class AstGraphviz : public ConstVisitor{
    public:
    AstGraphviz();
    virtual ~AstGraphviz();
    virtual void visit(const Ast& ast);
    void writeGraphviz(std::string path) const;

    void setNegative(bool negative = true);
    void enablePosition(bool show_lines = true);

    private:
    bool is_negative;
    bool show_lines;
    std::vector<std::string> edges;
    std::vector<std::string> nodes;
    std::vector<std::string> leaves;
    std::string node_name(const Ast &node) const;
    std::string format_node(const Ast &node) const;
    std::string format_link(const Ast &from, const Ast &to) const;
};

///@brief Traverses the AST setting every node childs' parent pointer.
class AstParentSetter : public Visitor{
    public:
    AstParentSetter();
    virtual ~AstParentSetter();
    virtual void visit(Ast& ast);
};

///@brief Check the correctness of self & parent pointers.
class AstIntegrity : public Visitor{
    public:
    AstIntegrity();
    virtual ~AstIntegrity();
    virtual void visit(Ast& ast);
    bool isOk() const;

    private:
    bool all_ok;
};

/// @brief Visitor for finding and exctracting nodes from the Ast by their type
/// @tparam T Type of the node to search for
///
/// This class is used to search for nodes of type T on the AST and optionally
/// extract them from the AST. Error and Warning collection is its main use
/// case.
template <class T>
class CollectNodes : public Visitor {
    private:
    std::vector<std::shared_ptr<Ast>> nodes;
    bool extract_nodes;
    bool shallow;

    public:
    CollectNodes(bool extract_nodes = false);
    /// @brief Returns all nodes found.
    std::vector<std::shared_ptr<Ast>> getNodes() const;

    //Builder methods
    /// @brief Extracts the nodes from the AST during traverse.
    CollectNodes<T>& setExtract(bool v = true);
    /// @brief When finding a node of type T, don't traverse down that node.
    CollectNodes<T>& setShallow(bool v = true);

    //Visitor implementation
    virtual void visit(Ast& ast);
};

/// @brief Removes empty statements from the syntax tree.
class EmptyStatementCleaner : public Visitor {
    public:
    EmptyStatementCleaner();
    virtual void visit(Ast &ast);
};

/// @brief Flattens parse tree recursive lists into a single list node.
class ListFlattener : public Visitor {
    public:
    ListFlattener();
    virtual void visit(Ast &ast);
    void setShallow(bool shallow = true);

    private:
    bool shallow;
    void flatten(ast::AstList& ast);
};

}//visitor
}//ast
}//as85

//Implementation of templates.
#include "visitor.txx"

#endif
