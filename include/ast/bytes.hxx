#ifndef __BYTES_HXX
#define __BYTES_HXX

#include <string>
#include <vector>
#include <ast/ast.hxx>
#include <ast/list.hxx>
#include <cstdint>

namespace as85{
namespace ast {

/// @brief  Stuff will translate into bytes.
///
/// Anything that translates to bytes shall inherit from this class. This
/// class may be given children and the result of getBytes() will be the
/// result of appending the bytes of all its children which MUST be of
/// Bytes class.
class Bytes : public Ast {
    public:
    Bytes();
    Bytes(const YYSTYPE* token);
    virtual std::string toString() const;
    virtual std::vector<std::uint8_t> getBytes() const;
};

/// @brief Parse string literals and output their bytes.
class String : public Bytes {
    public:

    enum StringType{
        STR_DEFAULT,
        STR_NULL_TERMINATED,
        STR_UNFORMATTED
    };

    String();
    String(YYSTYPE* token, StringType type = StringType::STR_DEFAULT);
    virtual std::string toString() const;
    virtual std::vector<std::uint8_t> getBytes() const;
    std::string getString() const;

    private:
    void parseString();
    std::string string_literal;
    std::vector<std::uint8_t> string_bytes;
    StringType string_type;
};

/// @brief Takes an AstValue and outputs bytes on the given format.
class Word : public Bytes {
    public:
    enum WordFormat{
        FMT_LITTLE  = 0, /* <- Default */
        FMT_BIG
    };

    Word(int word_bits = 8, int format = WordFormat::FMT_LITTLE);
    virtual std::string toString() const;
    virtual std::vector<std::uint8_t> getBytes() const;
    static std::vector<std::uint8_t> getBytes(std::int32_t value, int word_bits = 8, int format = WordFormat::FMT_LITTLE);

    private:
    int format;
    int word_bits;
};

/// @brief Takes a list of astvalues, and formats them on a given format
class WordArray : public Bytes {
    public:
    WordArray(int word_bits = 8, int format = Word::WordFormat::FMT_LITTLE);
    virtual std::string toString() const;
    virtual std::vector<std::uint8_t> getBytes() const;

    private:
    int format;
    int word_bits;
};

}//ast
}//as85

#endif
