#ifndef __WARNING_HXX
#define __WARNING_HXX

#include "ast.hxx"
#include "visitor/visitor.hxx"

namespace as85{
namespace ast{

enum WarningId{
    /// @brief An integer value has been truncated
    WRNID_PRECISSION_LOSS,
    /// @brief MOV MEM, MEM results in the HLT instruction
    WRNID_MOV_IS_HLT,

    /// @brief Default warning message.
    WRNID_UNDEF
};

/// @brief A warning node.
///
/// This node type shall be used to annotate warnings as they happen
/// during the parsing process or any other AST operation.
class Warning : public AstAnnotation {
    public:
    Warning(std::string text = "[WARNING]");
    Warning(WarningId id = WarningId::WRNID_UNDEF);
    Warning(YYSTYPE *token, WarningId id = WarningId::WRNID_UNDEF);
    virtual std::string toString() const;

    static std::string getWarningString(WarningId id);

    private:
    std::string token_text;
};

}
}

#endif
