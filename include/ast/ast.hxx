#ifndef __AST_HXX
#define __AST_HXX

#include <string>
#include <memory>
#include <vector>

//Forward declaration of BYACC type YYSTYPE.
//Type definition can be found at i8085.tab.hxx
union YYSTYPE;

//For acessing YYSTYPE
#include <cstddef>
#include <cstdint>


namespace as85{
/// Parse tree & Abstract syntax tree classes.
namespace ast{

/// Stores an AST node position on the source code.
struct Position{
    struct{
        size_t row;
        size_t col;
    } begin;
    struct{
        size_t row;
        size_t col;
    } end;
    Position merge(const Position &p) const;

    //Base comparison operators
    bool operator<(const Position &rhs) const;
    bool operator==(const Position &rhs) const;
    //Derived operators
    bool operator<=(const Position &rhs) const;
    bool operator>(const Position &rhs) const;
    bool operator>=(const Position &rhs) const;
    bool operator!=(const Position &rhs) const;
};

//Forward declaration of visitor
namespace visitor{
    class Visitor;
    class ConstVisitor;
}

/// @brief Base class for all AST nodes.
///
/// This class keeps track of its position, its children, and manages the
/// modification of these two things.
class Ast : public std::enable_shared_from_this<Ast> {
    public:
    Ast();
    Ast(const YYSTYPE *token);

    /* Forbid AST copying, for now */
    Ast(Ast& node) = delete;
    Ast& operator=(Ast& node) = delete;

    virtual ~Ast();

    //Ast methods
    void addChildBack(Ast* node);
    void addChildBack(std::shared_ptr<Ast> node);
    void addChildFront(Ast* node);
    void addChildFront(std::shared_ptr<Ast> node);
    std::shared_ptr<Ast> extractChild(size_t position);
    std::shared_ptr<Ast> extractChild(const Ast &n);
    /// @brief Remove this node from the tree and return a reference to itself.
    std::shared_ptr<Ast> extract();
    std::vector <std::shared_ptr<Ast>> getChildren() const;
    size_t getChildCount() const;
    std::shared_ptr<Ast> getParent() const;
    

    void setParent(std::weak_ptr<Ast> ref);
    void setParent(std::shared_ptr<Ast> ref);

    std::weak_ptr<Ast> getSelfRef();

    //Virtual Ast methods
    virtual Position getPosition() const;
    virtual void accept(visitor::ConstVisitor &v) const;
    virtual void accept(visitor::Visitor &v);
    virtual std::string toString() const;

    protected:
    std::weak_ptr<Ast> wparent;
    Position position;
    std::vector <std::shared_ptr<Ast>> children;

    static void free_token(YYSTYPE* token);
    void update_position();

    private:
    void update_parent(Ast& node, bool invalidate = false);
};

/// @brief Inserts a child node to the right hand side of the tree.
/// @note Usage is restricted to the YACC parser due to it being VERY unsafe
/// to use; See AST_GET on i8085.y for the single place it is acceptable
/// to be used for now.
Ast* operator<<(Ast* parent_ptr, std::shared_ptr<Ast> child_ref);
/// @brief Inserts a child node to the right hand side of the tree.
Ast& operator<<(Ast& parent, std::shared_ptr<Ast> child_ref);
/// @brief Inserts a child node to the right hand side of the tree.
std::shared_ptr<Ast> operator<<(std::shared_ptr<Ast> parent_ref, std::shared_ptr<Ast> child_ref);


/// @brief Stores some text on the AST.
///
/// This class is intended to be used when no other AST class is available,
/// especially during development where we may want to add some commentary
/// until a new AST node class is implemented.
class TextNode : public Ast{
    public:
    TextNode(std::string text = "");
    TextNode(YYSTYPE *token);
    virtual std::string toString() const;
    void setText(const std::string &text);

    private:
    std::string text;
};

/// @brief Adds a comment to an AST node.
///
/// This class is used to point to AST nodes and add some information intended
/// for the programmer, for example Warnings and errors.
class AstAnnotation : public TextNode {
    public:
    AstAnnotation(std::string text = "");
    AstAnnotation(YYSTYPE *token);
};

/// @brief Base class for all AST nodes that translate to integer values.
///
/// This class is intended to be used whever anything translates into an
/// integer, regardless of its origin or value. This class is pure virtual
/// and any subclasses must define the `getValue`method.
class AstValue : public Ast {
    public:
    AstValue(const YYSTYPE* token);
    AstValue();
    virtual std::int32_t getValue() const = 0;
};

/// Parse integer constants and make its value available.
class Integer : public AstValue {
    public:
    Integer(YYSTYPE* token);
    virtual std::int32_t getValue() const;
    virtual std::string toString() const;

    private:
    std::int32_t value;
};

/// @brief Class for label definition.
class Label : public AstValue {
    public:
    Label(YYSTYPE* token);
    virtual std::int32_t getValue() const;
    virtual std::string toString() const;
    std::string getIdentifier() const;

    private:
    std::string identifier;
};

}//ast
}//as85

#endif
