#ifndef __ASM_HXX
#define __ASM_HXX

#include <string>
#include <memory>
#include <exception>
#include <vector>
#include <stdpolyfills.hxx>

#include <as85.hxx>

#include "nametable.hxx"

namespace as85{

//Forward declarations from as85::ast
namespace ast{
    class Ast;
}
//-----------------------------------

/// @brief Assembly process classes
namespace assembler {

/// @brief Keeps the assembly state 
class Assembler {
public:
    Assembler(const std::string src);

    /// @brief Parses the source file and generates the AST.
    void parse();
    void logAnnotations();

private:
    STD::filesystem::path src_path;
    std::shared_ptr<ast::Ast> ast_root;
    std::vector< std::shared_ptr<const ast::Ast> > annotations;
    Nametable nametable;

    /// @brief Checks the integrity of the AST.
    /// 
    /// If the integrity check fails, an instance of AssemblerException is thrown.
    /// @param file File that called the method, use __FILE__ or leave emtpy.
    /// @param line Line where the call is located, use __LINE__ or leave empty.
    void astIntegrityCheck(const std::string &file = "", int line = -1) const;

    //Allow test class to access privates
    friend class TestClass_Assembler;
};

}//assembler
}//as85

#endif
