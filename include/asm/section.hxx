#ifndef __SECTION_HXX
#define __SECTION_HXX

namespace as85{
namespace assembler {

/// @brief Keeps track of the named ections and their addresses.
class Section {
    public:
    Section();
    Section(const Section& n) = delete;
    virtual ~Section();
};

} //assembler
} //as85

#endif
