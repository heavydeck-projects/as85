#ifndef __ASM_EXCEPTION_HXX
#define __ASM_EXCEPTION_HXX

#include <string>
#include <as85.hxx>

namespace as85{
namespace assembler{

/// @brief Base exception for all the as85::assembler namespace exceptions
class AssemblerException : public as85::As85Exception{
    public:
    AssemblerException();
    AssemblerException(const std::string &message, const std::string &file = "", int line = -1);
};

/// @brief File does not exist
class FileNotFoundException : public AssemblerException {
    public:
    FileNotFoundException(const std::string &path, const std::string &file = "", int line = -1);
};

} //assembler
} //as85

#endif
