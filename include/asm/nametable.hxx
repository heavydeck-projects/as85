#ifndef __NAMETABLE_HXX
#define __NAMETABLE_HXX

namespace as85{
namespace assembler {

/// @brief Keeps track of the named symbols and their position.
class Nametable {
    public:
    Nametable();
    Nametable(const Nametable& n) = delete;
    virtual ~Nametable();
};

} //assembler
} //as85

#endif
