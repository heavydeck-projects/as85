# --- Parser tests
ADD_TEST(NAME "parser::too_many_comments"      COMMAND "test_parser" "--out-directory=${PROJECT_SOURCE_DIR}/test/_graphs" "-v" "${PROJECT_SOURCE_DIR}/test/parser/too_many_comments.asm")
ADD_TEST(NAME "parser::too_many_nops"          COMMAND "test_parser" "--out-directory=${PROJECT_SOURCE_DIR}/test/_graphs" "-v" "${PROJECT_SOURCE_DIR}/test/parser/too_many_nops.asm")
ADD_TEST(NAME "parser::absolute"               COMMAND "test_parser" "--out-directory=${PROJECT_SOURCE_DIR}/test/_graphs" "-v" "${PROJECT_SOURCE_DIR}/test/parser/absolute.asm")
ADD_TEST(NAME "parser::all_opcodes"            COMMAND "test_parser" "--out-directory=${PROJECT_SOURCE_DIR}/test/_graphs" "-v" "${PROJECT_SOURCE_DIR}/test/parser/all_opcodes.asm")
ADD_TEST(NAME "parser::bom_utf8"               COMMAND "test_parser" "--out-directory=${PROJECT_SOURCE_DIR}/test/_graphs" "-v" "${PROJECT_SOURCE_DIR}/test/parser/bom_utf8.asm")
ADD_TEST(NAME "parser::case_insensitive"       COMMAND "test_parser" "--out-directory=${PROJECT_SOURCE_DIR}/test/_graphs" "-v" "${PROJECT_SOURCE_DIR}/test/parser/case_insensitive.asm")
ADD_TEST(NAME "parser::labels"                 COMMAND "test_parser" "--out-directory=${PROJECT_SOURCE_DIR}/test/_graphs" "-v" "${PROJECT_SOURCE_DIR}/test/parser/labels.asm")
ADD_TEST(NAME "parser::missing_newline_at_end" COMMAND "test_parser" "--out-directory=${PROJECT_SOURCE_DIR}/test/_graphs" "-v" "${PROJECT_SOURCE_DIR}/test/parser/missing_newline_at_end.asm")
ADD_TEST(NAME "parser::strings_and_arrays"     COMMAND "test_parser" "--out-directory=${PROJECT_SOURCE_DIR}/test/_graphs" "-v" "${PROJECT_SOURCE_DIR}/test/parser/strings_and_arrays.asm")
ADD_TEST(NAME "parser::triviality_00"          COMMAND "test_parser" "--out-directory=${PROJECT_SOURCE_DIR}/test/_graphs" "-v" "${PROJECT_SOURCE_DIR}/test/parser/triviality_00.asm")
ADD_TEST(NAME "parser::z80_detection"          COMMAND "test_parser" "--out-directory=${PROJECT_SOURCE_DIR}/test/_graphs" "-v" "${PROJECT_SOURCE_DIR}/test/parser/z80_detection.asm")
ADD_TEST(NAME "parser::declare_word"           COMMAND "test_parser" "--out-directory=${PROJECT_SOURCE_DIR}/test/_graphs" "-v" "${PROJECT_SOURCE_DIR}/test/parser/declare_word.asm")
ADD_TEST(NAME "parser::empty_program"          COMMAND "test_parser" "--out-directory=${PROJECT_SOURCE_DIR}/test/_graphs" "-v" "${PROJECT_SOURCE_DIR}/test/parser/empty_program.asm")

# --- Parser error tests
#These tests MUST provoke the creation of Error-type nodes on the AST, but
#the parsing process itself shall not fail catastrophically.
ADD_TEST(NAME "parser_error::bom_utf16le"        COMMAND "test_parser" "--out-directory=${PROJECT_SOURCE_DIR}/test/_graphs" "--any-errors"         "${PROJECT_SOURCE_DIR}/test/parser_error/bom_utf16le.asm")
ADD_TEST(NAME "parser_error::bom_utf16be"        COMMAND "test_parser" "--out-directory=${PROJECT_SOURCE_DIR}/test/_graphs" "--any-errors"         "${PROJECT_SOURCE_DIR}/test/parser_error/bom_utf16be.asm")
ADD_TEST(NAME "parser_error::bad_label"          COMMAND "test_parser" "--out-directory=${PROJECT_SOURCE_DIR}/test/_graphs" "--expected-errors=1"  "${PROJECT_SOURCE_DIR}/test/parser_error/bad_label.asm")
ADD_TEST(NAME "parser_error::missing_value"      COMMAND "test_parser" "--out-directory=${PROJECT_SOURCE_DIR}/test/_graphs" "--expected-errors=5"  "${PROJECT_SOURCE_DIR}/test/parser_error/missing_value.asm")
ADD_TEST(NAME "parser_error::missing_reg"        COMMAND "test_parser" "--out-directory=${PROJECT_SOURCE_DIR}/test/_graphs" "--expected-errors=8"  "${PROJECT_SOURCE_DIR}/test/parser_error/missing_reg.asm")
ADD_TEST(NAME "parser_error::syntax_errors"      COMMAND "test_parser" "--out-directory=${PROJECT_SOURCE_DIR}/test/_graphs" "--expected-errors=46" "${PROJECT_SOURCE_DIR}/test/parser_error/syntax_errors.asm")
ADD_TEST(NAME "parser_error::declare_word_error" COMMAND "test_parser" "--out-directory=${PROJECT_SOURCE_DIR}/test/_graphs" "--expected-errors=5"  "${PROJECT_SOURCE_DIR}/test/parser_error/declare_word_error.asm")

# --- Parser warning
#These tests MUST NOT create error nodes but MUST create Warning
#nodes on the AST. Parsing MUST succed.
ADD_TEST(NAME "warning::warnings" COMMAND "test_parser" "--out-directory=${PROJECT_SOURCE_DIR}/test/_graphs" "-v" "${PROJECT_SOURCE_DIR}/test/warning/warnings.asm")

# --- Parser reusability tests
ADD_TEST(NAME "ast::parser::parser_reset" COMMAND "test_parser_reset"
    "${PROJECT_SOURCE_DIR}/test/parser/absolute.asm"
    "${PROJECT_SOURCE_DIR}/test/parser/all_opcodes.asm"
    "${PROJECT_SOURCE_DIR}/test/parser/too_many_comments.asm"
    "${PROJECT_SOURCE_DIR}/test/parser/too_many_nops.asm"
    "${PROJECT_SOURCE_DIR}/test/parser/labels.asm"
    "${PROJECT_SOURCE_DIR}/test/parser/missing_newline_at_end.asm"
    "${PROJECT_SOURCE_DIR}/test/parser/strings_and_arrays.asm"
    "${PROJECT_SOURCE_DIR}/test/parser/triviality_00.asm"
    "${PROJECT_SOURCE_DIR}/test/parser/z80_detection.asm"
    "${PROJECT_SOURCE_DIR}/test/parser/bom_utf8.asm"
    "${PROJECT_SOURCE_DIR}/test/parser/case_insensitive.asm"
    "${PROJECT_SOURCE_DIR}/test/parser/declare_word.asm"
)

# --- Assembled size tests ---

ADD_TEST(NAME "size::db-8bit"  COMMAND "test_parser" "--out-directory=${PROJECT_SOURCE_DIR}/test/_graphs" "--expected-size=8"  "${PROJECT_SOURCE_DIR}/test/size/db-8bit.asm")
ADD_TEST(NAME "size::dw-16bit" COMMAND "test_parser" "--out-directory=${PROJECT_SOURCE_DIR}/test/_graphs" "--expected-size=16" "${PROJECT_SOURCE_DIR}/test/size/dw-16bit.asm")
ADD_TEST(NAME "size::dw-24bit" COMMAND "test_parser" "--out-directory=${PROJECT_SOURCE_DIR}/test/_graphs" "--expected-size=24" "${PROJECT_SOURCE_DIR}/test/size/dw-24bit.asm")
ADD_TEST(NAME "size::dw-32bit" COMMAND "test_parser" "--out-directory=${PROJECT_SOURCE_DIR}/test/_graphs" "--expected-size=32" "${PROJECT_SOURCE_DIR}/test/size/dw-32bit.asm")


# --- Binary representation tests
ADD_TEST(NAME "ast::Opcode::test_bytes"   COMMAND "test_opcode_bytes")
ADD_TEST(NAME "ast::String::test_strings" COMMAND "test_strings")

# --- Tests used to keep solved bugs in check and prevent regressions ---
ADD_TEST(NAME "bug::bison_double_delete" COMMAND "test_parser" "--out-directory=${PROJECT_SOURCE_DIR}/test/_graphs" "--any-errors" "${PROJECT_SOURCE_DIR}/test/bugs/bison_double_delete.asm")
ADD_TEST(NAME "bug::leaky_errors"        COMMAND "test_parser" "--out-directory=${PROJECT_SOURCE_DIR}/test/_graphs" "--any-errors" "${PROJECT_SOURCE_DIR}/test/bugs/leaky_errors.asm")
ADD_TEST(NAME "bug::bad_weak_ptr"        COMMAND "test_parser" "--out-directory=${PROJECT_SOURCE_DIR}/test/_graphs" "--no-handle-exceptions" "${PROJECT_SOURCE_DIR}/test/bugs/bad_weak_ptr.asm")
