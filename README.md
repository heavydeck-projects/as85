
# AS85 an i8085 assembler

Add some description here

# Build requirements

AS85 requires the following tools for building. These should be available on
most Linux distributions packages. Windows binaries also exist.

* C and C++ compiler
* CMake
* [Flex][flex] lexical analyzer
* [BYACC][byacc] or [GNU Bison][bison]

[flex]: https://github.com/westes/flex
[byacc]: https://invisible-island.net/byacc/
[bison]: https://www.gnu.org/software/bison/

## Building

This project requires some external repositories which must be initialized
first by running:

```sh
git submodule update --init
```

AS85 uses CMake for builds, if you only want to build this project the
quick recipe is running the following commands on the project's source root
directory:

```sh
cmake -DCMAKE_BUILD_TYPE=Release .
make
#You may optionally run tests to check everything is OK
make test
```

### Additional options

You may pass additional options to CMake with `-D<OPTION>=<VALUE>` for building
in your particular environment.

| Option         | Default | Description                                      |
|----------------|:-------:|--------------------------------------------------|
| `DEBUG_PARSER` |  `NO`   | Enable parser debug output |
| `USE_BISON`    |  `NO`   | Use GNU Bison instead of BYACC for parser generation |
| `USE_BOOST`    |  `NO`   | Use Boost instead of C++ std for certain headers and classes that may not be available on some environments |

# Known issues

*Nothing to see here for now...*
