#include <as85.hxx>
#include <info.hxx>
#include <iostream>
#include <typeinfo>
#include <cstdlib>
#include <string>
#include <sstream>
#include <list>
#include <map>

// -----------------------------------------------------
// --- Last-ditch exception handler / crash reporter ---
// -----------------------------------------------------

#ifdef __GNUC__
#include <cxxabi.h>
#endif

static const char* slurp = 
"            `         `     \r\n"
"         _   `       ``     \r\n"
"     `  |_|`` ``    `  `    \r\n"
"    ` `-___-_  `` ``   ``   \r\n"
"   `` /      )  ``      `   \r\n"
"  ___/| (O) (O)_()``     `` \r\n"
"/|  | |   ^_____) ``      ` \r\n"
"||  |_|      |` cU````   `` \r\n"
"||   ||      |`============ \r\n"
;
///@brief Last-ditch exception handler; Call whenever an exception reaches main().
static void this_is_fine(int argc, char** argv, std::exception &e)
{
    //A catch-all exception handler to show some information about the
    //exception before exitting unceremoniously with an error
    std::cerr << slurp; //This is fine...;
    std::cerr << "-------------------------------------------------------------------------------" << std::endl;
    std::cerr << "Unhandled exception reached main function. Exception reason:" << std::endl;
    std::cerr << "  > " << e.what() << std::endl;
    std::cerr << "Exception type:" << std::endl;
    {
        //Get the exception class name from RTTI
        std::string exception_type;
#if defined __GNUC__
        //GCC typeid returns mangled names but provides a way of demangling it.
        int status = 0;
        exception_type = typeid(e).name();
        const char* real_exception_type = abi::__cxa_demangle(exception_type.c_str(), NULL, NULL, &status);
        if(status == 0){
            exception_type = std::string(real_exception_type);
            std::free((void*)real_exception_type);
        }
        else{
            exception_type = exception_type + " (__cxa_demangle failed)";
        }
#elif defined _MSC_VER
        //MSVC returns a readable name as-is.
        exception_type = typeid(e).name();
#else
        //typeid name() may return a mangled name which is not very readable
        //but it works on any C++ environment. Using as fallback.
        exception_type = typeid(e).name();
#endif
        std::cerr << "  > " << exception_type << std::endl;
    }
    std::cerr << "Program arguments:" << std::endl;
    for(int i = 0; i < argc; i++){
        std::cerr << "  > " << argv[i] << std::endl;
    }
    std::cerr << "-------------------------------------------------------------------------------" << std::endl;
    std::cerr << "---          POTENTIAL BUG! Please report this error to developers          ---" << std::endl;
    std::cerr << "-------------------------------------------------------------------------------" << std::endl;
    std::cerr.flush();
}

//Optparse plus implementation
#define OPTPARSE_API static
#define OPTPARSE_IMPLEMENTATION
#include <optparse.h>

struct opt_description_s {
    std::string param_name;
    std::string description;
};

/// @brief Help descriptions in the order they will be printed.
/// If an option description is not listed here, it will not be
/// printed when calling `--help`
static const std::list< std::pair<const std::string, const struct opt_description_s> > descriptions =
{
    {"help",    {"",     "This help message."}},
    {"output",  {"FILE", "Output file."}},
    {"quiet",   {"",     "Do not output messages to console."}},
    {"trace",   {"",     "Print a lot of info; Only useful for as85 developers."}},
    {"verbose", {"",     "Print more information to console."}},
    {"version", {"",     "Print program version."}},
};

static void version(){
    auto config = as85::Config::getInstance();
    std::cout << as85::version_as85() << std::endl;
    if(config->verbose_level >= as85::V_INFO){
        std::cout << as85::version_yacc() << std::endl;
    }
}

static void usage(const struct optparse_long *longopts){
    //Generate a map of string=>longopts for easy lookups
    std::map<std::string, const struct optparse_long *> opt_map;
    while(longopts->shortname){
        as85::trace() << "longopt: --" << longopts->longname << std::endl;
        opt_map[std::string(longopts->longname)] = longopts;
        longopts++;
    }

    //find the maximum width of left column.
    size_t longest_opt = 0;
    for(const auto &desc : descriptions){
        auto opt = opt_map.at(desc.first);
        //Length is 2 characters of left padding
        size_t opt_len = 2;
        //Plus 4 characters of the short opt + its padding: "-v, "
        opt_len += 4;
        //Plus the length of the long option
        opt_len += desc.first.length();
        //Plus the length of the argument name (if any) plus the '=' char
        if(desc.second.param_name.length()){
            opt_len += desc.second.param_name.length() + 1;
        }
        //If the argument is optional, add 2 for the enclosing "[]"
        if(opt->argtype == OPTPARSE_OPTIONAL){
            opt_len += 2;
        }
        //If the argument is not none, add 1 for the '=' char
        if(opt->argtype != OPTPARSE_NONE){
            opt_len++;
        }
        //And some extra space for separating the arguments from the side
        opt_len+=2;
        longest_opt = opt_len > longest_opt ? opt_len : longest_opt;
        as85::trace() << "longest opt: " << longest_opt << std::endl;
    }
    //The left padding is either longest_opt or the maximum padding
    static const size_t left_pad_max = 30;
    size_t left_pad = longest_opt > left_pad_max ? left_pad_max : longest_opt;

    //Tokenize the description using spaces
    std::map<std::string, std::list<std::string>> desc_words;
    for(const auto &desc : descriptions){
        size_t word_begin = 0;
        size_t word_end = 0;
        as85::trace() << "Tokenizing: " << desc.first << std::endl;
        auto &words = desc_words[desc.first];
        const std::string &text = desc.second.description;
        while(word_end != std::string::npos){
            word_end = text.find(" ", word_begin);
            std::string word;
            if(word_end != std::string::npos)
                word = std::string(text.cbegin() + word_begin, text.cbegin() + word_end + 1);
            else
                word = std::string(text.cbegin() + word_begin, text.cend());
            words.push_back(word);
            as85::trace() << "  - '" << word  << "' " << word_begin << " -> " << word_end << std::endl;
            word_begin = word_end + 1;
        }
    }

    //Print some header info
    {
        std::cerr << "Usage: as85 [OPTION]... [--] FILE [FILE]..." << std::endl;
        std::cerr << std::endl;
        std::cerr << "Mandatory arguments to long options are mandatory for short options too." << std::endl;
        std::cerr << std::endl;
    }

    //Print all the (documented) argument descriptions
    //A parameter description consists of the short opt (if any) followed by
    //the long opt, its arguments (if any) and then the description on the
    //right hand side.
    //The width of the whole line must not exceed 80 characters and the
    //description must be left padded to be aligned among themselves.
    for(const auto &desc : descriptions){
        std::stringstream ss;
        const auto &opt = opt_map[desc.first];
        ss << "  ";
        //If it has a short opt, print it. Otherwise, 4 spaces
        if(opt_map[desc.first]->shortname < '~'){
            ss << "-" << (char) opt_map[desc.first]->shortname << ", ";
        }
        else{
            ss << "    ";
        }
        //Print the long opt
        ss << "--" << desc.first;
        if(opt->argtype == OPTPARSE_OPTIONAL) ss << "[";
        if(opt->argtype != OPTPARSE_NONE) {
            ss << "=" << desc.second.param_name;
        }
        if(opt->argtype == OPTPARSE_OPTIONAL) ss << "]";

        //For each description word
        for(const std::string &word : desc_words[desc.first]){
            //Add padding if needed
            while(ss.str().length() < left_pad){
                ss << " ";
            }
            if( (word.length() + ss.str().length()) < 80){
                ss << word;
            }
            else{
                //Line reached 80 characters. Print it and clear ss
                std::cerr << ss.str() << std::endl;
                ss.str("");
            }
        }
        if(ss.str().length()){
            std::cerr << ss.str() << std::endl;
            ss.str("");
        }
    }

    //Print something at the end
    {
        //Nothing yet
    }
}

/// @brief Parse command line arguments into Config then call assembler.
/// @param argc argc as-is from main
/// @param argv argv as-is from main
/// @return Program main return status
static int inner_main(int argc, char** argv)
{
    (void) argc;
    bool args_ok = true;
    {
        auto config = as85::Config::getMutableInstance();

        //Initialize optparse
        ///@todo Add argument options.
        struct optparse_long longopts[] = {
            //Arguments with short + long options
            {"output",  'o', OPTPARSE_REQUIRED},
            {"quiet",   'q', OPTPARSE_NONE},
            {"verbose", 'v', OPTPARSE_NONE},
            {"help",    'h', OPTPARSE_NONE},
            {"version", 'V', OPTPARSE_NONE},

            //Arguments with only long options
            {"trace", 0x100, OPTPARSE_NONE},
            {"slurp", 0x1FF, OPTPARSE_NONE},
            
            //End of arguments
            {nullptr,  '\0', OPTPARSE_NONE},
        };
        struct optparse options;
        optparse_init(&options, argv);

        //Parse named options
        int option;
        while ((option = optparse_long(&options, longopts, NULL)) != -1) {
            switch (option){
                //help
                case 'h':
                    usage(longopts);
                    return 0;
                //out=FILE
                case 'o':
                    config->out_file = options.optarg;
                    break;
                //quiet
                case 'q':
                    config->verbose_level = as85::V_QUIET;
                    break;
                //verbose
                case 'v':
                    ++config->verbose_level;
                    break;
                //verbose
                case 'V':
                    version();
                    return 0;
                //trace
                case 0x100:
                    config->verbose_level = as85::V_MAX;
                    break;
                //slurp
                case 0x1FF:
                    throw as85::As85Exception("This is fine, just an exception test", __FILE__, __LINE__);
                    break;
                //Bad option
                case '?':
                    std::cerr << argv[0] << "--" << options.errmsg << std::endl;
                    args_ok = false;
                    break;
                //In case I forgot to implement managing an option
                default:
                {
                    std::stringstream ss;
                    if(option < 256){
                        ss << "Unmanaged argument <" << (char)option << ">" << std::flush;
                    }
                    else{
                        ss << "Unmanaged argument 0x" << std::hex << option << std::flush;
                    }
                    throw as85::As85Exception(ss.str(), __FILE__, __LINE__);
                }
            }
        }

        //Parse remaining (unnamed) arguments as input files
        char* arg;
        while ((arg = optparse_arg(&options))){
            STD::filesystem::path in_path(arg);
            config->input_files.push_back(in_path.lexically_normal());
        }
    }

    //If arguments failed to parse, exit with error
    if(!args_ok){
        return 1;
    }

    //Argument parsing done! Call assembler main
    return as85::as85_main();
}

/// @brief Entry point, install crash handler and call inner_main
int main(int argc, char** argv)
{
    //Catch-all exception handler, for critical bug error messages.
    try{
        return inner_main(argc, argv);
    }
    catch(std::exception &e)
    {
        this_is_fine(argc, argv, e);
        return 1;
    }
}
