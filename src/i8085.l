%{

#include "i8085.tab.h"

#include <cstring>
#include <cassert>
//#include <iostream>

/* Helper function to keep track of row/column of each token option */
static int col_count = 1;
static int row_count = 1;
static void parse_token(YYSTYPE* l_value, int token)
{
    size_t token_size = strlen(yytext);
    /* Copy all relevant info stuff to yylval struct */
    l_value->s.token = token;
    l_value->s.text = NULL;
    l_value->s.row = row_count;
    l_value->s.col = col_count;
    l_value->s.size = token_size;

    /* Only copy some tokens which will be actually needed by the parser */
    /* Parser is responsible for freeing these buffers */
    switch(token){

        /* These tokens are NEVER copied */
        case 0:
        case '(':
        case ')':
        case '[':
        case ']':
        case ':':
        case ',':
        case NEWLINE:
        case UNICODE_BOM:
        break;

        /* Default case is to DO copy the token */
        default:

        /* These tokens are ALWAYS copied */
        case IDENTIFIER:
        case BINARY_CONSTANT:
        case DECIMAL_CONSTANT:
        case OCTAL_CONSTANT:
        case HEXADECIMAL_CONSTANT:
        {
            char* str = (char*) calloc(1, token_size + 1);
            l_value->s.text = str;
            strncpy(str, yytext, token_size + 1);
        }
        break;

        /* For string and character literals */
        /* `s.text` will not contain enclosing quotes */
        case STRING_LITERAL:
        case CHARACTER_LITERAL:
        if(token_size > 2){
            char* str = (char*) calloc(1, token_size + 1);
            l_value->s.text = str;
            strncpy(str, yytext + 1, token_size - 2);
        }
        else{
            //Empty string if only the delimiters are present
            l_value->s.text = (char*) calloc(1, 1);
        }
        break;
    }

    /*Update the column count*/
    col_count += (int)token_size;

    /*If newline token, reset column counter, increment row counter*/
    if( token == NEWLINE )
    {
        col_count = 1;
        row_count++;
    }

    //If text on token, print the debug
    //if(l_value->s.text) std::cerr << "+" << l_value->s.text << std::endl;
}

%}

%option noyywrap

/* Regex macros */

nondigit            [_a-zA-Z]
digit               [0-9]
alphanum            [0-9_a-zA-Z]
nonzero_digit       [1-9]
octal_digit         [0-7]
hexadecimal_digit   [0-9a-fA-F]
binary_digit        [01]

simple_escape_sequence \\[\\abfnrtv0]
hexadecimal_escape_sequence \\[xX]{hexadecimal_digit}{hexadecimal_digit}
octal_escape_sequence \\{octal_digit}{octal_digit}{octal_digit}
escape_sequence {simple_escape_sequence}|{octal_escape_sequence}|{hexadecimal_escape_sequence}
s_char_sequence ({escape_sequence}|[^\"\\])*
c_char_sequence ({escape_sequence}|[^'\\])

%%

 /* Registers */
[Aa]             { parse_token(yylval, REG_A); return REG_A; }
[Ff]             { parse_token(yylval, REG_F); return REG_F; }
[Bb]             { parse_token(yylval, REG_B); return REG_B; }
[Cc]             { parse_token(yylval, REG_C); return REG_C; }
[Dd]             { parse_token(yylval, REG_D); return REG_D; }
[Ee]             { parse_token(yylval, REG_E); return REG_E; }
[Hh]             { parse_token(yylval, REG_H); return REG_H; }
[Ll]             { parse_token(yylval, REG_L); return REG_L; }
[Mm][Ee][Mm]     { parse_token(yylval, REG_MEM); return REG_MEM; }
 /* 16bit registers */
[Ss][Pp]         { parse_token(yylval, REG_SP); return REG_SP; }
[Pp][Cc]         { parse_token(yylval, REG_PC); return REG_PC; }
 /* Register pairs */
[Aa][Ff]         { parse_token(yylval, REG_AF); return REG_AF; }
[Pp][Ss][Ww]     { parse_token(yylval, REG_AF); return REG_AF; }
[Bb][Cc]         { parse_token(yylval, REG_BC); return REG_BC; }
[Dd][Ee]         { parse_token(yylval, REG_DE); return REG_DE; }
[Hh][Ll]         { parse_token(yylval, REG_HL); return REG_HL; }

 /* Data transfer */
[Mm][Oo][Vv]     { parse_token(yylval, OP_MOV); return OP_MOV; }
[Mm][Vv][Ii]     { parse_token(yylval, OP_MVI); return OP_MVI; }
[Ll][Xx][Ii]     { parse_token(yylval, OP_LXI); return OP_LXI; }
[Ll][Dd][Aa]     { parse_token(yylval, OP_LDA); return OP_LDA; }
[Ss][Tt][Aa]     { parse_token(yylval, OP_STA); return OP_STA; }
[Ll][Hh][Ll][Dd] { parse_token(yylval, OP_LHLD); return OP_LHLD; }
[Ss][Hh][Ll][Dd] { parse_token(yylval, OP_SHLD); return OP_SHLD; }
[Ll][Dd][Aa][Xx] { parse_token(yylval, OP_LDAX); return OP_LDAX; }
[Ss][Tt][Aa][Xx] { parse_token(yylval, OP_STAX); return OP_STAX; }
[Xx][Cc][Hh][Gg] { parse_token(yylval, OP_XCHG); return OP_XCHG; }

 /* Arithmetic */
[Aa][Dd][Dd]     { parse_token(yylval, OP_ADD); return OP_ADD; }
[Aa][Dd][Cc]     { parse_token(yylval, OP_ADC); return OP_ADC; }
[Aa][Dd][Ii]     { parse_token(yylval, OP_ADI); return OP_ADI; }
[Aa][Cc][Ii]     { parse_token(yylval, OP_ACI); return OP_ACI; }
[Dd][Aa][Dd]     { parse_token(yylval, OP_DAD); return OP_DAD; }
[Ss][Uu][Bb]     { parse_token(yylval, OP_SUB); return OP_SUB; }
[Ss][Bb][Bb]     { parse_token(yylval, OP_SBB); return OP_SBB; }
[Ss][Uu][Ii]     { parse_token(yylval, OP_SUI); return OP_SUI; }
[Ss][Bb][Ii]     { parse_token(yylval, OP_SBI); return OP_SBI; }
[Ii][Nn][Rr]     { parse_token(yylval, OP_INR); return OP_INR; }
[Dd][Cc][Rr]     { parse_token(yylval, OP_DCR); return OP_DCR; }
[Ii][Nn][Xx]     { parse_token(yylval, OP_INX); return OP_INX; }
[Dd][Cc][Xx]     { parse_token(yylval, OP_DCX); return OP_DCX; }
[Dd][Aa][Aa]     { parse_token(yylval, OP_DAA); return OP_DAA; }

 /* Logical */
[Aa][Nn][Aa]     { parse_token(yylval, OP_ANA); return OP_ANA; } /*                                   */
[Aa][Nn][Dd]     { parse_token(yylval, OP_ANA); return OP_ANA; } /*<- Both AND/ANA appear on the docs */
[Aa][Nn][Ii]     { parse_token(yylval, OP_ANI); return OP_ANI; }
[Oo][Rr][Aa]     { parse_token(yylval, OP_ORA); return OP_ORA; }
[Oo][Rr][Ii]     { parse_token(yylval, OP_ORI); return OP_ORI; }
[Xx][Rr][Aa]     { parse_token(yylval, OP_XRA); return OP_XRA; }
[Xx][Rr][Ii]     { parse_token(yylval, OP_XRI); return OP_XRI; }
[Cc][Mm][Aa]     { parse_token(yylval, OP_CMA); return OP_CMA; }
[Cc][Mm][Cc]     { parse_token(yylval, OP_CMC); return OP_CMC; }
[Ss][Tt][Cc]     { parse_token(yylval, OP_STC); return OP_STC; }
[Cc][Mm][Pp]     { parse_token(yylval, OP_CMP); return OP_CMP; }
[Cc][Pp][Ii]     { parse_token(yylval, OP_CPI); return OP_CPI; }

 /* Rotations and shifts */
[Rr][Ll][Cc]     { parse_token(yylval, OP_RLC); return OP_RLC; }
[Rr][Rr][Cc]     { parse_token(yylval, OP_RRC); return OP_RRC; }
[Rr][Aa][Ll]     { parse_token(yylval, OP_RAL); return OP_RAL; }
[Rr][Aa][Rr]     { parse_token(yylval, OP_RAR); return OP_RAR; }

 /* Jumps */
[Jj][Mm][Pp]     { parse_token(yylval, OP_JMP); return OP_JMP; }
[Jj][Zz]         { parse_token(yylval, OP_JZ); return OP_JZ; }
[Jj][Nn][Zz]     { parse_token(yylval, OP_JNZ); return OP_JNZ; }
[Jj][Cc]         { parse_token(yylval, OP_JC); return OP_JC; }
[Jj][Nn][Cc]     { parse_token(yylval, OP_JNC); return OP_JNC; }
[Jj][Pp]         { parse_token(yylval, OP_JP); return OP_JP; }
[Jj][Mm]         { parse_token(yylval, OP_JM); return OP_JM; }
[Jj][Pp][Ee]     { parse_token(yylval, OP_JPE); return OP_JPE; }
[Jj][Pp][Oo]     { parse_token(yylval, OP_JPO); return OP_JPO; }
[Pp][Cc][Hh][Ll] { parse_token(yylval, OP_PCHL); return OP_PCHL; }
 /* Calls */
[Cc][Aa][Ll][Ll] { parse_token(yylval, OP_CALL); return OP_CALL; }
[Cc][Cc]         { parse_token(yylval, OP_CC); return OP_CC; }
[Cc][Nn][Cc]     { parse_token(yylval, OP_CNC); return OP_CNC; }
[Cc][Zz]         { parse_token(yylval, OP_CZ); return OP_CZ; }
[Cc][Nn][Zz]     { parse_token(yylval, OP_CNZ); return OP_CNZ; }
[Cc][Pp]         { parse_token(yylval, OP_CP); return OP_CP; }
[Cc][Mm]         { parse_token(yylval, OP_CM); return OP_CM; }
[Cc][Pp][Ee]     { parse_token(yylval, OP_CPE); return OP_CPE; }
[Cc][Pp][Oo]     { parse_token(yylval, OP_CPO); return OP_CPO; }
 /* Returns */
[Rr][Ee][Tt]     { parse_token(yylval, OP_RET); return OP_RET; }
[Rr][Cc]         { parse_token(yylval, OP_RC); return OP_RC; }
[Rr][Nn][Cc]     { parse_token(yylval, OP_RNC); return OP_RNC; }
[Rr][Zz]         { parse_token(yylval, OP_RZ); return OP_RZ; }
[Rr][Nn][Zz]     { parse_token(yylval, OP_RNZ); return OP_RNZ; }
[Rr][Pp]         { parse_token(yylval, OP_RP); return OP_RP; }
[Rr][Mm]         { parse_token(yylval, OP_RM); return OP_RM; }
[Rr][Pp][Ee]     { parse_token(yylval, OP_RPE); return OP_RPE; }
[Rr][Pp][Oo]     { parse_token(yylval, OP_RPO); return OP_RPO; }
[Rr][Ss][Tt]     { parse_token(yylval, OP_RST); return OP_RST; }

 /* Stack */
[Pp][Uu][Ss][Hh] { parse_token(yylval, OP_PUSH); return OP_PUSH; }
[Pp][Oo][Pp]     { parse_token(yylval, OP_POP); return OP_POP; }
[Xx][Tt][Hh][Ll] { parse_token(yylval, OP_XTHL); return OP_XTHL; }
[Ss][Pp][Hh][Ll] { parse_token(yylval, OP_SPHL); return OP_SPHL; }

 /* IO and others */
[Ii][Nn]         { parse_token(yylval, OP_IN); return OP_IN; }
[Oo][Uu][Tt]     { parse_token(yylval, OP_OUT); return OP_OUT; }
[Hh][Ll][Tt]     { parse_token(yylval, OP_HLT); return OP_HLT; }
[Ee][Ii]         { parse_token(yylval, OP_EI); return OP_EI; }
[Dd][Ii]         { parse_token(yylval, OP_DI); return OP_DI; }
[Ss][Ii][Mm]     { parse_token(yylval, OP_SIM); return OP_SIM; }
[Rr][Ii][Mm]     { parse_token(yylval, OP_RIM); return OP_RIM; }
[Nn][Oo][Pp]     { parse_token(yylval, OP_NOP); return OP_NOP; }

 /* Pseudo-ops */
\.[Oo][Rr][Gg]                 { parse_token(yylval, PS_ORG); return PS_ORG; }
\.[Ss][Tt][Rr]                 { parse_token(yylval, PS_STR); return PS_STR; }
\.[Cc][Ss][Tt][Rr]             { parse_token(yylval, PS_CSTR); return PS_CSTR; }
\.[Dd][Bb]                     { parse_token(yylval, PS_DB); return PS_DB; }
\.[Dd][Ww]                     { parse_token(yylval, PS_DW); return PS_DW; }
\.[Ss][Ee][Cc][Tt][Ii][Oo][Nn] { parse_token(yylval, PS_SECTION); return PS_SECTION; }
\.[Ee][Xx][Pp][Oo][Rr][Tt]     { parse_token(yylval, PS_EXPORT); return PS_EXPORT; }
\.[Aa][Ll][Ii][Gg][Nn]         { parse_token(yylval, PS_ALIGN); return PS_ALIGN; }
\.[Ee][Xx][Tt][Ee][Rr][Nn]     { parse_token(yylval, PS_EXTERN); return PS_EXTERN; }

 /* Additional non-opcode tokens */
","              { parse_token(yylval, ','); return ','; }
"["              { parse_token(yylval, '['); return '['; }
"]"              { parse_token(yylval, ']'); return ']'; }
"("              { parse_token(yylval, '('); return '('; }
")"              { parse_token(yylval, ')'); return ')'; }
":"              { parse_token(yylval, ':'); return ':'; }

 /*  Identifier  */
{nondigit}{alphanum}* { parse_token(yylval, IDENTIFIER); return IDENTIFIER; }

 /*  numeric constants  */
"0"[bB]{binary_digit}+      { parse_token(yylval, BINARY_CONSTANT);      return BINARY_CONSTANT; }
"0"{octal_digit}*           { parse_token(yylval, OCTAL_CONSTANT);       return OCTAL_CONSTANT; }
{nonzero_digit}{digit}*     { parse_token(yylval, DECIMAL_CONSTANT);     return DECIMAL_CONSTANT; }
"0"[xX]{hexadecimal_digit}+ { parse_token(yylval, HEXADECIMAL_CONSTANT); return HEXADECIMAL_CONSTANT; }

 /*  String literal  */
\"{s_char_sequence}\"      { parse_token(yylval, STRING_LITERAL);    return STRING_LITERAL; }
\'{c_char_sequence}\'      { parse_token(yylval, CHARACTER_LITERAL); return CHARACTER_LITERAL; }

 /*  Unicode BOMs. UTF-8 will be ignored. UTF16/32 will generate a UNICODE_BOM  */
 /*  token that will make the parser fail at the very begining.                 */
 /*  See: https://en.wikipedia.org/wiki/Byte_order_mark                         */
\xEF\xBB\xBF     { /* UTF8 is OK, ignore BOM and keep going */ } 
\xFE\xFF         { parse_token(yylval, UNICODE_BOM); return UNICODE_BOM; /* UTF16-BE */ }
\xFF\xFE         { parse_token(yylval, UNICODE_BOM); return UNICODE_BOM; /* UTF16-LE */ }
\x00\x00\xFE\xFF { parse_token(yylval, UNICODE_BOM); return UNICODE_BOM; /* UTF32-BE */ }
\xFF\xFE\x00\x00 { parse_token(yylval, UNICODE_BOM); return UNICODE_BOM; /* UTF32-LE */ }
\x00             { /* Null bytes are ignored outside BOM; Fail a bit more gracefully */ }

 /*  Whitespace and rest  */
[ \t\v\f\r]+ { parse_token(yylval, 0); /* Silently discard whitespace between tokens */ }

\n           { parse_token(yylval, NEWLINE); return NEWLINE; }
 /* EOF will add an extra \n just in case the last line doesn't have a newline */
 /* Test case `missing_newline_at_end.asm` explicitly covers this case. */
<<EOF>>     {
                static int once = 0;
                parse_token(yylval, NEWLINE);
                if(once){
                    col_count = 1;
                    row_count = 1;
                    once = 0;
                    return 0;
                }
                else {
                    once++;
                    return NEWLINE;
                }
            }

;.*         { parse_token(yylval, 0); /* Discard comments */ }

.           { parse_token(yylval, UNKNOWN_TOKEN); return UNKNOWN_TOKEN; }

%%
