#include <as85.hxx>
#include <iostream>
#include <sstream>
#include <memory>
#include <stdpolyfills.hxx>

#include <asm/asm.hxx>

namespace as85{

// --------------------
// --- Config class ---
// --------------------
//Save the build debug type here to prevent ifdefs on the future.
#ifdef NDEBUG
const bool Config::debug = false;
#else
const bool Config::debug = true;
#endif
std::shared_ptr<Config> Config::instance;
Config::Config()
{
    //Add configuration defaults here
    this->out_file = STD::filesystem::path("a.out");
    this->verbose_level = this->debug ? V_MAX : V_WARNING;
}

std::shared_ptr<Config> Config::getMutableInstance()
{
    if(!Config::instance)
    {
        //Can't use make_shared due to it being private.
        Config::instance = std::shared_ptr<Config>(new Config());
    }
    return Config::instance;
}

std::shared_ptr<const Config> Config::getInstance()
{
    return Config::getMutableInstance();
}

// ----------------------
// --- Verbosity enum ---
// ----------------------
void operator++(verbosity &v){
    v = (v >= V_MAX) ? (V_MAX) : ((verbosity)(((int)v) + 1));
}
void operator--(verbosity &v){
    v = (v >= V_MAX) ? (V_MAX) : ((verbosity)(((int)v) - 1));
}

// --------------------------------
// --- AssemblerException class ---
// --------------------------------
As85Exception::As85Exception(const std::string &message, const std::string &file, int line) {
    this->file = file;
    this->line = line;
    if(message.length()){
        this->message = message;
    }
    else{
        this->message = "[As85Exception] Default message.";
    }
    this->message += this->location_str();
}

std::string As85Exception::location_str() const{
    //If no data, just return an empty string
    if((this->file.length() == 0) && (this->line < 0)){
        return "";
    }

    std::stringstream ss;
    ss << "[";
    if(this->file.length()){
        STD::filesystem::path p(this->file);
        ss << p.filename().string();
    }
    else{
        ss << "<Unknown file>";
    }

    if(this->line >= 0){
        ss << " @" << this->line;
    }
    ss << "] " << std::flush;
    return ss.str();
}

const char* As85Exception::what() const noexcept{
    return this->message.c_str();
}

// --- Logging ---
static std::ostream bit_bucket(0);
std::ostream& log(verbosity log_level){
    auto config = Config::getInstance();
    if (log_level <= config->verbose_level){
        return std::cerr;
    }
    else{
        return bit_bucket;
    }
}

std::ostream& critical() {return log(V_CRITICAL) << "[ERROR] ";}
std::ostream& warning()  {return log(V_WARNING)  << "[WARNING] ";}
std::ostream& info()     {return log(V_INFO) << "[INFO] ";}
std::ostream& debug()    {return log(V_DEBUG) << "[DEBUG] ";};
std::ostream& trace()    {return log(V_TRACE) << "[TRACE] ";}

// ------------
// --- Main ---
// ------------
int as85_main()
{
    auto config = Config::getInstance();

    //Check files exist
    for(auto in_file : config->input_files){
        //Check that file exists
        if(!STD::filesystem::exists(in_file)){
            warning() << "File " << in_file.string() << " not found" << std::endl;
            return RET_NOK;
        }
    }

    //Assemble each file
    for(size_t i = 0; i < config->input_files.size(); i++){
        trace() << "Assembling file " << i+1 << "/" << config->input_files.size() << ": " << config->input_files.at(i) << std::endl;
        assembler::Assembler a( (config->input_files.at(i)).string() );
        a.parse();
    }

    return RET_OK;
}

}; //as85
