#include <ast/register.hxx>
#include <ast/ast.hxx>
#include <ast/exception.hxx>

#include <i8085.tab.h>

namespace as85 {
namespace ast {

// --- Register class ---

Register::Register(const YYSTYPE* token) : AstValue(token) {

}

// --- Register8 class ---

static const std::vector<std::string> reg8_names = {
    "B",
    "C",
    "D",
    "E",
    "H",
    "L",
    "MEM",
    "A",
};

Register8::Register8(YYSTYPE* token) : Register(token)
{
    switch (token->s.token)
    {
        case REG_B:
        this->reg = 0;
        break;

        case REG_C:
        this->reg = 1;
        break;
        
        case REG_D:
        this->reg = 2;
        break;
        
        case REG_E:
        this->reg = 3;
        break;

        case REG_H:
        this->reg = 4;
        break;

        case REG_L:
        this->reg = 5;
        break;

        case REG_MEM:
        case REG_HL:
        this->reg = 6;
        break;

        case REG_A:
        this->reg = 7;
        break;
        
        default:
        /* Unknown token received in constructor, fail. Loudly */
        AstException e(
            token,
            "Unexpected token received <" + std::to_string(token->s.token) + "> expected a reg_8 token",
            __FILE__, __LINE__);
        Ast::free_token(token);
        throw(e);
        break;
    }
    Ast::free_token(token);
}

std::int32_t Register8::getValue() const
{
    return this->reg;
}

std::string Register8::toString() const
{
    return "ast::Register;" + getRegisterName()  + " (" + std::to_string(this->reg) + ")";
}

uint8_t Register8::getMask() const
{
    return 0x07;
}

std::string Register8::getRegisterName() const
{
    return reg8_names.at(this->reg);
}

// --- Register16 class ---

static const std::vector<std::string> reg16_names = {
    "BC",
    "DE",
    "HL",
    "SP/AF",
};
Register16::Register16(YYSTYPE* token) : Register(token)
{
    switch (token->s.token)
    {
        case REG_BC:
        this->reg = 0;
        break;

        case REG_DE:
        this->reg = 1;
        break;
        
        case REG_HL:
        this->reg = 2;
        break;
        
        case REG_AF:
        case REG_SP:
        this->reg = 3;
        break;
        
        default:
        /* Unknown token received in constructor, fail. Loudly */
        AstException e(
            token,
            "Unexpected token received <" + std::to_string(token->s.token) + ">  expected a reg_16 token",
            __FILE__, __LINE__
            );
        Ast::free_token(token);
        throw(e);
        break;
    }
    Ast::free_token(token);
}

std::int32_t Register16::getValue() const
{
    return this->reg;
}

std::string Register16::toString() const
{
    return "ast::Register16;" + getRegisterName() + " (" + std::to_string(this->reg) + ")";
}

uint8_t Register16::getMask() const
{
    return 0x03;
}

std::string Register16::getRegisterName() const
{
    return reg16_names.at(this->reg);
}

} //ast
} //as85
