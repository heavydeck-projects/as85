#include <ast/opcode.hxx>

#include <cstdint>
#include <cassert>
#include <string>
#include <sstream>
#include <map>

#include <ast/exception.hxx>
#include <as85.hxx>

#include <i8085.tab.h>

namespace as85{
namespace ast{

/// @brief Maps token value with opcode name; Used for toString().
const std::map<const int, const std::string> Opcode::opcode_name = {
    {0,      "[UNK]"}, {OP_CC,   "CC"},   {OP_CM,   "CM"},   {OP_CP,   "CP"},
    {OP_CZ,   "CZ"},   {OP_DI,   "DI"},   {OP_EI,   "EI"},   {OP_IN,   "IN"},
    {OP_JC,   "JC"},   {OP_JM,   "JM"},   {OP_JP,   "JP"},   {OP_JZ,   "JZ"},
    {OP_RC,   "RC"},   {OP_RM,   "RM"},   {OP_RP,   "RP"},   {OP_RZ,   "RZ"},
    {OP_ACI,  "ACI"},  {OP_ADC,  "ADC"},  {OP_ADD,  "ADD"},  {OP_ADI,  "ADI"},
    {OP_ANA,  "ANA"},  {OP_ANI,  "ANI"},  {OP_CMA,  "CMA"},  {OP_CMC,  "CMC"},
    {OP_CMP,  "CMP"},  {OP_CNC,  "CNC"},  {OP_CNZ,  "CNZ"},  {OP_CPE,  "CPE"},
    {OP_CPI,  "CPI"},  {OP_CPO,  "CPO"},  {OP_DAA,  "DAA"},  {OP_DAD,  "DAD"},
    {OP_DCR,  "DCR"},  {OP_DCX,  "DCX"},  {OP_HLT,  "HLT"},  {OP_INR,  "INR"},
    {OP_INX,  "INX"},  {OP_JMP,  "JMP"},  {OP_JNC,  "JNC"},  {OP_JNZ,  "JNZ"},
    {OP_JPE,  "JPE"},  {OP_JPO,  "JPO"},  {OP_LDA,  "LDA"},  {OP_LXI,  "LXI"},
    {OP_MOV,  "MOV"},  {OP_MVI,  "MVI"},  {OP_NOP,  "NOP"},  {OP_ORA,  "ORA"},
    {OP_ORI,  "ORI"},  {OP_OUT,  "OUT"},  {OP_POP,  "POP"},  {OP_RAL,  "RAL"},
    {OP_RAR,  "RAR"},  {OP_RET,  "RET"},  {OP_RIM,  "RIM"},  {OP_RLC,  "RLC"},
    {OP_RNC,  "RNC"},  {OP_RNZ,  "RNZ"},  {OP_RPE,  "RPE"},  {OP_RPO,  "RPO"},
    {OP_RRC,  "RRC"},  {OP_RST,  "RST"},  {OP_SBB,  "SBB"},  {OP_SBI,  "SBI"},
    {OP_SIM,  "SIM"},  {OP_STA,  "STA"},  {OP_STC,  "STC"},  {OP_SUB,  "SUB"},
    {OP_SUI,  "SUI"},  {OP_XRA,  "XRA"},  {OP_XRI,  "XRI"},  {OP_CALL, "CALL"},
    {OP_LDAX, "LDAX"}, {OP_LHLD, "LHLD"}, {OP_PCHL, "PCHL"}, {OP_PUSH, "PUSH"},
    {OP_SHLD, "SHLD"}, {OP_SPHL, "SPHL"}, {OP_STAX, "STAX"}, {OP_XCHG, "XCHG"},
    {OP_XTHL, "XTHL"},
};

/// @brief Maps token value with opcode byte, without immediates.
//  These bytes will be bitwise-OR with their respective arguments if any.
const std::map<const int, const std::uint8_t> Opcode::opcode_byte = {
    {0,       0xFF}, {OP_CC,   0xDC}, {OP_CM,   0xFC}, {OP_CP,   0xF4},
    {OP_CZ,   0xCC}, {OP_DI,   0xF3}, {OP_EI,   0xFB}, {OP_IN,   0xDB},
    {OP_JC,   0xDA}, {OP_JM,   0xFA}, {OP_JP,   0xF2}, {OP_JZ,   0xCA},
    {OP_RC,   0xD8}, {OP_RM,   0xF8}, {OP_RP,   0xF0}, {OP_RZ,   0xC8},
    {OP_ACI,  0xCE}, {OP_ADC,  0x88}, {OP_ADD,  0x80}, {OP_ADI,  0xC6},
    {OP_ANA,  0xA0}, {OP_ANI,  0xE6}, {OP_CMA,  0x2F}, {OP_CMC,  0x3F},
    {OP_CMP,  0xB8}, {OP_CNC,  0xD4}, {OP_CNZ,  0xC4}, {OP_CPE,  0xEC},
    {OP_CPI,  0xFE}, {OP_CPO,  0xE4}, {OP_DAA,  0x27}, {OP_DAD,  0x09},
    {OP_DCR,  0x05}, {OP_DCX,  0x0B}, {OP_HLT,  0x76}, {OP_INR,  0x04},
    {OP_INX,  0x03}, {OP_JMP,  0xC3}, {OP_JNC,  0xD2}, {OP_JNZ,  0xC2},
    {OP_JPE,  0xEA}, {OP_JPO,  0xE2}, {OP_LDA,  0x3A}, {OP_LXI,  0x01},
    {OP_MOV,  0x40}, {OP_MVI,  0x06}, {OP_NOP,  0x00}, {OP_ORA,  0xB0},
    {OP_ORI,  0xF6}, {OP_OUT,  0xD3}, {OP_POP,  0xC1}, {OP_RAL,  0x17},
    {OP_RAR,  0x1F}, {OP_RET,  0xC9}, {OP_RIM,  0x20}, {OP_RLC,  0x07},
    {OP_RNC,  0xD0}, {OP_RNZ,  0xC0}, {OP_RPE,  0xE8}, {OP_RPO,  0xE0},
    {OP_RRC,  0x0F}, {OP_RST,  0xC7}, {OP_SBB,  0x98}, {OP_SBI,  0xDE},
    {OP_SIM,  0x30}, {OP_STA,  0x32}, {OP_STC,  0x37}, {OP_SUB,  0x90},
    {OP_SUI,  0xD6}, {OP_XRA,  0xA8}, {OP_XRI,  0xEE}, {OP_CALL, 0xCD},
    {OP_LDAX, 0x0A}, {OP_LHLD, 0x2A}, {OP_PCHL, 0xE9}, {OP_PUSH, 0xC5},
    {OP_SHLD, 0x22}, {OP_SPHL, 0xF9}, {OP_STAX, 0x02}, {OP_XCHG, 0xEB},
    {OP_XTHL, 0xE3},
};

/// @brief Map contains the register/argument part of an opcode byte.
/// Most opcodes have no argument.
const std::map<const int, const std::uint8_t> Opcode::opcode_arg_mask{
    {0,       0x00}, {OP_CC,   0x00}, {OP_CM,   0x00}, {OP_CP,   0x00},
    {OP_CZ,   0x00}, {OP_DI,   0x00}, {OP_EI,   0x00}, {OP_IN,   0x00},
    {OP_JC,   0x00}, {OP_JM,   0x00}, {OP_JP,   0x00}, {OP_JZ,   0x00},
    {OP_RC,   0x00}, {OP_RM,   0x00}, {OP_RP,   0x00}, {OP_RZ,   0x00},
    {OP_ACI,  0x00}, {OP_ADC,  0x07}, {OP_ADD,  0x07}, {OP_ADI,  0x00},
    {OP_ANA,  0x07}, {OP_ANI,  0x00}, {OP_CMA,  0x00}, {OP_CMC,  0x00},
    {OP_CMP,  0x07}, {OP_CNC,  0x00}, {OP_CNZ,  0x00}, {OP_CPE,  0x00},
    {OP_CPI,  0x00}, {OP_CPO,  0x00}, {OP_DAA,  0x00}, {OP_DAD,  0x30},
    {OP_DCR,  0x38}, {OP_DCX,  0x00}, {OP_HLT,  0x00}, {OP_INR,  0x38},
    {OP_INX,  0x30}, {OP_JMP,  0x00}, {OP_JNC,  0x00}, {OP_JNZ,  0x00},
    {OP_JPE,  0x00}, {OP_JPO,  0x00}, {OP_LDA,  0x00}, {OP_LXI,  0x30},
    {OP_MOV,  0x3F}, {OP_MVI,  0x38}, {OP_NOP,  0x00}, {OP_ORA,  0x07},
    {OP_ORI,  0x00}, {OP_OUT,  0x00}, {OP_POP,  0x30}, {OP_RAL,  0x00},
    {OP_RAR,  0x00}, {OP_RET,  0x00}, {OP_RIM,  0x00}, {OP_RLC,  0x00},
    {OP_RNC,  0x00}, {OP_RNZ,  0x00}, {OP_RPE,  0x00}, {OP_RPO,  0x00},
    {OP_RRC,  0x00}, {OP_RST,  0x38}, {OP_SBB,  0x07}, {OP_SBI,  0x00},
    {OP_SIM,  0x00}, {OP_STA,  0x00}, {OP_STC,  0x00}, {OP_SUB,  0x07},
    {OP_SUI,  0x00}, {OP_XRA,  0x07}, {OP_XRI,  0x00}, {OP_CALL, 0x00},
    {OP_LDAX, 0x10}, {OP_LHLD, 0x00}, {OP_PCHL, 0x00}, {OP_PUSH, 0x30},
    {OP_SHLD, 0x00}, {OP_SPHL, 0x00}, {OP_STAX, 0x10}, {OP_XCHG, 0x00},
    {OP_XTHL, 0x00},
};



// --- Opcode class ---

Opcode::Opcode() : Bytes()
{
    this->token_value = 0;
}

Opcode::Opcode(YYSTYPE* token) : Bytes(token)
{
    this->token_value = token->s.token;
    Ast::free_token(token);

    //Raise exception if expected byte value is 0xFF
    if(Opcode::opcode_byte.at(this->token_value) == 0xFF)
    {
        std::stringstream ss;
        ss << "Unimplemented opcode <" << Opcode::opcode_name.at(this->token_value) << ">" << std::flush;
        throw as85::As85Exception(ss.str(), __FILE__, __LINE__);
    }
}

std::string Opcode::toString() const
{
    std::stringstream ss;
    ss << "ast::Opcode;";
    ss << Opcode::opcode_name.at(this->token_value);
    ss << std::flush;
    return ss.str();
}

std::vector<std::uint8_t> Opcode::getBytes() const
{
    std::vector<std::uint8_t> rv;
    rv.push_back(Opcode::opcode_byte.at(this->token_value));
    return rv;
}

// --- OpRST ---
OpRST::OpRST(YYSTYPE* token, std::uint8_t rst_vector) : Opcode(token)
{
    this->rst_vector = rst_vector;
    if(rst_vector > 7){
        throw AstException(token, "Bad RST vector: " + std::to_string(rst_vector), __FILE__, __LINE__);
    }
}

std::string OpRST::toString() const
{
    std::string rv = Opcode::toString();
    rv = rv + " " + std::to_string(this->rst_vector);
    return rv;
}

std::vector<std::uint8_t> OpRST::getBytes() const
{
    std::uint8_t op_byte = Opcode::getBytes().at(0);
    op_byte = op_byte | (this->rst_vector << 3);
    std::vector<std::uint8_t> rv;
    rv.push_back(op_byte);
    return rv;
}

// ------------------
// --- Decorators ---
// ------------------

// --- OpcodeRegisterDecorator ---

///@todo Delete this method when safe to do so.
OpcodeRegisterDecorator::OpcodeRegisterDecorator(Opcode* inner_op, Register* reg, int bit_position) : Opcode()
{
    this->reg_node     = std::shared_ptr<Register>(reg);
    this->opcode_node  = std::shared_ptr<Opcode>(inner_op);
    this->bit_position = bit_position;
    this->inner_constructor();
}

OpcodeRegisterDecorator::OpcodeRegisterDecorator(std::shared_ptr<Opcode> inner_op, std::shared_ptr<Register> reg, int bit_position) : Opcode()
{
    this->reg_node     = reg;
    this->opcode_node  = inner_op;
    this->bit_position = bit_position;
    this->inner_constructor();
}

void OpcodeRegisterDecorator::inner_constructor()
{
    if((!this->reg_node) || (!this->opcode_node)){
        throw AstException(*this, "Null node references given to constructor", __FILE__, __LINE__);
    }
    //Validate bit position
    switch (this->reg_node->getMask()){
        // Supported bitmasks
        case 3:   /* 2bit mask */
        if(this->bit_position > 6){
            throw AstException(
                *this->opcode_node,
                "Unsupported bit position; Expected not above 6, found: " + std::to_string(this->bit_position),
                __FILE__, __LINE__
            );
        }
        break;

        case 7:   /* 3bit mask */
        if(this->bit_position > 5){
            throw AstException(
                *this->opcode_node,
                "Unsupported bit position; Expected not above 5, found: " + std::to_string(this->bit_position),
                __FILE__, __LINE__
            );
        }
        break;

        // Unsupported bitmasks
        case 1:   /* 1bit mask */
        case 15:  /* 4bit mask */
        case 31:  /* 5bit mask */
        case 63:  /* 6bit mask */
        case 127: /* 7bit mask */
        default:  /* Anything else */
        throw AstException(*this->reg_node, "Unsupported bitmask " + std::to_string(this->reg_node->getMask()), __FILE__, __LINE__);
    }
}

std::string OpcodeRegisterDecorator::toString() const
{
    std::stringstream ss;
    ss << this->opcode_node->toString() << std::endl;
    ss << this->reg_node->toString();
    ss << std::flush;
    return ss.str();
}

Position OpcodeRegisterDecorator::getPosition() const
{
    auto pos_opcode = this->opcode_node->getPosition();
    auto pos_reg = this->reg_node->getPosition();

    return pos_reg.merge(pos_opcode);
}

//This getbytes takes the byte vector from the opcode, and logical-OR the
//register value at the given position
std::vector<std::uint8_t> OpcodeRegisterDecorator::getBytes() const
{
    std::vector<std::uint8_t> rv;
    std::vector<std::uint8_t> original_bytes = this->opcode_node->getBytes();
    //Update the opcode byte
    {
        std::uint8_t opcode_byte = original_bytes.at(0);
        std::uint8_t register_bits = (this->reg_node->getValue() & (0x07)) << this->bit_position;
        std::uint8_t mask_bits = this->reg_node->getMask() << this->bit_position;

        //Make sure there are no set bits on the place where the register
        //will be OR-ed in.
        if(opcode_byte & mask_bits){
            std::string msg = 
                "Register bits are already set before OR-in register; Bits " +
                std::to_string(this->bit_position) + " to " + std::to_string(this->bit_position + 2);
            ;
            //Add information about its child nodes
            msg += "\n\tOp:\t" + this->opcode_node->toString();
            msg += "\n\tReg:\t" + this->reg_node->toString();
            throw AstException(*this->reg_node, msg, __FILE__, __LINE__);
        }

        //OR-in the register value
        opcode_byte = opcode_byte | register_bits;
        rv.push_back(opcode_byte);
    }
    //Append the rest of the bytes
    rv.insert(rv.cend(), original_bytes.cbegin() + 1, original_bytes.cend());

    return rv;
}

}//ast
}//as85
