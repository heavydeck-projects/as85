#include <ast/exception.hxx>
#include <sstream>
#include <ast/ast.hxx>

#include <i8085.tab.h>

namespace as85{
namespace ast{

AstException::AstException(const YYSTYPE* token, const std::string &message, const std::string &file, int line) : As85Exception("", file, line)
{
    std::stringstream ss;
    ss << "ast::AstException ";
    if(token){
        ss << std::to_string(token->s.row) << " :" << std::to_string(token->s.col);
        if(token->s.text){
            ss << " near: '" << token->s.text << "'";
        }
        if(message.length()){
            ss << " -- " << message;
        }
    }
    else{
        ss << "<Missing token> -- " << message;
    }
    ss << std::flush;
    this->message = this->location_str() + ss.str();
}

AstException::AstException(const Ast& node, const std::string &message, const std::string &file, int line) : As85Exception("", file, line)
{
    auto position = node.getPosition();
    std::stringstream ss;
    ss << "ast::AstException ";
    ss << std::to_string(position.begin.row) << " :" << std::to_string(position.begin.col);
    if(message.length()){
        ss << " -- " << message;
    }
    ss << std::flush;
    this->message = this->location_str() + ss.str();
}

} //ast
} //as85