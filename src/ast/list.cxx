#include <ast/list.hxx>
#include <list>
#include <cassert>

namespace as85 {
namespace ast {

// ------------------------
// --- Ast list classes ---
// ------------------------

// --- AstList ---
AstList::AstList() : Ast() {}
std::string AstList::toString() const
{
    return "ast::AstList";
}

// --- StatementList ---
StatementList::StatementList() : AstList() {}
std::string StatementList::toString() const
{
    return "ast::StatementList";
}

} //ast
} //as85
