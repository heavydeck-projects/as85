#include <ast/ast.hxx>
#include <i8085.tab.h>

#include <ast/visitor/visitor.hxx>
#include <ast/exception.hxx>

#include <sstream>
#include <iomanip>
#include <stdexcept>
#include <cassert>

//#include <iostream>

namespace as85{
namespace ast{

// ----------------------
// --- Position class ---
// ----------------------

Position Position::merge(const Position& p) const
{
    Position rv;
    rv.begin.row = std::min<size_t>(this->begin.row, p.begin.row);
    rv.begin.col = std::min<size_t>(this->begin.col, p.begin.col);
    rv.end.row = std::max<size_t>(this->end.row, p.end.row);
    rv.end.col = std::max<size_t>(this->end.col, p.end.col);
    return rv;
}

bool Position::operator<(const Position &rhs) const{
    //If begin rows differ, we can know which one is less.
    if (this->begin.row < rhs.begin.row) return true;
    if (this->begin.row > rhs.begin.row) return false;
    //If begin rows are equal, check if the columns differ
    if (this->begin.col < rhs.begin.col) return true;
    if (this->begin.col > rhs.begin.col) return false;

    //If begin col/row are the same, compare the end row
    if (this->end.row < rhs.end.row) return true;
    if (this->end.row > rhs.end.row) return false;
    //If they are equal, compare the end 
    if (this->end.col < rhs.end.col) return true;
    if (this->end.col > rhs.end.col) return false;
    //If everything is equal... It is equal. Duh xP
    return false;
}

bool Position::operator==(const Position &rhs) const{
    return
        (this->begin.row == rhs.begin.row) &&
        (this->begin.col == rhs.begin.col) &&
        (this->end.row == rhs.end.row) &&
        (this->end.col == rhs.end.col)
    ;
}

bool Position::operator<=(const Position &rhs) const { return  (*this == rhs) || (*this < rhs); }
bool Position::operator>(const Position &rhs) const  { return !(*this <= rhs); }
bool Position::operator>=(const Position &rhs) const { return !(*this <  rhs); }
bool Position::operator!=(const Position &rhs) const { return !(*this == rhs); }

// -----------------
// --- Ast class ---
// -----------------

Ast::Ast()
{
    this->position = {{0,0},{0,0}};
}


Ast::Ast(const YYSTYPE *token)
{
    assert(token);
    //Store token position
    this->position.begin.row = token->s.row;
    this->position.begin.col = token->s.col;
    this->position.end.row = token->s.row;
    this->position.end.col = token->s.col + token->s.size;

    assert(this->position.begin.row <= this->position.end.row);
    assert(this->position.begin.col <= this->position.end.col);
}

Ast::~Ast()
{
    
}


Position Ast::getPosition() const
{
    Position rv;
    if(this->children.size() == 0){
        rv = this->position;
    }
    else if(this->children.size() == 1){
        rv = children.front()->getPosition();
    }
    else{
        //Else we set the begin to the first child begin
        rv.begin = children.front()->getPosition().begin;
        //And end to the last child end
        rv.end = children.back()->getPosition().end;
    }
    return rv;
}

void Ast::addChildBack(Ast* node)
{
    auto ref = node->getSelfRef().lock();
    assert(ref);
    addChildBack(ref);
}

void Ast::addChildBack(std::shared_ptr<Ast> node)
{
    this->children.push_back(node);
    node->wparent = this->getSelfRef();
    update_parent(*node);
}

void Ast::addChildFront(Ast* node)
{
    auto ref = node->getSelfRef().lock();
    assert(ref);
    addChildFront(ref);
}

void Ast::addChildFront(std::shared_ptr<Ast> node)
{
    this->children.insert(this->children.begin(), node);
    node->wparent = this->getSelfRef();
    update_parent(*node);
}


std::shared_ptr<Ast> Ast::extractChild(size_t position)
{
    auto rv =  this->children.at(position);
    auto erase_iter = this->children.begin() + position;
    this->children.erase(erase_iter);
    update_parent(*rv, true);
    return rv;
}

std::shared_ptr<Ast> Ast::extractChild(const Ast &n)
{
    for (size_t i = 0; i < this->children.size(); i++) {
        if (this->children.at(i).get() == &n) {
            auto child = extractChild(i);
            update_parent(*child, true);
            return child;
        }
    }
    return std::shared_ptr<Ast>();
}

std::shared_ptr<Ast> Ast::extract()
{
    auto parent = this->wparent.lock();
    if(!parent){
        throw AstException(
            *this,
            "Cannot extract AST root nodes or AST parent reference is inconsistent.",
            __FILE__, __LINE__
        );
    }
    return parent->extractChild(*this);
}

std::vector <std::shared_ptr<Ast>> Ast::getChildren() const
{
    return this->children;
}

size_t Ast::getChildCount() const
{
    return this->children.size();
}

std::weak_ptr<Ast> Ast::getSelfRef()
{
    auto rv = this->shared_from_this();
    assert(rv);
    return rv;
}

std::shared_ptr<Ast> Ast::getParent() const
{
    return wparent.lock();
}

void Ast::setParent(std::weak_ptr<Ast> ref)
{
    this->wparent = ref;
}

void Ast::setParent(std::shared_ptr<Ast> ref)
{
    this->wparent = std::weak_ptr<Ast>(ref);
}

void Ast::accept(visitor::Visitor &v)
{
    v.visit(*this);
}

void Ast::accept(visitor::ConstVisitor &v) const
{
    v.visit(*this);
}

std::string Ast::toString() const
{
    return "ast::Ast";
}

Ast* operator<<(Ast* parent, std::shared_ptr<Ast> child_ref)
{
    // Make sure at least during testing that even when using the unsafe Ast*
    // variant that the underlying object can get a proper shared_ptr.
    assert(parent->getSelfRef().lock());

    parent->addChildBack(child_ref);
    return parent;
}

Ast& operator<<(Ast& parent, std::shared_ptr<Ast> child_ref)
{
    // Same as with the Ast* variant, make sure the object can get a shared_ptr
    assert(parent.getSelfRef().lock());

    parent.addChildBack(child_ref);
    return parent;
}


std::shared_ptr<Ast> operator<<(std::shared_ptr<Ast> parent_ref, std::shared_ptr<Ast> child_ref)
{
    parent_ref->addChildBack(child_ref);
    return parent_ref;
}

/// A single point on which to correctly free() lexer tokens.
void Ast::free_token(YYSTYPE* token){
    if(token){
        if(token->s.text){
            //std::cerr << "-" << std::flush << token->s.text << std::endl;
            //Remove the const-ness. Trust me, it's safe.
            free((void*)token->s.text);
            token->s.text = nullptr;
        }
    }
}

void Ast::update_parent(Ast& node, bool invalidate)
{
    if(invalidate){
        node.wparent.reset();
    }
    else{
        node.wparent = this->getSelfRef();
    }
}

/// Used when removing nodes, if node may become a leaf,
/// make sure the stored position is correct.
void Ast::update_position()
{
    this->position = this->getPosition();
}

// ----------------------
// --- TextNode class ---
// ----------------------
TextNode::TextNode(std::string text) : Ast()
{
    this->text = text;
}
TextNode::TextNode(YYSTYPE *token) : Ast(token)
{
    if(token->s.text){
        this->text = std::string(token->s.text);
    }
    Ast::free_token(token);
}
void TextNode::setText(const std::string &text)
{
    this->text = text;
}
std::string TextNode::toString() const
{
    std::stringstream ss;
    ss << this->text << std::flush;
    return ss.str();
}

// ---------------------------
// --- AstAnnotation class ---
// ---------------------------
AstAnnotation::AstAnnotation(std::string text) : TextNode(text)
{}

AstAnnotation::AstAnnotation(YYSTYPE *token) : TextNode(token)
{}

// ----------------------
// --- AstValue class ---
// ----------------------

AstValue::AstValue(const YYSTYPE* token) : Ast(token) {}
AstValue::AstValue() : Ast() {}

// ---------------
// --- Integer ---
// ---------------

Integer::Integer(YYSTYPE* token) : AstValue(token)
{
    assert(token->s.text);
    //Parse the value inside the token text
    std::string num_str(token->s.text);
    switch(token->s.token){
        case DECIMAL_CONSTANT:
        this->value = std::stoi(num_str, nullptr, 10);
        break;

        case HEXADECIMAL_CONSTANT:
        this->value = std::stoll(num_str, nullptr, 0);
        break;

        case OCTAL_CONSTANT:
        this->value = std::stoi(num_str, nullptr, 0);
        break;

        case BINARY_CONSTANT:
        //Remove heading 0b before parsing
        num_str = num_str.substr(2, std::string::npos);
        this->value = std::stoi(num_str, nullptr, 2);
        break;
    }

    //Free token
    Ast::free_token(token);
}

std::int32_t Integer::getValue() const
{
    return this->value;
}

std::string Integer::toString() const
{
    std::stringstream ss;
    ss << "ast::Integer;";
    ss << "0x" ;
    ss << std::setbase(16) << this->value;
    ss << std::setbase(10) << " (" << this->value << ")";
    ss << std::flush;
    return ss.str();
}

// -------------
// --- Label ---
// -------------

Label::Label(YYSTYPE* token) : AstValue(token)
{
    if(token->s.text == nullptr){
        throw AstException(*this, "Label won't accept a text-free token", __FILE__, __LINE__);
    }
    this->identifier = std::string(token->s.text);
    Ast::free_token(token);
}

std::int32_t Label::getValue() const
{
    return 0x55AA;
}

std::string Label::toString() const
{
    std::stringstream ss;
    ss << "ast::Label;";
    ss << "'" << this->identifier << "'";
    ss << std::flush;
    return ss.str();
}

std::string Label::getIdentifier() const
{
    return this->identifier;
}

}//ast
}//as85
