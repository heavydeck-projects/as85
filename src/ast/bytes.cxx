#include <ast/bytes.hxx>
#include <ast/exception.hxx>

#include <sstream>
#include <list>
#include <cassert>

#include <i8085.tab.h>

namespace as85{
namespace ast {

// --- Bytes class ---

Bytes::Bytes() : Ast()
{

}

Bytes::Bytes (const YYSTYPE* token) : Ast(token)
{

}

std::string Bytes::toString() const
{
    return "ast::Bytes";
}

std::vector<uint8_t> Bytes::getBytes() const
{
    std::vector<uint8_t> rv;
    for(auto e : this->children)
    {
        auto child = std::dynamic_pointer_cast<Bytes>(e);
        if(!child){
            throw AstException(*e, "Bytes node children must be of Bytes class.", __FILE__, __LINE__);
        }
        auto child_bytes = child->getBytes();
        rv.insert(rv.end(), child_bytes.cbegin(), child_bytes.cend());
    }
    return rv;
}


// --- String class ---

String::String() : Bytes()
{
    this->string_type = StringType::STR_DEFAULT;
}

String::String(YYSTYPE* token, StringType type) : Bytes(token)
{
    this->string_literal = std::string(token->s.text);
    this->string_type = type;
    Ast::free_token(token);
    //Convert string into bytes
    this->parseString();
}

std::string String::toString() const
{
    return "ast::String;" + this->string_literal;
}

std::vector<uint8_t> String::getBytes() const
{
    return this->string_bytes;
}

std::string String::getString() const
{
    return this->string_literal;
}

void String::parseString()
{
    this->string_bytes.clear();
    ///@todo Actually parse the string literal.
    //Parse the string and its escape sequences.
    enum parse_state_t{
        SS_CHARACTER = 0,
        SS_EXPECT_ESCAPE,
        SS_HEX,
        SS_OCT
    } parse_state = SS_CHARACTER;
    std::string number_str;
    for(size_t i = 0; i < string_literal.length(); i++){
        const char &c = this->string_literal.at(i);
        switch (parse_state)
        {
        case SS_HEX:
            //hex parsing expects exactly 2 hex characters
            number_str = number_str + c;
            if(number_str.size() == 2){
                //If 2 characters, parse the byte
                this->string_bytes.push_back((uint8_t)std::stoi(number_str, nullptr, 16));
                //Back to regular character mode
                parse_state = SS_CHARACTER;
            }
            //otherwise, get another character
            break;

        case SS_EXPECT_ESCAPE:
            //abfnrtv'"\ match C's escape sequences
            if     (c == '\\'){ this->string_bytes.push_back('\\'); parse_state = SS_CHARACTER; break; }
            else if(c == 'a') { this->string_bytes.push_back('\a'); parse_state = SS_CHARACTER; break; }
            else if(c == 'b') { this->string_bytes.push_back('\b'); parse_state = SS_CHARACTER; break; }
            else if(c == 'f') { this->string_bytes.push_back('\f'); parse_state = SS_CHARACTER; break; }
            else if(c == 'n') { this->string_bytes.push_back('\n'); parse_state = SS_CHARACTER; break; }
            else if(c == 'r') { this->string_bytes.push_back('\r'); parse_state = SS_CHARACTER; break; }
            else if(c == 't') { this->string_bytes.push_back('\t'); parse_state = SS_CHARACTER; break; }
            else if(c == 'v') { this->string_bytes.push_back('\v'); parse_state = SS_CHARACTER; break; }
            else if(c == '\''){ this->string_bytes.push_back('\''); parse_state = SS_CHARACTER; break; }
            else if(c == '"') { this->string_bytes.push_back('"');  parse_state = SS_CHARACTER; break; }
            //An X activates hex mode
            else if((c == 'x') || (c == 'X')) { number_str = ""; parse_state = SS_HEX; break; }
            //0-7 activates octal mode
            else if((c >= '0') && (c <= '7')) { number_str = "";  parse_state = SS_OCT; /* Fall-through */ }
            ///@bug Unknown escape should raise a warning.
            else   { this->string_bytes.push_back(c); break; }
            /* --- Octal case intentionally falls through --- */
            [[fallthrough]];

        case SS_OCT:
        {
            //If next character is not octal parse what you have
            //and signal we need to fall-through to regular character
            bool fall_through = false;
            bool is_octal = ((c>='0') || (c<='7'));
            bool is_last  = (i == (string_literal.length() - 1));
            if(!is_octal){ fall_through = true; }
            //Otherwise, add character to number str
            else                  { number_str += c; }

            //If string is 3ch long OR is the last char OR fall_through happens, parse the number.
            if(is_last || fall_through || (number_str.length() == 3)){
                this->string_bytes.push_back((uint8_t)std::stoi(number_str, nullptr, 8));
                //Back to regular character mode
                parse_state = SS_CHARACTER;
            }
            //If not fall_through, continue loop
            if(!fall_through)
                continue;
            [[fallthrough]];
        }
        // --- Intentional case fall-through ---

        default: /* SS_CHARACTER */
            assert(parse_state == SS_CHARACTER);
            //If not a `\` character, just add it to the output
            if(c != '\\')
                this->string_bytes.push_back(c);
            //If a `\` character is found, next character is an escape sequence
            else
                parse_state = SS_EXPECT_ESCAPE;
            break;
        }
    }
    //Add null termination if needed
    if(this->string_type == StringType::STR_NULL_TERMINATED){
        this->string_bytes.push_back(0x00);
    }
}


// --- Word class ---

Word::Word(int word_bits, int format) : Bytes()
{
    this->format    = format;
    this->word_bits = word_bits;
}

std::string Word::toString() const
{
    std::stringstream ss;
    ss << "ast::Word;(";
    switch(this->word_bits){
        case 8:
        ss << "8"; break;
        case 16:
        ss << "16"; break;
        case 24:
        ss << "24"; break;
        case 32:
        ss << "32"; break;
        default:
        ss << this->word_bits << "?"; break;
    }
    switch(this->format){
        case Word::FMT_LITTLE:
        ss << "LE"; break;
        case Word::FMT_BIG:
        ss << "BE"; break;
        default:
        ss << "?"; break;
    }
    ss << ")" << std::flush;
    return ss.str();
}

std::vector<uint8_t> Word::getBytes(std::int32_t value, int word_bits, int format)
{
    std::vector<uint8_t> rv;
    size_t bytes;
    switch (word_bits)
    {
        case 8:
        bytes = 1;
        break;

        case 16:
        bytes = 2;
        break;
        
        case 24:
        bytes = 3;
        break;
        
        case 32:
        bytes = 4;
        break;

        default:
        throw AstException(nullptr, "Invalid word format " + std::to_string(format), __FILE__, __LINE__);
    }

    //Extract all bytes, MSB first
    std::list<uint8_t> all_bytes_big;
    for(int i = 1; i <= 4; i++){
        uint8_t b = value >> (8 * (4 - i));
        all_bytes_big.push_back(b);
    }

    //Put them into rv in the correct endianness and truncation.
    std::list<uint8_t> out_bytes;
    bool is_little = (format) == WordFormat::FMT_LITTLE;
    while(bytes){
        if(is_little){
            out_bytes.push_back(all_bytes_big.back());
            all_bytes_big.pop_back();
        }
        else{
            out_bytes.push_front(all_bytes_big.back());
            all_bytes_big.pop_back();
        }
        bytes--;
    }
    rv.insert(rv.begin(), out_bytes.begin(), out_bytes.end());
    return rv;
}

std::vector<uint8_t> Word::getBytes() const
{   
    auto underlying_value = std::dynamic_pointer_cast<AstValue>(this->children.at(0));
    try{
        return Word::getBytes(underlying_value->getValue(), this->word_bits, this->format);
    }
    catch(AstException &e){
        //Rethrow with line object instance
        throw AstException(*this, "Invalid word format " + std::to_string(format), __FILE__, __LINE__);
    }
}

// --- WordArray class ---

WordArray::WordArray(int word_bits, int format) : Bytes()
{
    this->word_bits = word_bits;
    this->format = format;
}

std::vector<uint8_t> WordArray::getBytes() const
{
    std::vector<uint8_t> rv;
    //Find the value list node
    std::shared_ptr<AstList> values;
    for(const auto &node : this->children){
        values = std::dynamic_pointer_cast<AstList>(node);
        if(values) break;
    }
    //Check that we found it
    if(!values){
        throw AstException(*this, "WordArray requires a list of values under it.", __FILE__, __LINE__);
    }
    //Iterate over the values and append them to rv
    for(const auto &value : values->getChildren()){
        auto word_bytes = Word::getBytes(
            std::dynamic_pointer_cast<AstValue>(value)->getValue(),
            this->word_bits,
            this->format
        );
        rv.insert(rv.end(), word_bytes.begin(), word_bytes.end());
    }
    return rv;
}

std::string WordArray::toString() const
{
    std::stringstream ss;
    ss << "ast::WordArray;(";
    switch(this->word_bits){
        case 8:
        ss << "8"; break;
        case 16:
        ss << "16"; break;
        case 24:
        ss << "24"; break;
        case 32:
        ss << "32"; break;
        default:
        ss << this->word_bits << "?"; break;
    }
    switch(this->format){
        case Word::FMT_LITTLE:
        ss << "LE"; break;
        case Word::FMT_BIG:
        ss << "BE"; break;
        default:
        ss << "?"; break;
    }
    ss << ")" << std::flush;
    return ss.str();
}

}
}