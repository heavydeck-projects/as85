#include <ast/visitor/visitor.hxx>
#include <ast/list.hxx>
#include <ast/warning.hxx>
#include <ast/error.hxx>
#include <ast/exception.hxx>
#include <fstream>
#include <sstream>

#include <cassert>

#include <list>

namespace as85{
namespace ast{
namespace visitor{
// ------------------------------------
// --- Visitor/ConstVisitor classes ---
// ------------------------------------

Visitor::Visitor() {}
Visitor::~Visitor() {}

ConstVisitor::ConstVisitor() {}
ConstVisitor::~ConstVisitor() {}



// --------------------------------
// --- AstGraphvizVisitor class ---
// --------------------------------

static const std::string font_node_default("Noto serif, serif");
static const std::string font_leaf_default("Noto mono, courier, monospace");

AstGraphviz::AstGraphviz()
{
    this->is_negative = false;
    this->show_lines = false;
}
AstGraphviz::~AstGraphviz() {}

void AstGraphviz::setNegative(bool negative)
{
    this->is_negative = negative;
}


void AstGraphviz::enablePosition(bool show_lines)
{
    this->show_lines = show_lines;
}

void AstGraphviz::visit(const Ast& ast)
{
    //Format the current node
    std::string node_text = this->format_node(ast);

    //If current node has no childs, add format string to "leaves"
    if(ast.getChildCount() == 0)
    {
        this->leaves.push_back(node_text);
    }
    else{
        //Add child to "nodes"
        this->nodes.push_back(node_text);

        //Then Visit every children
        for (auto node : ast.getChildren()) {
            node->accept(*this);
            //And add the relation to "relations"
            this->edges.push_back(format_link(ast, *node));
        }
    }
}

std::string AstGraphviz::node_name(const Ast &node) const
{
    std::string rv = "astnode_" + std::to_string((uint64_t) &node);
    return rv;
}

std::string AstGraphviz::format_link(const Ast &from, const Ast &to) const
{
    std::string rv = node_name(from) + " -> " + node_name(to) + ";";
    return rv;
}

std::string AstGraphviz::format_node(const Ast &node) const
{
    //Default values for any nondescript node
    std::string fillcolor = "";
    std::string fontcolor = "";
    std::string shape     = "";
    std::string style     = "";
    std::string fontname  = "";

    std::string node_label_orig = node.toString();
    std::string node_label;

    //Add linenumbers if enabled
    if(this->show_lines){
        auto pos = node.getPosition();
        std::stringstream ss;
        ss << node_label_orig;
        ss << "\n(" << std::to_string(pos.begin.row) << ":" << std::to_string(pos.begin.col);
        if( (pos.begin.row != pos.end.row) || (pos.begin.col != pos.end.col) ){
            ss << "→" << std::to_string(pos.end.row) << ":" << std::to_string(pos.end.col);
        }
        ss << ")";
        ss << std::flush;
        node_label_orig = ss.str();
    }

    //Escape quotes & other sus characters
    for(auto c : node_label_orig)
    {
        if(c == '"'){
            node_label.push_back('\\');
            node_label.push_back('"');
        }
        else if(c == '\n'){
            node_label.push_back('\\');
            node_label.push_back('n');
        }
        //Convert ';' into newlines.
        else if(c == ';'){
            node_label.push_back('\\');
            node_label.push_back('n');
        }
        else{
            node_label.push_back(c);
        }
    }

    //Modify defaults depending on node type
    if (dynamic_cast<const Error*>(&node)){
        //Paint errors red; And always use non-monospace font.
        fillcolor = "tomato";
        fontname = font_node_default;
    }
    else if (dynamic_cast<const Warning*>(&node)){
        fillcolor = "yellow";
    }
    else if (dynamic_cast<const AstList*>(&node)){
        fillcolor = "gray50";
    }

    //Nodes without parent, make them stand-out in gold.
    //Only the root node should be this color.
    if(node.getParent().get() == nullptr){
        fillcolor="goldenrod";
    }

    //Build the node record
    std::stringstream ss;
    ss << node_name(node);
    ss << "[ " ;
        
        if(style.length())     { ss << "style=" << style << " "; }
        if(shape.length())     { ss << "shape=" << shape << " "; }
        if(fontname.length())  { ss << "fontname=\"" << fontname << "\" "; }
        if(fillcolor.length()) { ss << "fillcolor=" << fillcolor << " "; }
        if(fontcolor.length()) { ss << "fontcolor=" << fontcolor << " "; }

        ss << "label=\"" << node_label << "\" ";
    ss << "]" ;
    ss << std::flush;
    
    return ss.str();
}

void AstGraphviz::writeGraphviz(std::string path) const
{
    std::ofstream f(path.c_str());

    //dotfile header
    f << "digraph {" << std::endl;
    f << "ranksep = 1;" << std::endl;
    f << "ordering=out;" << std::endl;
    f << "bgcolor=transparent;" << std::endl;

    if(this->is_negative){
        f << "edge [ ";
            f << "color=white ";
        f << " ];" << std::endl;

        f << "node [";
            f << "shape = box ";
            f << "style = filled ";
            f << "fillcolor = black ";
            f << "fontcolor = white ";
            f << "color = white ";
            f << "fontname = \"" + font_node_default + "\"";
        f << "];" << std::endl;
    }
    else{
        f << "edge [ ";
            f << "color=\"\" ";
        f << " ];" << std::endl;

        f << "node [";
            f << "shape = box ";
            f << "style = filled ";
            f << "fillcolor = white ";
            f << "fontcolor = black ";
            f << "color = black ";
            f << "fontname = \"" + font_node_default + "\"";
        f << "];" << std::endl;
    }

    //All strings on "nodes"
    {
        for(std::string s : this->nodes){
            f << s << std::endl;
        }
    }

    //All strings on "leaves"
    {
        f << "{" << std::endl;
        f << "rank = sink;" << std::endl;
        //Leaves are monospaced by default
        f << "node [ fontname = \"" << font_leaf_default << "\"];" << std::endl;
        for(std::string s : this->leaves){
            f << s << std::endl;
        }
        f << "}" << std::endl;
    }

    //All edges
    {
        for(std::string s : this->edges){
            f << s << std::endl;
        }
    }

    //dotfile tail
    f << "}" << std::endl;
}

// ------------------------------------
// --- AstParentSetterVisitor clas  ---
// ------------------------------------
AstParentSetter::AstParentSetter() : Visitor() {}
AstParentSetter::~AstParentSetter() {}
void AstParentSetter::visit(Ast& ast)
{
    if(ast.getSelfRef().expired())
    {
        throw AstException(ast, "Node has invalid self reference.", __FILE__, __LINE__);
    }
    for (auto node : ast.getChildren()) {
        node->setParent(ast.getSelfRef());
        node->accept(*this);
    }
}

// ---------------------------------
// --- AstIntegrityVisitor class ---
// ---------------------------------
AstIntegrity::AstIntegrity() : Visitor(), all_ok(true){}
AstIntegrity::~AstIntegrity() {}
void AstIntegrity::visit(Ast& ast)
{
    //Check the self reference
    this->all_ok = this->all_ok && (&ast == ast.getSelfRef().lock().get());

    //Check all the children
    for(const auto &child : ast.getChildren()){
        this->all_ok = this->all_ok &&
            (child->getParent().get() == &ast);
        child->accept(*this);
    }
}

bool AstIntegrity::isOk() const
{
    return this->all_ok;
}


// -----------------------------------
// --- EmptyStatementCleaner class ---
// -----------------------------------

EmptyStatementCleaner::EmptyStatementCleaner() : Visitor(){}
void EmptyStatementCleaner::visit(Ast &ast)
{
    //Check if the ast node is a StatementList-like object
    StatementList* l = dynamic_cast<StatementList*> (&ast);
    if(l){
        //If this node is an statement list
        //Find all empty statements
        std::list<std::shared_ptr<Ast>> empty_statements;
        for(auto child : ast.getChildren()){
            if(child->getChildCount() == 0){
                empty_statements.push_back(child);
            }
        }
        //Remove them
        for(auto statement : empty_statements){
            ast.extractChild(*statement);
        }
    }

    //recurse on the children
    for (auto n : ast.getChildren()) {
        n->accept(*this);
    }
}

// ---------------------------
// --- ListFlattener class ---
// ---------------------------
ListFlattener::ListFlattener() : Visitor()
{
    this->shallow = false;
}

void ListFlattener::setShallow(bool shallow)
{
    this->shallow = shallow;
}

/// @brief Flattens list-like syntax rules on the AST.
/// Converts list-like syntax productions where a list node holds at most one
/// element, into a single list-like element with multiple children.
/// @param ast The syntax tree
void ListFlattener::visit(Ast &ast)
{
    //Check if the ast node is a list-like object
    AstList* l = dynamic_cast<AstList*> (&ast);
    if(l){
        //If this node is a list, flatten it before recursing down.
        this->flatten(static_cast<AstList&> (ast));

        //If a shallow flattening is requested, do not recurse down
        //and return instead
        if (this->shallow) {
            return;
        }
    }

    //recurse on the children
    for (auto n : ast.getChildren()) {
        n->accept(*this);
    }
}

void ListFlattener::flatten(AstList& ast)
{
    //When first called by visit() the `ast` node points to the top node
    //of the list which contains the LAST element of the complete list.
    // To flatten the list we must reach the list node with a single child
    //and merge it upwards until we reach the list top.

    //Find the first element on the list
    std::shared_ptr<Ast> last    = ast.getSelfRef().lock();
    std::shared_ptr<Ast> current = ast.getSelfRef().lock();
    while(current->getChildCount() > 1){
        current = (current->getChildren().at(0));
    }

    //Flatten the list going bottom-up by extracting all nodes and
    //saving them on a list until we reach first == ast
    std::list<std::shared_ptr<Ast>> list_elements;
    while(current != last)
    {
        //Take current node children
        auto children = current->getChildren();
        //Save them onto the list_elements temp list
        for(auto child : children){
            list_elements.push_back(child);
        }

        //Extract myself from the parent node
        auto next_current = current->getParent();
        current->getParent()->extractChild(*current);

        //Move the current node up one level
        assert(next_current);
        current = next_current;

        //Let 'myself' go out of scope, deleting the dangling list object.
    }

    //put all the saved children on this node
    for(auto child : list_elements){
        ast << child;
    }
}

} //visitor
} //ASt
} //as85