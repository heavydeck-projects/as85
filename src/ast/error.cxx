#include <ast/error.hxx>
#include <map>
#include <stdexcept>

namespace as85{
namespace ast{

// -------------------
// --- Error class ---
// -------------------
Error::Error(std::string text) : AstAnnotation(text) { }

Error::Error(YYSTYPE *token, ErrorId id) : AstAnnotation(token)
{
    //Given a token, we store its contents for reporting them even if text is
    //overriden by setText()
    token_text = TextNode::toString();

    //If an error id is given, get its default description
    if(id != ErrorId::ERRID_UNDEF){
        TextNode::setText(Error::getErrorString(id));
    }
    else{
        TextNode::setText("");
    }
}

std::string Error::toString() const
{
    std::string rv;
    rv = std::to_string(this->position.begin.row) + ":" + std::to_string(this->position.begin.col);
    if(token_text.size() > 0){
        //If we have a token stored, preface the error message with its position and text
        rv = rv + " near: '" + token_text + "'";
    }
    rv += " -- ";
    rv += TextNode::toString();
    return rv;
}

// ----------------------------------
// --- AST parsing error messages ---
// ----------------------------------

static const std::map<ErrorId, std::string> error_messages = {
    {ErrorId::ERRID_BAD_ENCODING,  "Bad encoding. Expected UTF-8/ANSI; Found UTF-16/UTF-32."},
    {ErrorId::ERRID_BAD_STATEMENT, "Invalid statement"},

    {ErrorId::ERRID_EXPECTED_IDENTIFIER,   "Expected an identifier"},
    {ErrorId::ERRID_EXPECTED_VALUE,        "Expected a value"},
    {ErrorId::ERRID_EXPECTED_VALUE_OR_ID,  "Expected a value or identifier"},
    {ErrorId::ERRID_EXPECTED_VALUE_LIST,   "Expected a list of values"},
    {ErrorId::ERRID_EXPECTED_STRING,       "Expected a string literal"},
    {ErrorId::ERRID_EXPECTED_SECTION_NAME, "Expected a section name"},
    {ErrorId::ERRID_EXPECTED_REG8,         "Expected an 8bit register or MEM"},
    {ErrorId::ERRID_EXPECTED_REG16,        "Expected BC DE HL or SP"},
    {ErrorId::ERRID_EXPECTED_REG_STACK,    "Expected BC DE HL or AF"},
    {ErrorId::ERRID_EXPECTED_RST_VECTOR,   "Expected a valid RST vector [0-7]"},
    {ErrorId::ERRID_EXPECTED_WORD_SIZE,    "Expected word bit size [8,16,24,32]"},

    {ErrorId::ERRID_UNDEF, "UNDEFINED ERROR"},
};

std::string Error::getErrorString(ErrorId id)
{
    try{
        return error_messages.at(id);
    }
    catch(std::out_of_range &e){
        (void)e;
        return error_messages.at(ERRID_UNDEF);
    }
}

}//ast
}//as85
