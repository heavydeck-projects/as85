#include <ast/statement.hxx>

#include <list>

namespace as85{
namespace ast {

// --- Classes ---

Statement::Statement() : Ast() {}
std::string Statement::toString() const
{
    return "ast::Statement";
}

} //ast
} //as85