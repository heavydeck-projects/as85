#include <ast/directive.hxx>
#include <ast/exception.hxx>
#include <ast/opcode.hxx>

namespace as85{
namespace ast{

// -----------------------
// --- Directive class ---
// -----------------------

Directive::Directive() : Ast()
{

}

Directive::Directive(const YYSTYPE* token) : Ast(token)
{

}

// -------------------
// --- Align class ---
// -------------------

Align::Align(YYSTYPE* token) : Directive(token)
{
    Ast::free_token(token);
    this->alignment = 1;
}

void Align::setAlignment(const Ast *node)
{
    const AstValue* arg = dynamic_cast<const AstValue*>(node);
    if(arg == nullptr){
        throw AstException(*node, "Expected an AstValue object as argument");
    }
    setAlignment(arg);
}

void Align::setAlignment(const AstValue *node)
{
    this->alignment = node->getValue();

    //Update the positioning
    this->position = this->position.merge(node->getPosition());
}

void Align::setAlignment(const std::shared_ptr<Ast> node)
{
    setAlignment(node.get());
}

void Align::setAlignment(const std::shared_ptr<AstValue> node)
{
    setAlignment(node.get());
}

std::string Align::toString() const
{
    std::stringstream ss;
    ss << "ast::Align;";
    ss << "0x" << std::hex << this->alignment;
    return ss.str();
}

// ----------------------
// --- Absolute class ---
// ----------------------

Absolute::Absolute(YYSTYPE* token) : Directive(token)
{
    Ast::free_token(token);
    this->address = UINT32_MAX;
}

void Absolute::setAddress(const Ast *node)
{
    const AstValue* arg = dynamic_cast<const AstValue*>(node);
    if(arg == nullptr){
        throw AstException(*node, "Expected an AstValue object as argument");
    }
    setAddress(arg);
}

void Absolute::setAddress(const AstValue *node)
{
    this->address = node->getValue();

    //Update the positioning
    this->position = this->position.merge(node->getPosition());
}

void Absolute::setAddress(const std::shared_ptr<Ast>node)
{
    setAddress(node.get());
}

void Absolute::setAddress(const std::shared_ptr<AstValue>node)
{
    setAddress(node.get());
}

std::string Absolute::toString() const
{
    std::stringstream ss;
    ss << "ast::Absolute;";
    ss << "0x" << std::hex << this->address;
    return ss.str();
}

// --- Section ---

Section::Section(YYSTYPE* token) : Directive(token)
{
    this->section_name = "UNDEF";
    free_token(token);
}

Section::Section(YYSTYPE* token, const String &name) : Directive(token)
{
    this->section_name = std::string(name.getString());
    free_token(token);
    this->position = this->position.merge(name.getPosition());
}

std::string Section::toString() const
{
    return "ast::Section;" + this->section_name;
}

// --- Export ---
Export::Export(YYSTYPE* token) : Directive(token)
{
    Ast::free_token(token);
}

Export::Export(YYSTYPE* token, const Label &l) : Directive(token)
{
    Ast::free_token(token);
    this->symbol = l.getIdentifier();
    this->position = this->position.merge(l.getPosition());
}

std::string Export::toString() const
{
    return "ast::Export;" + this->symbol;
}

// --- Extern ---
Extern::Extern(YYSTYPE* token) : Directive(token)
{
    Ast::free_token(token);
}

Extern::Extern(YYSTYPE* token, const Label &l) : Directive(token)
{
    Ast::free_token(token);
    this->symbol = l.getIdentifier();
    this->position = this->position.merge(l.getPosition());
}

std::string Extern::toString() const
{
    return "ast::Extern;" + this->symbol;
}

}
}
