#include <ast/warning.hxx>
#include <stdexcept>
#include <map>

namespace as85{
namespace ast{

static const std::map<WarningId, std::string> warning_messages = {
    {WarningId::WRNID_PRECISSION_LOSS, "Loss of precission"},
    {WarningId::WRNID_MOV_IS_HLT,      "MOV MEM, MEM is not a valid move; It will result in the HLT opcode"},
    {WarningId::WRNID_UNDEF,           "UNDEFINED WARNING"},
};

// ---------------------
// --- Warning class ---
// ---------------------

Warning::Warning(std::string text) : AstAnnotation(text) { }

Warning::Warning(WarningId id) : AstAnnotation(Warning::getWarningString(id)) { }

Warning::Warning(YYSTYPE *token, WarningId id) : AstAnnotation(token)
{
    //Given a token, we store its contents for reporting them even if text is
    //overriden by setText()
    token_text = TextNode::toString();

    //If a message id is given, get its default description
    TextNode::setText(Warning::getWarningString(id));
}

std::string Warning::toString() const
{
    std::string rv;
    rv = std::to_string(this->position.begin.row) + ":" + std::to_string(this->position.begin.col);
    if(token_text.size() > 0){
        //If we have a token stored, preface the error message with its position and text
        rv = rv + " near: '" + token_text + "'";
    }
    rv += " -- ";
    rv += TextNode::toString();
    return rv;
}

std::string Warning::getWarningString(WarningId id)
{
    try{
        return warning_messages.at(id);
    }
    catch(std::out_of_range &e){
        (void) e;
        return warning_messages.at(WarningId::WRNID_UNDEF);
    }
}

}
}
