%{
#include <cstdint>
#include <cassert>
#include <memory>
#include <string>
#include <sstream>

/* Include the headers required for AST building here */
#include <ast/exception.hxx>
#include <ast/ast.hxx>
#include <ast/error.hxx>
#include <ast/warning.hxx>
#include <ast/list.hxx>
#include <ast/statement.hxx>
#include <ast/register.hxx>
#include <ast/opcode.hxx>
#include <ast/directive.hxx>
#include <parser.hxx>

//Everything is ::ast related so we use its namespace
using namespace as85::ast;

//Include stuff from lexer
#include "i8085.yy.h"

static int error_count = 0;
static int yyerror(const char* str)
{
    (void) str;
    error_count++;
    return 0;
}

//A few helper functions for keeping references to objects during parsing
std::map<void*, std::shared_ptr<as85::ast::Ast> > i8085_parser_nodes;

static Ast* ast_put(std::shared_ptr<as85::ast::Ast> ref)
{
    i8085_parser_nodes[ref.get()] = ref;
    return ref.get();
}

static std::shared_ptr<as85::ast::Ast> ast_get(void* ptr)
{
    if(i8085_parser_nodes.find(ptr) == i8085_parser_nodes.cend()){
        throw AstException(
            * reinterpret_cast<as85::ast::Ast*>(ptr),
            "Asked to retrieve a reference not previously inserted into parser node pool",
            __FILE__,
            __LINE__
        );
    }
    else{
        auto rv = i8085_parser_nodes.at(ptr);
        assert(rv->getSelfRef().lock());
        i8085_parser_nodes.erase(ptr);
        return rv;
    }
}

#define AST_PUT(P) (ast_put(P))
#define AST_GET(P) (ast_get(P))

//Helper function to reset the parser
void i8085_parser_reset(FILE* f)
{
    i8085_yylex_destroy();
    i8085_yyrestart(f);
    i8085_parser_nodes.clear();
    i8085_ast_root.reset();
}

//Get the parser version info
std::string i8085_parser_version(){
    std::stringstream ss;
#if defined YYBISON_VERSION
    ss << "Bison v" << YYBISON_VERSION;
#elif defined YYBYACC
    ss << "BYACC v" << YYMAJOR << "." << YYMINOR << "." <<  YYPATCH;
#endif
    auto rv = ss.str();
    if(rv.length() == 0){
        rv = "Unknown x.x.x";
    }
    return rv;
}

%}

/* Bison: Select the reeentrant parser. Not really required under BYACC */
%define api.pure full

/* yylval union declaration */
%union {
    /*The 's' struct is to be used by the lexer to provide token info to the parser*/
    struct {
        int token;    /*<-- The same as flex return value*/
        int row;      /*<-- Token position (row)*/
        int col;      /*<-- Token start (column)*/
        size_t size;  /*<-- Size of token as per strlen(yytext)*/
        const char* text;   /*<-- If not null, must be free() by parser*/
    } s;
    /*This pointer holds the AST object for the current token or nonterminal*/
    void* ast;
}

/* Registers */
%token REG_A
%token REG_F /* <-- Reserved but never used */
%token REG_B
%token REG_C
%token REG_D
%token REG_E
%token REG_H
%token REG_L
%token REG_MEM
/* 16bit registers */
%token REG_SP
%token REG_PC /* <-- Reserved but never used */
/* Register pairs */
%token REG_AF
%token REG_BC
%token REG_DE
%token REG_HL
/* Data transfer */
%token OP_MOV
%token OP_MVI
%token OP_LXI
%token OP_LDA
%token OP_STA
%token OP_LHLD
%token OP_SHLD
%token OP_LDAX
%token OP_STAX
%token OP_XCHG
/* Arithmetic */
%token OP_ADD
%token OP_ADC
%token OP_ADI
%token OP_ACI
%token OP_DAD
%token OP_SUB
%token OP_SBB
%token OP_SUI
%token OP_SBI
%token OP_INR
%token OP_DCR
%token OP_INX
%token OP_DCX
%token OP_DAA
/* Logical */
%token OP_ANA
%token OP_ANI
%token OP_ORA
%token OP_ORI
%token OP_XRA
%token OP_XRI
%token OP_CMA
%token OP_CMC
%token OP_STC
%token OP_CMP
%token OP_CPI
/* Rotations and shifts */
%token OP_RLC
%token OP_RRC
%token OP_RAL
%token OP_RAR
/* Jumps */
%token OP_JMP
%token OP_JZ
%token OP_JNZ
%token OP_JC
%token OP_JNC
%token OP_JP
%token OP_JM
%token OP_JPE
%token OP_JPO
%token OP_CALL
%token OP_CC
%token OP_CNC
%token OP_CZ
%token OP_CNZ
%token OP_CP
%token OP_CM
%token OP_CPE
%token OP_CPO
%token OP_RET
%token OP_RC
%token OP_RNC
%token OP_RZ
%token OP_RNZ
%token OP_RP
%token OP_RM
%token OP_RPE
%token OP_RPO
%token OP_RST
%token OP_PCHL
/* Stack */
%token OP_PUSH
%token OP_POP
%token OP_XTHL
%token OP_SPHL
/* IO and others */
%token OP_IN
%token OP_OUT
%token OP_HLT
%token OP_EI
%token OP_DI
%token OP_SIM
%token OP_RIM
%token OP_NOP

/* Pseudo-ops */
%token PS_ORG
%token PS_STR
%token PS_CSTR
%token PS_DB
%token PS_DW
%token PS_SECTION
%token PS_EXPORT
%token PS_ALIGN
%token PS_EXTERN

/* Additional non-opcode tokens */
%token NEWLINE  10
/* Identifier */
%token IDENTIFIER
/* numeric constants */
%token BINARY_CONSTANT
%token OCTAL_CONSTANT
%token DECIMAL_CONSTANT
%token HEXADECIMAL_CONSTANT
/* String & character literals */
%token STRING_LITERAL
%token CHARACTER_LITERAL

/* Unknown character token. Used to make parser fail. */
%token UNKNOWN_TOKEN

/* Unicode BOM will generate errors, in a sorta-controlled way*/
%token UNICODE_BOM

/* Bison disallows using untyped $$ $x tokens where on BYACC is optional */
/*  - Non-terminals */
%type<ast> program statement_list statement label value_list integer_value  
%type<ast> reg_8 reg_16 reg_16_stack opcode opcode_memory opcode_arithmetic
%type<ast> opcode_logical opcode_shift_rotate opcode_jump opcode_other
%type<ast> pseudo_op value_or_id

/*  - Terminals */
%type<s> REG_A REG_F REG_B REG_C REG_D REG_E REG_H REG_L REG_MEM REG_SP REG_PC
%type<s> REG_AF REG_BC REG_DE REG_HL OP_MOV OP_MVI OP_LXI OP_LDA OP_STA OP_LHLD
%type<s> OP_SHLD OP_LDAX OP_STAX OP_XCHG OP_ADD OP_ADC OP_ADI OP_ACI OP_DAD
%type<s> OP_SUB OP_SBB OP_SUI OP_SBI OP_INR OP_DCR OP_INX OP_DCX OP_DAA OP_ANA
%type<s> OP_ANI OP_ORA OP_ORI OP_XRA OP_XRI OP_CMA OP_CMC OP_STC OP_CMP OP_CPI
%type<s> OP_RLC OP_RRC OP_RAL OP_RAR OP_JMP OP_JZ OP_JNZ OP_JC OP_JNC OP_JP
%type<s> OP_JM OP_JPE OP_JPO OP_CALL OP_CC OP_CNC OP_CZ OP_CNZ OP_CP OP_CM
%type<s> OP_CPE OP_CPO OP_RET OP_RC OP_RNC OP_RZ OP_RNZ OP_RP OP_RM OP_RPE
%type<s> OP_RPO OP_RST OP_PCHL OP_PUSH OP_POP OP_XTHL OP_SPHL OP_IN OP_OUT
%type<s> OP_HLT OP_EI OP_DI OP_SIM OP_RIM OP_NOP PS_ORG PS_STR PS_CSTR PS_DB
%type<s> PS_DW PS_SECTION PS_EXPORT PS_ALIGN PS_EXTERN NEWLINE IDENTIFIER
%type<s> BINARY_CONSTANT OCTAL_CONSTANT DECIMAL_CONSTANT HEXADECIMAL_CONSTANT
%type<s> STRING_LITERAL CHARACTER_LITERAL UNICODE_BOM UNKNOWN_TOKEN
%type<s> ':'
%type<s> error

/* Prevent memory leaks on errors */
%destructor {
    //Free memory from discarded tokens
    if($$.text){
        free((void*)$$.text);
        $$.text = nullptr;
    }
    //Clear unused warnings
#ifdef YYBISON_VERSION
    (void) yymsg;
#else
    (void) msg;
#endif
} <s>
%destructor {
#ifdef YYBISON_VERSION
    //Clear "unused warning"
    (void) yymsg;
    (void) $$;
    //Error tokens apparently get discarded *twice* in Bison.
    //Nothing to free when discarded as `error` a second time.
#else
    (void) msg;
    //BYACC presumably still needs this.
    if($$.text){
        free((void*)$$.text);
        $$.text = nullptr;
    }
#endif
} error

%%

/* ---------------------------------------------------------------------- */
/* Parser rules will build an AST from tokens and nonterminals. This code */
/* is fairly repetitive since each rule will have to do the following:    */
/*   - Create a new AST node for the nonterminal                          */
/*   - Create a new AST node for each TERMINAL on the rule                */
/*   - Append child nodes to the newly created nonterminal object         */
/*                                                                        */
/* Check the implementation of `statement` and `label` for documented     */
/* examples of these actions. As a general rule, _all_ tokens except      */
/* NEWLINE and literal characters must be used to create AST objects as   */
/* to prevent memory leaks.                                               */
/* ---------------------------------------------------------------------- */

/* Initial rule */
program
    : statement_list    {   auto n = std::make_shared<TextNode>("program");
                            $$ = n.get();
                            n << AST_GET($1);
                            //Program has been parsed, make the AST root available
                            i8085_ast_root = std::shared_ptr<as85::ast::Ast>(n);
                            //The reference pool should be EMPTY (no dangling nodes)
                            //provided the program has been properly parsed
                        }
    ;

statement_list
    : statement                 {   $$ = AST_PUT(std::make_shared<StatementList>())
                                        << AST_GET($1);
                                }
    | statement_list statement  {   $$ = AST_PUT(std::make_shared<StatementList>())
                                        << AST_GET($1) << AST_GET($2);
                                }
    ;

statement
    :            NEWLINE    {   /*Create an AST node and save it to $$ */
                                $$ = AST_PUT(std::make_shared<Statement>());
                                /*This is an empty statement, nothing to add.*/
                                (void) $1; /*<- Bison: Explicitly ignore token */
                            }
    | opcode     NEWLINE    {   /* Create an AST node and save it to $$ */
                                $$ = AST_PUT(std::make_shared<Statement>())
                                    /* Append child node to the just created AST node */
                                    /*Once the node is added, the AST tree will manage the memory itself*/
                                    << AST_GET($1);
                                (void) $2; /*<- Bison: Explicitly ignore token */
                            }
    | pseudo_op  NEWLINE    {   $$ = AST_PUT(std::make_shared<Statement>())
                                    << AST_GET($1);
                                (void) $2; /*<- Bison: Explicitly ignore token */
                            }
    | label                 {   $$ = AST_PUT(std::make_shared<Statement>())
                                    << AST_GET($1);
                            }
    /* --- Bad statements --- */
    /* Wrong encoding */
    | UNICODE_BOM           {   $$ = AST_PUT(std::make_shared<Statement>())
                                    << std::make_shared<Error>((YYSTYPE*) & $1, ErrorId::ERRID_BAD_ENCODING);
                            }
    /* Unknown/invalid statement */
    | error      NEWLINE    {   /* Create an Statement node with an Error child, signaling the parsing has had errors. */
                                $$ = AST_PUT(std::make_shared<Statement>())
                                    ///@bug Error token freed before reducing.
                                    //<< std::make_shared<Error>((YYSTYPE*) & $1, ErrorId::ERRID_BAD_STATEMENT)
                                    ///Using the following instead:
                                    << std::make_shared<Error>((YYSTYPE*) & $2, ErrorId::ERRID_BAD_STATEMENT);
                                yyclearin; /*<- Bison: Discard `error` token immediately. See test case bug::bison_double_delete */
                                (void) $1; /*<- Bison: Explicitly ignore token */
                            }
    ;

label
    : IDENTIFIER ':'    {   /*Create an AST `Label` node and save it to $$ */
                            $$ = AST_PUT(std::make_shared<Label>((YYSTYPE*) & $1));
                            (void) $2; /*<- Bison: Explicitly ignore token */
                        }
    | error      ':'    {   /* Create an Error child node with the likely cause of the error. */
                            /* The cause is that it expected an identifier but got something else */
                            $$ = AST_PUT(std::make_shared<Error>((YYSTYPE*) & $2, ErrorId::ERRID_EXPECTED_IDENTIFIER));
                            (void) $1; /*<- Bison: Explicitly ignore token */
                            yyclearin;
                        }
    ;

value_list
    : value_or_id                       {   $$ = AST_PUT(std::make_shared<AstList>())
                                                << AST_GET($1);
                                        }
    | value_list ',' value_or_id        {   $$ = AST_PUT(std::make_shared<AstList>())
                                                << AST_GET($1) << AST_GET($3);
                                        }
    ;

integer_value     
    : BINARY_CONSTANT       { $$ = AST_PUT(std::make_shared<Integer>((YYSTYPE*) & $1)); }
    | OCTAL_CONSTANT        { $$ = AST_PUT(std::make_shared<Integer>((YYSTYPE*) & $1)); }
    | DECIMAL_CONSTANT      { $$ = AST_PUT(std::make_shared<Integer>((YYSTYPE*) & $1)); }
    | HEXADECIMAL_CONSTANT  { $$ = AST_PUT(std::make_shared<Integer>((YYSTYPE*) & $1)); }
    | CHARACTER_LITERAL     { $$ = AST_PUT(std::make_shared<Integer>((YYSTYPE*) & $1)); }
    ;

value_or_id
    : integer_value         { $$ = $1; }
    | IDENTIFIER            { $$ = AST_PUT(std::make_shared<Label>((YYSTYPE*) & $1)); }
    ;

reg_8
    : REG_A             { $$ = AST_PUT(std::make_shared<Register8>((YYSTYPE*) & $1)); }
    | REG_B             { $$ = AST_PUT(std::make_shared<Register8>((YYSTYPE*) & $1)); }
    | REG_C             { $$ = AST_PUT(std::make_shared<Register8>((YYSTYPE*) & $1)); }
    | REG_D             { $$ = AST_PUT(std::make_shared<Register8>((YYSTYPE*) & $1)); }
    | REG_E             { $$ = AST_PUT(std::make_shared<Register8>((YYSTYPE*) & $1)); }
    | REG_H             { $$ = AST_PUT(std::make_shared<Register8>((YYSTYPE*) & $1)); }
    | REG_L             { $$ = AST_PUT(std::make_shared<Register8>((YYSTYPE*) & $1)); }
    | REG_MEM           { $$ = AST_PUT(std::make_shared<Register8>((YYSTYPE*) & $1)); }
    | '(' REG_HL ')'    { $$ = AST_PUT(std::make_shared<Register8>((YYSTYPE*) & $2)); }
    ;

reg_16
    : REG_BC    { $$ = AST_PUT(std::make_shared<Register16>((YYSTYPE*) & $1)); }
    | REG_DE    { $$ = AST_PUT(std::make_shared<Register16>((YYSTYPE*) & $1)); }
    | REG_HL    { $$ = AST_PUT(std::make_shared<Register16>((YYSTYPE*) & $1)); }
    | REG_SP    { $$ = AST_PUT(std::make_shared<Register16>((YYSTYPE*) & $1)); }
    ;

reg_16_stack
    : REG_BC { $$ = AST_PUT(std::make_shared<Register16>((YYSTYPE*) & $1)); }
    | REG_DE { $$ = AST_PUT(std::make_shared<Register16>((YYSTYPE*) & $1)); }
    | REG_HL { $$ = AST_PUT(std::make_shared<Register16>((YYSTYPE*) & $1)); }
    | REG_AF { $$ = AST_PUT(std::make_shared<Register16>((YYSTYPE*) & $1)); }
    ;

/* `opcode` nonterminal is a special case where it serves only to aggregate */
/* other opcodes, hence all the work has already been done so we just       */
/* forward already existing objects up the AST as-is                        */
opcode
    : opcode_memory         { $$ = $1; }
    | opcode_arithmetic     { $$ = $1; }
    | opcode_logical        { $$ = $1; }
    | opcode_shift_rotate   { $$ = $1; }
    | opcode_jump           { $$ = $1; }
    | opcode_other          { $$ = $1; }
    ;

opcode_memory
    : OP_XTHL                               {   $$ = AST_PUT(std::make_shared<Opcode>((YYSTYPE*)& $1)); }
    | OP_SPHL                               {   $$ = AST_PUT(std::make_shared<Opcode>((YYSTYPE*)& $1)); }
    | OP_XCHG                               {   $$ = AST_PUT(std::make_shared<Opcode>((YYSTYPE*)& $1)); }
    | OP_LDA  value_or_id                   {   auto word = std::make_shared<Word>(16, Word::FMT_LITTLE) << AST_GET($2);
                                                $$ = AST_PUT(std::make_shared<Bytes>())
                                                    << std::make_shared<Opcode>((YYSTYPE*) & $1)
                                                    << word;
                                            }
    | OP_LDA  error                         {   $$ = AST_PUT(std::make_shared<Opcode>((YYSTYPE*) & $1))
                                                    << (std::make_shared<Error>((YYSTYPE*) & $2, ErrorId::ERRID_EXPECTED_VALUE_OR_ID));
                                                yyclearin;
                                            }
    | OP_STA  value_or_id                   {   auto word = std::make_shared<Word>(16, Word::FMT_LITTLE) << AST_GET($2);
                                                $$ = AST_PUT(std::make_shared<Bytes>())
                                                    << std::make_shared<Opcode>((YYSTYPE*) & $1)
                                                    << word;
                                            }
    | OP_STA  error                         {   $$ = AST_PUT(std::make_shared<Opcode>((YYSTYPE*) & $1))
                                                    << (std::make_shared<Error>((YYSTYPE*) & $2, ErrorId::ERRID_EXPECTED_VALUE_OR_ID));
                                                yyclearin;
                                            }
    | OP_LHLD value_or_id                   {   auto word = std::make_shared<Word>(16, Word::FMT_LITTLE) << AST_GET($2);
                                                $$ = AST_PUT(std::make_shared<Bytes>())
                                                    << std::make_shared<Opcode>((YYSTYPE*) & $1)
                                                    << word;
                                            }
    | OP_LHLD error                         {   $$ = AST_PUT(std::make_shared<Opcode>((YYSTYPE*) & $1))
                                                    << (std::make_shared<Error>((YYSTYPE*) & $2, ErrorId::ERRID_EXPECTED_VALUE_OR_ID));
                                                yyclearin;
                                            }
    | OP_SHLD value_or_id                   {   auto word = std::make_shared<Word>(16, Word::FMT_LITTLE) << AST_GET($2);
                                                $$ = AST_PUT(std::make_shared<Bytes>())
                                                    << std::make_shared<Opcode>((YYSTYPE*) & $1)
                                                    << word;
                                            }
    | OP_SHLD error                         {   $$ = AST_PUT(std::make_shared<Opcode>((YYSTYPE*) & $1))
                                                    << (std::make_shared<Error>((YYSTYPE*) & $2, ErrorId::ERRID_EXPECTED_VALUE_OR_ID));
                                                yyclearin;
                                            }
    | OP_LDAX reg_16                        {   //Threading carefully here.
                                                //$$ will be a register decorator object, but we first have to create
                                                //the underlying opcode
                                                auto op = std::make_shared<Opcode>((YYSTYPE*) & $1);
                                                auto reg_position = OpcodeRegisterDecorator::LS_16BIT;
                                                $$ = AST_PUT(
                                                    std::make_shared<OpcodeRegisterDecorator>(
                                                        op,
                                                        std::dynamic_pointer_cast<Register16>(AST_GET($2)), /* Must be Register16*/
                                                        reg_position
                                                    )
                                                );
                                            }
    | OP_LDAX error                         {   $$ = AST_PUT(std::make_shared<Opcode>((YYSTYPE*) & $1))
                                                    << (std::make_shared<Error>((YYSTYPE*) & $2, ErrorId::ERRID_EXPECTED_REG16));
                                                yyclearin;
                                            }
    | OP_STAX reg_16                        {   auto op = std::make_shared<Opcode>((YYSTYPE*) & $1);
                                                auto reg_position = OpcodeRegisterDecorator::LS_16BIT;
                                                $$ = AST_PUT(
                                                    std::make_shared<OpcodeRegisterDecorator>(
                                                        op,
                                                        std::dynamic_pointer_cast<Register16>(AST_GET($2)), /* Must be Register16*/
                                                        reg_position
                                                    )
                                                );
                                            }
    | OP_STAX error                         {   $$ = AST_PUT(std::make_shared<Opcode>((YYSTYPE*) & $1))
                                                    << (std::make_shared<Error>((YYSTYPE*) & $2, ErrorId::ERRID_EXPECTED_REG16));
                                                yyclearin;
                                            }
    | OP_PUSH reg_16_stack                  {   auto op = std::make_shared<Opcode>((YYSTYPE*) & $1);
                                                auto reg_position = OpcodeRegisterDecorator::PUSH_POP;
                                                $$ = AST_PUT(
                                                    std::make_shared<OpcodeRegisterDecorator>(
                                                        op,
                                                        std::dynamic_pointer_cast<Register16>(AST_GET($2)), /* Must be Register16*/
                                                        reg_position
                                                    )
                                                );
                                            }
    | OP_PUSH error                         {   $$ = AST_PUT(std::make_shared<Opcode>((YYSTYPE*) & $1))
                                                    << (std::make_shared<Error>((YYSTYPE*) & $2, ErrorId::ERRID_EXPECTED_REG_STACK));
                                                yyclearin;
                                            }
    | OP_POP  reg_16_stack                  {   auto op = std::make_shared<Opcode>((YYSTYPE*) & $1);
                                                auto reg_position = OpcodeRegisterDecorator::PUSH_POP;
                                                $$ = AST_PUT(
                                                    std::make_shared<OpcodeRegisterDecorator>(
                                                        op,
                                                        std::dynamic_pointer_cast<Register16>(AST_GET($2)), /* Must be Register16*/
                                                        reg_position
                                                    )
                                                );
                                            }
    | OP_POP  error                         {   $$ = AST_PUT(std::make_shared<Opcode>((YYSTYPE*) & $1))
                                                    << (std::make_shared<Error>((YYSTYPE*) & $2, ErrorId::ERRID_EXPECTED_REG_STACK));
                                                yyclearin;
                                            }
    | OP_MOV  reg_8  ',' reg_8              {   //Build the Opcode, then decorate it with 2 register
                                                //then save the result into $$
                                                auto op = std::make_shared<Opcode>((YYSTYPE*) & $1);
                                                //Decorate with DST register
                                                const auto dst_r = OpcodeRegisterDecorator::MOV_DST;
                                                auto op_r = std::make_shared<OpcodeRegisterDecorator>(
                                                    op,
                                                    std::dynamic_pointer_cast<Register8>(AST_GET($2)),
                                                    dst_r
                                                );
                                                //Decorate with SRC register
                                                const auto src_r = OpcodeRegisterDecorator::MOV_SRC;
                                                auto op_rr = std::make_shared<OpcodeRegisterDecorator>(
                                                    op_r,
                                                    std::dynamic_pointer_cast<Register8>(AST_GET($4)),
                                                    src_r
                                                );
                                                //If the resulting opcode is HLT, raise warning.
                                                if(op_rr->getBytes().at(0) == 0x76U){
                                                    *op_rr << std::make_shared<Warning>((YYSTYPE*) & $1, WRNID_MOV_IS_HLT);
                                                }
                                                //Save to $$
                                                $$ = AST_PUT(op_rr);
                                            }
    | OP_MOV  reg_8  ',' error              {   //Build the Opcode, then decorate it with 1 register
                                                //then add an error & save the result into $$
                                                auto op = std::make_shared<Opcode>((YYSTYPE*) & $1);
                                                //Decorate with DST register
                                                const auto dst_r = OpcodeRegisterDecorator::MOV_DST;
                                                auto op_r = std::make_shared<OpcodeRegisterDecorator>(
                                                    op,
                                                    std::dynamic_pointer_cast<Register8>(AST_GET($2)),
                                                    dst_r
                                                );
                                                //Save to $$
                                                $$ = AST_PUT(op_r)
                                                    //Add error node
                                                    << (std::make_shared<Error>((YYSTYPE*) & $4, ErrorId::ERRID_EXPECTED_REG8));
                                                yyclearin;
                                            }
    | OP_MOV  error                         {   $$ = AST_PUT(std::make_shared<Opcode>((YYSTYPE*) & $1))
                                                    << (std::make_shared<Error>((YYSTYPE*) & $2, ErrorId::ERRID_EXPECTED_REG8));
                                                yyclearin;
                                            }
    | OP_MVI  reg_8  ',' value_or_id        {   const auto arg_type = as85::ast::OpcodeRegisterDecorator::MOV_DST;
                                                $$ = AST_PUT(std::make_shared<Bytes>())
                                                    << std::make_shared <OpcodeRegisterDecorator>(
                                                        std::make_shared<Opcode>((YYSTYPE*) & $1),
                                                        std::dynamic_pointer_cast<Register8>(AST_GET($2)),
                                                        arg_type
                                                    )
                                                    << (std::make_shared<Word>(8) << AST_GET($4));
                                            }
    | OP_MVI  reg_8  ',' error              {   const auto arg_type = as85::ast::OpcodeRegisterDecorator::MOV_DST;
                                                $$ = AST_PUT(std::make_shared<Bytes>())
                                                    << std::make_shared <OpcodeRegisterDecorator>(
                                                        std::make_shared<Opcode>((YYSTYPE*) & $1),
                                                        std::dynamic_pointer_cast<Register8>(AST_GET($2)),
                                                        arg_type
                                                    )
                                                    << (std::make_shared<Error>((YYSTYPE*) & $4, ErrorId::ERRID_EXPECTED_VALUE_OR_ID));
                                                yyclearin;
                                            }
    | OP_MVI  error                         {   $$ = AST_PUT(std::make_shared<Opcode>((YYSTYPE*) & $1))
                                                    << (std::make_shared<Error>((YYSTYPE*) & $2, ErrorId::ERRID_EXPECTED_REG8));
                                                yyclearin;
                                            }
    | OP_LXI  reg_16 ',' value_or_id        {   auto op = std::make_shared<Opcode>((YYSTYPE*) & $1);
                                                const auto reg_position = OpcodeRegisterDecorator::LS_16BIT;
                                                auto op_reg = std::make_shared<OpcodeRegisterDecorator>(
                                                        op,
                                                        std::dynamic_pointer_cast<Register16>(AST_GET($2)), /* Must be Register16*/
                                                        reg_position
                                                );
                                                $$ = AST_PUT(std::make_shared<Bytes>())
                                                    << op_reg
                                                    << (std::make_shared<Word>(16, Word::FMT_LITTLE) << AST_GET($4));
                                            }
    | OP_LXI  reg_16 ',' error              {   auto op = std::make_shared<Opcode>((YYSTYPE*) & $1);
                                                const auto reg_position = OpcodeRegisterDecorator::LS_16BIT;
                                                auto op_reg = std::make_shared<OpcodeRegisterDecorator>(
                                                        op,
                                                        std::dynamic_pointer_cast<Register16>(AST_GET($2)), /* Must be Register16*/
                                                        reg_position
                                                );
                                                $$ = AST_PUT(std::make_shared<Bytes>())
                                                    << op_reg
                                                    << (std::make_shared<Error>((YYSTYPE*) & $4, ErrorId::ERRID_EXPECTED_VALUE_OR_ID));
                                                yyclearin;
                                            }
    | OP_LXI  error                         {   $$ = AST_PUT(std::make_shared<Opcode>((YYSTYPE*) & $1))
                                                    << (std::make_shared<Error>((YYSTYPE*) & $2, ErrorId::ERRID_EXPECTED_REG16));
                                                yyclearin;
                                            }
    ;

opcode_arithmetic
    : OP_ADD reg_8              {   auto op = std::make_shared<Opcode>((YYSTYPE*) & $1);
                                    //Decorate with DST register
                                    const auto dst_r = OpcodeRegisterDecorator::ALU_SRC;
                                    auto op_r = std::make_shared<OpcodeRegisterDecorator>(
                                        op,
                                        std::dynamic_pointer_cast<Register8>(AST_GET($2)),
                                        dst_r
                                    );
                                    $$ = AST_PUT(op_r);
                                }
    | OP_ADD error              {   auto op = std::make_shared<Opcode>((YYSTYPE*) & $1);
                                    $$ = AST_PUT(op)
                                        << (std::make_shared<Error>((YYSTYPE*) & $2, ErrorId::ERRID_EXPECTED_REG8));
                                    yyclearin;
                                }
    | OP_ADC reg_8              {   auto op = std::make_shared<Opcode>((YYSTYPE*) & $1);
                                    //Decorate with DST register
                                    const auto dst_r = OpcodeRegisterDecorator::ALU_SRC;
                                    auto op_r = std::make_shared<OpcodeRegisterDecorator>(
                                        op,
                                        std::dynamic_pointer_cast<Register8>(AST_GET($2)),
                                        dst_r
                                    );
                                    $$ = AST_PUT(op_r);
                                }
    | OP_ADC error              {   auto op = std::make_shared<Opcode>((YYSTYPE*) & $1);
                                    $$ = AST_PUT(op)
                                        << (std::make_shared<Error>((YYSTYPE*) & $2, ErrorId::ERRID_EXPECTED_REG8));
                                    yyclearin;
                                }
    | OP_ADI value_or_id        {   $$ = AST_PUT(std::make_shared<Bytes>())
                                        << std::make_shared<Opcode>((YYSTYPE*) & $1)
                                        << (std::make_shared<Word>(8) << AST_GET($2));
                                }
    | OP_ADI error              {   $$ = AST_PUT(std::make_shared<Opcode>((YYSTYPE*) & $1))
                                        << (std::make_shared<Error>((YYSTYPE*) & $2, ErrorId::ERRID_EXPECTED_VALUE_OR_ID));
                                    yyclearin;
                                }
    | OP_ACI value_or_id        {   $$ = AST_PUT(std::make_shared<Bytes>())
                                        << std::make_shared<Opcode>((YYSTYPE*) & $1)
                                        << (std::make_shared<Word>(8) << AST_GET($2));
                                }
    | OP_ACI error              {   $$ = AST_PUT(std::make_shared<Opcode>((YYSTYPE*) & $1))
                                        << (std::make_shared<Error>((YYSTYPE*) & $2, ErrorId::ERRID_EXPECTED_VALUE_OR_ID));
                                    yyclearin;
                                }
    | OP_DAD reg_16             {   auto op = std::make_shared<Opcode>((YYSTYPE*) & $1);
                                    auto reg_position = OpcodeRegisterDecorator::LS_16BIT;
                                    $$ = AST_PUT(
                                        std::make_shared<OpcodeRegisterDecorator>(
                                            op,
                                            std::dynamic_pointer_cast<Register16>(AST_GET($2)), /* Must be Register16*/
                                            reg_position
                                        )
                                    );
                                }
    | OP_DAD error              {   $$ = AST_PUT(std::make_shared<Opcode>((YYSTYPE*) & $1))
                                        << (std::make_shared<Error>((YYSTYPE*) & $2, ErrorId::ERRID_EXPECTED_REG16));
                                    yyclearin;
                                }
    | OP_SUB reg_8              {   auto op = std::make_shared<Opcode>((YYSTYPE*) & $1);
                                    //Decorate with DST register
                                    const auto dst_r = OpcodeRegisterDecorator::ALU_SRC;
                                    auto op_r = std::make_shared<OpcodeRegisterDecorator>(
                                        op,
                                        std::dynamic_pointer_cast<Register8>(AST_GET($2)),
                                        dst_r
                                    );
                                    $$ = AST_PUT(op_r);
                                }
    | OP_SUB error              {   auto op = std::make_shared<Opcode>((YYSTYPE*) & $1);
                                    $$ = AST_PUT(op)
                                        << (std::make_shared<Error>((YYSTYPE*) & $2, ErrorId::ERRID_EXPECTED_REG8));
                                    yyclearin;
                                }
    | OP_SBB reg_8              {   auto op = std::make_shared<Opcode>((YYSTYPE*) & $1);
                                    //Decorate with DST register
                                    const auto dst_r = OpcodeRegisterDecorator::ALU_SRC;
                                    auto op_r = std::make_shared<OpcodeRegisterDecorator>(
                                        op,
                                        std::dynamic_pointer_cast<Register8>(AST_GET($2)),
                                        dst_r
                                    );
                                    $$ = AST_PUT(op_r);
                                }
    | OP_SBB error              {   auto op = std::make_shared<Opcode>((YYSTYPE*) & $1);
                                    $$ = AST_PUT(op)
                                        << (std::make_shared<Error>((YYSTYPE*) & $2, ErrorId::ERRID_EXPECTED_REG8));
                                    yyclearin;
                                }
    | OP_SUI value_or_id        {   $$ = AST_PUT(std::make_shared<Bytes>())
                                        << std::make_shared<Opcode>((YYSTYPE*) & $1)
                                        << (std::make_shared<Word>(8) << AST_GET($2));
                                }
    | OP_SUI error              {   $$ = AST_PUT(std::make_shared<Opcode>((YYSTYPE*) & $1))
                                        << (std::make_shared<Error>((YYSTYPE*) & $2, ErrorId::ERRID_EXPECTED_VALUE_OR_ID));
                                    yyclearin;
                                }
    | OP_SBI value_or_id        {   $$ = AST_PUT(std::make_shared<Bytes>())
                                        << std::make_shared<Opcode>((YYSTYPE*) & $1)
                                        << (std::make_shared<Word>(8) << AST_GET($2));
                                }
    | OP_SBI error              {   $$ = AST_PUT(std::make_shared<Opcode>((YYSTYPE*) & $1))
                                        << (std::make_shared<Error>((YYSTYPE*) & $2, ErrorId::ERRID_EXPECTED_VALUE_OR_ID));
                                    yyclearin;
                                }
    | OP_INR reg_8              {   auto op = std::make_shared<Opcode>((YYSTYPE*) & $1);
                                    //Decorate with DST register
                                    const auto dst_r = OpcodeRegisterDecorator::ALU_DST;
                                    auto op_r = std::make_shared<OpcodeRegisterDecorator>(
                                        op,
                                        std::dynamic_pointer_cast<Register8>(AST_GET($2)),
                                        dst_r
                                    );
                                    $$ = AST_PUT(op_r);
                                }
    | OP_INR error              {   auto op = std::make_shared<Opcode>((YYSTYPE*) & $1);
                                    $$ = AST_PUT(op)
                                        << (std::make_shared<Error>((YYSTYPE*) & $2, ErrorId::ERRID_EXPECTED_REG8));
                                    yyclearin;
                                }
    | OP_DCR reg_8              {   auto op = std::make_shared<Opcode>((YYSTYPE*) & $1);
                                    //Decorate with DST register
                                    const auto dst_r = OpcodeRegisterDecorator::ALU_DST;
                                    auto op_r = std::make_shared<OpcodeRegisterDecorator>(
                                        op,
                                        std::dynamic_pointer_cast<Register8>(AST_GET($2)),
                                        dst_r
                                    );
                                    $$ = AST_PUT(op_r);
                                }
    | OP_DCR error              {   auto op = std::make_shared<Opcode>((YYSTYPE*) & $1);
                                    $$ = AST_PUT(op)
                                        << (std::make_shared<Error>((YYSTYPE*) & $2, ErrorId::ERRID_EXPECTED_REG8));
                                    yyclearin;
                                }
    | OP_INX reg_16             {   auto op = std::make_shared<Opcode>((YYSTYPE*) & $1);
                                    auto reg_position = OpcodeRegisterDecorator::LS_16BIT;
                                    $$ = AST_PUT(
                                        std::make_shared<OpcodeRegisterDecorator>(
                                            op,
                                            std::dynamic_pointer_cast<Register16>(AST_GET($2)), /* Must be Register16*/
                                            reg_position
                                        )
                                    );
                                }
    | OP_INX error              {   $$ = AST_PUT(std::make_shared<Opcode>((YYSTYPE*) & $1))
                                        << (std::make_shared<Error>((YYSTYPE*) & $2, ErrorId::ERRID_EXPECTED_REG16));
                                    yyclearin;
                                }
    | OP_DCX reg_16             {   auto op = std::make_shared<Opcode>((YYSTYPE*) & $1);
                                    auto reg_position = OpcodeRegisterDecorator::LS_16BIT;
                                    $$ = AST_PUT(
                                        std::make_shared<OpcodeRegisterDecorator>(
                                            op,
                                            std::dynamic_pointer_cast<Register16>(AST_GET($2)), /* Must be Register16*/
                                            reg_position
                                        )
                                    );
                                }
    | OP_DCX error              {   $$ = AST_PUT(std::make_shared<Opcode>((YYSTYPE*) & $1))
                                        << (std::make_shared<Error>((YYSTYPE*) & $2, ErrorId::ERRID_EXPECTED_REG16));
                                    yyclearin;
                                }
    | OP_DAA                    {   $$ = AST_PUT(std::make_shared<Opcode>((YYSTYPE*) & $1)); }
    ;

opcode_logical
    : OP_ANA reg_8              {   auto op = std::make_shared<Opcode>((YYSTYPE*) & $1);
                                    //Decorate with DST register
                                    const auto dst_r = OpcodeRegisterDecorator::ALU_SRC;
                                    auto op_r = std::make_shared<OpcodeRegisterDecorator>(
                                        op,
                                        std::dynamic_pointer_cast<Register8>(AST_GET($2)),
                                        dst_r
                                    );
                                    $$ = AST_PUT(op_r);
                                }
    | OP_ANA error              {   auto op = std::make_shared<Opcode>((YYSTYPE*) & $1);
                                    $$ = AST_PUT(op)
                                        << (std::make_shared<Error>((YYSTYPE*) & $2, ErrorId::ERRID_EXPECTED_REG8));
                                    yyclearin;
                                }
    | OP_ANI value_or_id        {   $$ = AST_PUT(std::make_shared<Bytes>())
                                        << std::make_shared<Opcode>((YYSTYPE*) & $1)
                                        << (std::make_shared<Word>(8) << AST_GET($2));
                                }
    | OP_ANI error              {   $$ = AST_PUT(std::make_shared<Opcode>((YYSTYPE*) & $1))
                                        << (std::make_shared<Error>((YYSTYPE*) & $2, ErrorId::ERRID_EXPECTED_VALUE_OR_ID));
                                    yyclearin;
                                }
    | OP_ORA reg_8              {   auto op = std::make_shared<Opcode>((YYSTYPE*) & $1);
                                    //Decorate with DST register
                                    const auto dst_r = OpcodeRegisterDecorator::ALU_SRC;
                                    auto op_r = std::make_shared<OpcodeRegisterDecorator>(
                                        op,
                                        std::dynamic_pointer_cast<Register8>(AST_GET($2)),
                                        dst_r
                                    );
                                    $$ = AST_PUT(op_r);
                                }
    | OP_ORA error              {   auto op = std::make_shared<Opcode>((YYSTYPE*) & $1);
                                    $$ = AST_PUT(op)
                                        << (std::make_shared<Error>((YYSTYPE*) & $2, ErrorId::ERRID_EXPECTED_REG8));
                                    yyclearin;
                                }
    | OP_ORI value_or_id        {   $$ = AST_PUT(std::make_shared<Bytes>())
                                        << std::make_shared<Opcode>((YYSTYPE*) & $1)
                                        << (std::make_shared<Word>(8) << AST_GET($2));
                                }
    | OP_ORI error              {   $$ = AST_PUT(std::make_shared<Opcode>((YYSTYPE*) & $1))
                                        << (std::make_shared<Error>((YYSTYPE*) & $2, ErrorId::ERRID_EXPECTED_VALUE_OR_ID));
                                    yyclearin;
                                }
    | OP_XRA reg_8              {   auto op = std::make_shared<Opcode>((YYSTYPE*) & $1);
                                    //Decorate with DST register
                                    const auto dst_r = OpcodeRegisterDecorator::ALU_SRC;
                                    auto op_r = std::make_shared<OpcodeRegisterDecorator>(
                                        op,
                                        std::dynamic_pointer_cast<Register8>(AST_GET($2)),
                                        dst_r
                                    );
                                    $$ = AST_PUT(op_r);
                                }
    | OP_XRA error              {   auto op = std::make_shared<Opcode>((YYSTYPE*) & $1);
                                    $$ = AST_PUT(op)
                                        << (std::make_shared<Error>((YYSTYPE*) & $2, ErrorId::ERRID_EXPECTED_REG8));
                                    yyclearin;
                                }
    | OP_XRI value_or_id        {   $$ = AST_PUT(std::make_shared<Bytes>())
                                        << std::make_shared<Opcode>((YYSTYPE*) & $1)
                                        << (std::make_shared<Word>(8) << AST_GET($2));
                                }
    | OP_XRI error              {   $$ = AST_PUT(std::make_shared<Opcode>((YYSTYPE*) & $1))
                                        << (std::make_shared<Error>((YYSTYPE*) & $2, ErrorId::ERRID_EXPECTED_VALUE_OR_ID));
                                    yyclearin;
                                }
    | OP_CMA                    {   $$ = AST_PUT(std::make_shared<Opcode>((YYSTYPE*) & $1)); }
    | OP_CMC                    {   $$ = AST_PUT(std::make_shared<Opcode>((YYSTYPE*) & $1)); }
    | OP_STC                    {   $$ = AST_PUT(std::make_shared<Opcode>((YYSTYPE*) & $1)); }
    | OP_CMP reg_8              {   auto op = std::make_shared<Opcode>((YYSTYPE*) & $1);
                                    //Decorate with DST register
                                    const auto dst_r = OpcodeRegisterDecorator::ALU_SRC;
                                    auto op_r = std::make_shared<OpcodeRegisterDecorator>(
                                        op,
                                        std::dynamic_pointer_cast<Register8>(AST_GET($2)),
                                        dst_r
                                    );
                                    $$ = AST_PUT(op_r);
                                }
    | OP_CMP error              {   auto op = std::make_shared<Opcode>((YYSTYPE*) & $1);
                                    $$ = AST_PUT(op)
                                        << (std::make_shared<Error>((YYSTYPE*) & $2, ErrorId::ERRID_EXPECTED_REG8));
                                    yyclearin;
                                }
    | OP_CPI value_or_id        {   $$ = AST_PUT(std::make_shared<Bytes>())
                                        << std::make_shared<Opcode>((YYSTYPE*) & $1)
                                        << (std::make_shared<Word>(8) << AST_GET($2));
                                }
    | OP_CPI error              {   $$ = AST_PUT(std::make_shared<Opcode>((YYSTYPE*) & $1))
                                        << (std::make_shared<Error>((YYSTYPE*) & $2, ErrorId::ERRID_EXPECTED_VALUE_OR_ID));
                                    yyclearin;
                                }
    ;

opcode_shift_rotate
    : OP_RLC    { $$ = AST_PUT(std::make_shared<Opcode>((YYSTYPE*) & $1)); }
    | OP_RRC    { $$ = AST_PUT(std::make_shared<Opcode>((YYSTYPE*) & $1)); }
    | OP_RAL    { $$ = AST_PUT(std::make_shared<Opcode>((YYSTYPE*) & $1)); }
    | OP_RAR    { $$ = AST_PUT(std::make_shared<Opcode>((YYSTYPE*) & $1)); }
    ;

opcode_jump
    : OP_JMP  value_or_id       {   $$ = AST_PUT(std::make_shared<Bytes>())
                                        << std::make_shared<Opcode>((YYSTYPE*) & $1)
                                        << (std::make_shared<Word>(16, Word::FMT_LITTLE) << AST_GET($2));
                                }
    | OP_JMP  error             {   $$ = AST_PUT(std::make_shared<Opcode>((YYSTYPE*) & $1))
                                        << (std::make_shared<Error>((YYSTYPE*) & $2, ErrorId::ERRID_EXPECTED_VALUE_OR_ID));
                                    yyclearin;
                                }
    | OP_JZ   value_or_id       {   $$ = AST_PUT(std::make_shared<Bytes>())
                                        << std::make_shared<Opcode>((YYSTYPE*) & $1)
                                        << (std::make_shared<Word>(16, Word::FMT_LITTLE) << AST_GET($2));
                                }
    | OP_JZ   error             {   $$ = AST_PUT(std::make_shared<Opcode>((YYSTYPE*) & $1))
                                        << (std::make_shared<Error>((YYSTYPE*) & $2, ErrorId::ERRID_EXPECTED_VALUE_OR_ID));
                                    yyclearin;
                                }
    | OP_JNZ  value_or_id       {   $$ = AST_PUT(std::make_shared<Bytes>())
                                        << std::make_shared<Opcode>((YYSTYPE*) & $1)
                                        << (std::make_shared<Word>(16, Word::FMT_LITTLE) << AST_GET($2));
                                }
    | OP_JNZ  error             {   $$ = AST_PUT(std::make_shared<Opcode>((YYSTYPE*) & $1))
                                        << (std::make_shared<Error>((YYSTYPE*) & $2, ErrorId::ERRID_EXPECTED_VALUE_OR_ID));
                                    yyclearin;
                                }
    | OP_JC   value_or_id       {   $$ = AST_PUT(std::make_shared<Bytes>())
                                        << std::make_shared<Opcode>((YYSTYPE*) & $1)
                                        << (std::make_shared<Word>(16, Word::FMT_LITTLE) << AST_GET($2));
                                }
    | OP_JC   error             {   $$ = AST_PUT(std::make_shared<Opcode>((YYSTYPE*) & $1))
                                        << (std::make_shared<Error>((YYSTYPE*) & $2, ErrorId::ERRID_EXPECTED_VALUE_OR_ID));
                                    yyclearin;
                                }
    | OP_JNC  value_or_id       {   $$ = AST_PUT(std::make_shared<Bytes>())
                                        << std::make_shared<Opcode>((YYSTYPE*) & $1)
                                        << (std::make_shared<Word>(16, Word::FMT_LITTLE) << AST_GET($2));
                                }
    | OP_JNC  error             {   $$ = AST_PUT(std::make_shared<Opcode>((YYSTYPE*) & $1))
                                        << (std::make_shared<Error>((YYSTYPE*) & $2, ErrorId::ERRID_EXPECTED_VALUE_OR_ID));
                                    yyclearin;
                                }
    | OP_JP   value_or_id       {   $$ = AST_PUT(std::make_shared<Bytes>())
                                        << std::make_shared<Opcode>((YYSTYPE*) & $1)
                                        << (std::make_shared<Word>(16, Word::FMT_LITTLE) << AST_GET($2));
                                }
    | OP_JP   error             {   $$ = AST_PUT(std::make_shared<Opcode>((YYSTYPE*) & $1))
                                        << (std::make_shared<Error>((YYSTYPE*) & $2, ErrorId::ERRID_EXPECTED_VALUE_OR_ID));
                                    yyclearin;
                                }
    | OP_JM   value_or_id       {   $$ = AST_PUT(std::make_shared<Bytes>())
                                        << std::make_shared<Opcode>((YYSTYPE*) & $1)
                                        << (std::make_shared<Word>(16, Word::FMT_LITTLE) << AST_GET($2));
                                }
    | OP_JM   error             {   $$ = AST_PUT(std::make_shared<Opcode>((YYSTYPE*) & $1))
                                        << (std::make_shared<Error>((YYSTYPE*) & $2, ErrorId::ERRID_EXPECTED_VALUE_OR_ID));
                                    yyclearin;
                                }
    | OP_JPE  value_or_id       {   $$ = AST_PUT(std::make_shared<Bytes>())
                                        << std::make_shared<Opcode>((YYSTYPE*) & $1)
                                        << (std::make_shared<Word>(16, Word::FMT_LITTLE) << AST_GET($2));
                                }
    | OP_JPE  error             {   $$ = AST_PUT(std::make_shared<Opcode>((YYSTYPE*) & $1))
                                        << (std::make_shared<Error>((YYSTYPE*) & $2, ErrorId::ERRID_EXPECTED_VALUE_OR_ID));
                                    yyclearin;
                                }
    | OP_JPO  value_or_id       {   $$ = AST_PUT(std::make_shared<Bytes>())
                                        << std::make_shared<Opcode>((YYSTYPE*) & $1)
                                        << (std::make_shared<Word>(16, Word::FMT_LITTLE) << AST_GET($2));
                                }
    | OP_JPO  error             {   $$ = AST_PUT(std::make_shared<Opcode>((YYSTYPE*) & $1))
                                        << (std::make_shared<Error>((YYSTYPE*) & $2, ErrorId::ERRID_EXPECTED_VALUE_OR_ID));
                                    yyclearin;
                                }
    | OP_CALL value_or_id       {   $$ = AST_PUT(std::make_shared<Bytes>())
                                        << std::make_shared<Opcode>((YYSTYPE*) & $1)
                                        << (std::make_shared<Word>(16, Word::FMT_LITTLE) << AST_GET($2));
                                }
    | OP_CALL  error            {   $$ = AST_PUT(std::make_shared<Opcode>((YYSTYPE*) & $1))
                                        << (std::make_shared<Error>((YYSTYPE*) & $2, ErrorId::ERRID_EXPECTED_VALUE_OR_ID));
                                    yyclearin;
                                }
    | OP_CC   value_or_id       {   $$ = AST_PUT(std::make_shared<Bytes>())
                                        << std::make_shared<Opcode>((YYSTYPE*) & $1)
                                        << (std::make_shared<Word>(16, Word::FMT_LITTLE) << AST_GET($2));
                                }
    | OP_CC   error             {   $$ = AST_PUT(std::make_shared<Opcode>((YYSTYPE*) & $1))
                                        << (std::make_shared<Error>((YYSTYPE*) & $2, ErrorId::ERRID_EXPECTED_VALUE_OR_ID));
                                    yyclearin;
                                }
    | OP_CNC  value_or_id       {   $$ = AST_PUT(std::make_shared<Bytes>())
                                        << std::make_shared<Opcode>((YYSTYPE*) & $1)
                                        << (std::make_shared<Word>(16, Word::FMT_LITTLE) << AST_GET($2));
                                }
    | OP_CNC  error             {   $$ = AST_PUT(std::make_shared<Opcode>((YYSTYPE*) & $1))
                                        << (std::make_shared<Error>((YYSTYPE*) & $2, ErrorId::ERRID_EXPECTED_VALUE_OR_ID));
                                    yyclearin;
                                }
    | OP_CZ   value_or_id       {   $$ = AST_PUT(std::make_shared<Bytes>())
                                        << std::make_shared<Opcode>((YYSTYPE*) & $1)
                                        << (std::make_shared<Word>(16, Word::FMT_LITTLE) << AST_GET($2));
                                }
    | OP_CZ   error             {   $$ = AST_PUT(std::make_shared<Opcode>((YYSTYPE*) & $1))
                                        << (std::make_shared<Error>((YYSTYPE*) & $2, ErrorId::ERRID_EXPECTED_VALUE_OR_ID));
                                    yyclearin;
                                }
    | OP_CNZ  value_or_id       {   $$ = AST_PUT(std::make_shared<Bytes>())
                                        << std::make_shared<Opcode>((YYSTYPE*) & $1)
                                        << (std::make_shared<Word>(16, Word::FMT_LITTLE) << AST_GET($2));
                                }
    | OP_CNZ  error             {   $$ = AST_PUT(std::make_shared<Opcode>((YYSTYPE*) & $1))
                                        << (std::make_shared<Error>((YYSTYPE*) & $2, ErrorId::ERRID_EXPECTED_VALUE_OR_ID));
                                    yyclearin;
                                }
    | OP_CP   value_or_id       {   $$ = AST_PUT(std::make_shared<Bytes>())
                                        << std::make_shared<Opcode>((YYSTYPE*) & $1)
                                        << (std::make_shared<Word>(16, Word::FMT_LITTLE) << AST_GET($2));
                                }
    | OP_CP   error             {   $$ = AST_PUT(std::make_shared<Opcode>((YYSTYPE*) & $1))
                                        << (std::make_shared<Error>((YYSTYPE*) & $2, ErrorId::ERRID_EXPECTED_VALUE_OR_ID));
                                    yyclearin;
                                }
    | OP_CM   value_or_id       {   $$ = AST_PUT(std::make_shared<Bytes>())
                                        << std::make_shared<Opcode>((YYSTYPE*) & $1)
                                        << (std::make_shared<Word>(16, Word::FMT_LITTLE) << AST_GET($2));
                                }
    | OP_CM   error             {   $$ = AST_PUT(std::make_shared<Opcode>((YYSTYPE*) & $1))
                                        << (std::make_shared<Error>((YYSTYPE*) & $2, ErrorId::ERRID_EXPECTED_VALUE_OR_ID));
                                    yyclearin;
                                }
    | OP_CPE  value_or_id       {   $$ = AST_PUT(std::make_shared<Bytes>())
                                        << std::make_shared<Opcode>((YYSTYPE*) & $1)
                                        << (std::make_shared<Word>(16, Word::FMT_LITTLE) << AST_GET($2));
                                }
    | OP_CPE  error             {   $$ = AST_PUT(std::make_shared<Opcode>((YYSTYPE*) & $1))
                                        << (std::make_shared<Error>((YYSTYPE*) & $2, ErrorId::ERRID_EXPECTED_VALUE_OR_ID));
                                    yyclearin;
                                }
    | OP_CPO  value_or_id       {   $$ = AST_PUT(std::make_shared<Bytes>())
                                        << std::make_shared<Opcode>((YYSTYPE*) & $1)
                                        << (std::make_shared<Word>(16, Word::FMT_LITTLE) << AST_GET($2));
                                }
    | OP_CPO  error             {   $$ = AST_PUT(std::make_shared<Opcode>((YYSTYPE*) & $1))
                                        << (std::make_shared<Error>((YYSTYPE*) & $2, ErrorId::ERRID_EXPECTED_VALUE_OR_ID));
                                    yyclearin;
                                }
    | OP_RET                    { $$ = AST_PUT(std::make_shared<Opcode>((YYSTYPE*) & $1)); }
    | OP_RC                     { $$ = AST_PUT(std::make_shared<Opcode>((YYSTYPE*) & $1)); }
    | OP_RNC                    { $$ = AST_PUT(std::make_shared<Opcode>((YYSTYPE*) & $1)); }
    | OP_RZ                     { $$ = AST_PUT(std::make_shared<Opcode>((YYSTYPE*) & $1)); }
    | OP_RNZ                    { $$ = AST_PUT(std::make_shared<Opcode>((YYSTYPE*) & $1)); }
    | OP_RP                     { $$ = AST_PUT(std::make_shared<Opcode>((YYSTYPE*) & $1)); }
    | OP_RM                     { $$ = AST_PUT(std::make_shared<Opcode>((YYSTYPE*) & $1)); }
    | OP_RPE                    { $$ = AST_PUT(std::make_shared<Opcode>((YYSTYPE*) & $1)); }
    | OP_RPO                    { $$ = AST_PUT(std::make_shared<Opcode>((YYSTYPE*) & $1)); }
    | OP_RST  value_or_id       {   
                                    //Extract the RST vector value from the underlying node
                                    //by casting it as an `AstValue`class.
                                    auto node = AST_GET($2);
                                    auto value = std::dynamic_pointer_cast<AstValue>(node);
                                    if(!value){
                                        throw as85::ast::AstException(*node, "RST vector is not of `AstValue` type");
                                    }
                                    //Business as usual now
                                    auto n = std::make_shared<OpRST>(
                                        (YYSTYPE*) & $1,
                                        (value)->getValue()
                                    );
                                    $$ = AST_PUT(n);
                                    //Underlying value_or_id node should be disposed here automagically
                                }
    | OP_RST  error             {   $$ = AST_PUT(std::make_shared<Opcode>((YYSTYPE*) & $1))
                                        << (std::make_shared<Error>((YYSTYPE*) & $2, ErrorId::ERRID_EXPECTED_RST_VECTOR));
                                    yyclearin;
                                }
    | OP_PCHL                   {   $$ = AST_PUT(std::make_shared<Opcode>((YYSTYPE*) & $1)); }
    ;

opcode_other
    : OP_IN  value_or_id        {   $$ = AST_PUT(std::make_shared<Bytes>())
                                        << std::make_shared<Opcode>((YYSTYPE*) & $1)
                                        << (std::make_shared<Word>(8) << AST_GET($2));
                                }
    | OP_IN  error              {   $$ = AST_PUT(std::make_shared<Opcode>((YYSTYPE*) & $1))
                                        << (std::make_shared<Error>((YYSTYPE*) & $2, ErrorId::ERRID_EXPECTED_VALUE_OR_ID));
                                    yyclearin;
                                }
    | OP_OUT value_or_id        {   $$ = AST_PUT(std::make_shared<Bytes>())
                                        << std::make_shared<Opcode>((YYSTYPE*) & $1)
                                        << (std::make_shared<Word>(8) << AST_GET($2));
                                }
    | OP_OUT error              {   $$ = AST_PUT(std::make_shared<Opcode>((YYSTYPE*) & $1))
                                        << (std::make_shared<Error>((YYSTYPE*) & $2, ErrorId::ERRID_EXPECTED_VALUE_OR_ID));
                                    yyclearin;
                                }
    | OP_HLT                    { $$ = AST_PUT(std::make_shared<Opcode>((YYSTYPE*) & $1)); }
    | OP_EI                     { $$ = AST_PUT(std::make_shared<Opcode>((YYSTYPE*) & $1)); }
    | OP_DI                     { $$ = AST_PUT(std::make_shared<Opcode>((YYSTYPE*) & $1)); }
    | OP_SIM                    { $$ = AST_PUT(std::make_shared<Opcode>((YYSTYPE*) & $1)); }
    | OP_RIM                    { $$ = AST_PUT(std::make_shared<Opcode>((YYSTYPE*) & $1)); }
    | OP_NOP                    { $$ = AST_PUT(std::make_shared<Opcode>((YYSTYPE*) & $1)); }
    ;

pseudo_op
    : PS_ORG      integer_value     {   auto n = std::make_shared<Absolute>((YYSTYPE*) & $1);
                                        n->setAddress(AST_GET($2));
                                        $$ = AST_PUT(n);
                                    }
    | PS_ORG      error             {   $$ = AST_PUT(std::make_shared<Absolute>((YYSTYPE*) & $1))
                                            << (std::make_shared<Error>((YYSTYPE*) & $2, ErrorId::ERRID_EXPECTED_VALUE));
                                        yyclearin;
                                    }
    | PS_ALIGN    integer_value     {   auto n = std::make_shared<Align>((YYSTYPE*) & $1);
                                        n->setAlignment(AST_GET($2));
                                        $$ = AST_PUT(n);
                                    }
    | PS_ALIGN    error             {   $$ = AST_PUT(std::make_shared<Align>((YYSTYPE*) & $1))
                                            << (std::make_shared<Error>((YYSTYPE*) & $2, ErrorId::ERRID_EXPECTED_VALUE));
                                        yyclearin;
                                    }
    | PS_SECTION  STRING_LITERAL    {   auto s = std::make_shared<String>((YYSTYPE*) & $2, String::STR_UNFORMATTED);
                                        $$ = AST_PUT(std::make_shared<Section>((YYSTYPE*) & $1, *s));
                                    }
    | PS_SECTION  error             {   $$ = AST_PUT(std::make_shared<Section>((YYSTYPE*) & $1))
                                            << (std::make_shared<Error>((YYSTYPE*) & $2, ErrorId::ERRID_EXPECTED_SECTION_NAME));
                                        yyclearin;
                                    }
    | PS_EXPORT   IDENTIFIER        {   auto l = std::make_shared<Label>((YYSTYPE*) & $2);
                                        $$ = AST_PUT(std::make_shared<Export>((YYSTYPE*) & $1, *l));
                                    }
    | PS_EXPORT   error             {   $$ = AST_PUT(std::make_shared<Export>((YYSTYPE*) & $1))
                                            << (std::make_shared<Error>((YYSTYPE*) & $2, ErrorId::ERRID_EXPECTED_IDENTIFIER));
                                        yyclearin;
                                    }
    | PS_EXTERN   IDENTIFIER        {   auto l = std::make_shared<Label>((YYSTYPE*) & $2);
                                        $$ = AST_PUT(std::make_shared<Extern>((YYSTYPE*) & $1, *l));
                                    }
    | PS_EXTERN   error             {   $$ = AST_PUT(std::make_shared<Extern>((YYSTYPE*) & $1))
                                            << (std::make_shared<Error>((YYSTYPE*) & $2, ErrorId::ERRID_EXPECTED_IDENTIFIER));
                                        yyclearin;
                                    }
    | PS_STR      STRING_LITERAL    {   $$ = AST_PUT(std::make_shared<String>((YYSTYPE*) & $2, String::STR_DEFAULT));
                                        (std::make_shared<TextNode>((YYSTYPE*) & $1)); //Dispose of .str token
                                    }
    | PS_STR      error             {   $$ = AST_PUT(std::make_shared<String>((YYSTYPE*) & $1))
                                            << (std::make_shared<Error>((YYSTYPE*) & $2, ErrorId::ERRID_EXPECTED_STRING));
                                        yyclearin;
                                    }
    | PS_CSTR     STRING_LITERAL    {   $$ = AST_PUT(std::make_shared<String>((YYSTYPE*) & $2, String::STR_NULL_TERMINATED));
                                        (std::make_shared<TextNode>((YYSTYPE*) & $1)); //Dispose of .cstr token
                                    }
    | PS_CSTR     error             {   $$ = AST_PUT(std::make_shared<String>((YYSTYPE*) & $1))
                                            << (std::make_shared<Error>((YYSTYPE*) & $2, ErrorId::ERRID_EXPECTED_STRING));
                                        yyclearin;
                                    }
    | PS_DB       value_list        {   auto n = std::make_shared<WordArray>(8);
                                        $$ = AST_PUT(n);
                                        *n << AST_GET($2);
                                        (std::make_shared<TextNode>((YYSTYPE*) & $1)); //Dispose of .db token
                                    }
    | PS_DB       error             {   auto n = std::make_shared<WordArray>(8);
                                        $$ = AST_PUT(n);
                                        *n << (std::make_shared<Error>((YYSTYPE*) & $2, ErrorId::ERRID_EXPECTED_VALUE_LIST));
                                        (std::make_shared<TextNode>((YYSTYPE*) & $1)); //Dispose of .db token
                                        yyclearin;
                                    }
    | PS_DW       value_list        {   auto n = std::make_shared<WordArray>(16);
                                        $$ = AST_PUT(n);
                                        *n << AST_GET($2);
                                        (std::make_shared<TextNode>((YYSTYPE*) & $1)); //< Dispose of .dw token
                                    }
    | PS_DW       error             {   auto n = std::make_shared<WordArray>(16);
                                        $$ = AST_PUT(n);
                                        *n << (std::make_shared<Error>((YYSTYPE*) & $2, ErrorId::ERRID_EXPECTED_VALUE_LIST));
                                        (std::make_shared<TextNode>((YYSTYPE*) & $1)); //< Dispose of .dw token
                                        yyclearin;
                                    }
    /* .DW (n) explicitly takes a bit width parameter, assembler will round it up to the nearest byte. */
    /* .DW (8) is equivalent to .DB */
    | PS_DW '(' integer_value ')' value_list 
                                    {   //Get the () integer value, and dispose of the ast node all at once
                                        std::int32_t word_bits = std::dynamic_pointer_cast<AstValue>(AST_GET($3))->getValue();
                                        //Create the proper AST nodes
                                        auto n = std::make_shared<WordArray>(word_bits);
                                        $$ = AST_PUT(n);
                                        *n << AST_GET($5);
                                        (std::make_shared<TextNode>((YYSTYPE*) & $1)); //< Dispose of .dw token
                                    }
    | PS_DW '('   error             {   auto n = std::make_shared<WordArray>(8);
                                        $$ = AST_PUT(n);
                                        *n << (std::make_shared<Error>((YYSTYPE*) & $3, ErrorId::ERRID_EXPECTED_WORD_SIZE));
                                        (std::make_shared<TextNode>((YYSTYPE*) & $1)); //< Dispose of .dw token
                                        yyclearin;
                                    }
    | PS_DW '(' integer_value ')' error
                                   {    std::int32_t word_bits = std::dynamic_pointer_cast<AstValue>(AST_GET($3))->getValue();
                                        auto n = std::make_shared<WordArray>(word_bits);
                                        $$ = AST_PUT(n);
                                        *n << (std::make_shared<Error>((YYSTYPE*) & $5, ErrorId::ERRID_EXPECTED_VALUE_LIST));
                                        (std::make_shared<TextNode>((YYSTYPE*) & $1)); //< Dispose of .dw token
                                        yyclearin;
                                    }
    ;

%%
