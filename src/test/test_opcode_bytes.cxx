#include <as85.hxx>

#include <ast/ast.hxx>
#include <i8085.tab.h>

#include <ast/opcode.hxx>
#include <iostream>
#include <list>
#include <set>

#include <cstring>
#include <cassert>

using namespace as85::ast;

namespace as85::ast
{

//Friend class of Opcode, for testing internals.
class TestClass_Opcode
{
    public:
    /// @brief Checks opcodes for repeats.
    /// @return Error count.
    static int test_repeats();

    /// @brief Checks .db .dw for correctness.
    /// @return Error count.
    static int test_dw();
};

int TestClass_Opcode::test_repeats()
{
    int error_count = 0;

    //Check for spurious bits against argument mask
    {
        for(auto b : Opcode::opcode_byte){
            uint8_t masked_b = b.second & (Opcode::opcode_arg_mask.at(b.first));
            if(masked_b){
                error_count++;
                std::cerr 
                    << "FAIL: Found opcode byte with argument bits set: 0x"
                    << std::hex << (unsigned int)b.second
                    << std::endl
                    ;
            }
        }
    }
    if (error_count) return error_count;

    //Check for opcode byte duplicates (arguments masked out)
    {
        //Get all the opcode bytes from the token/byte map
        std::set<uint8_t> known_bytes;
        for(auto b : Opcode::opcode_byte){
            uint8_t masked_b = b.second & (0xFF ^ Opcode::opcode_arg_mask.at(b.first));
            if(known_bytes.find(masked_b) != known_bytes.cend()){
                error_count++; //Increment error count
                std::cerr
                    << "FAIL: Found repeating byte: 0x"
                    << std::hex << (unsigned int)b.second
                    << std::endl
                    ;
            }
            else{
                known_bytes.insert(masked_b);
            }
        }
    }
    return error_count;
}

struct dw_testcase {
    std::string name;
    std::string text;
    int bits;
    int format;
    std::vector<uint8_t> expected_bytes;
};

static const std::list<struct dw_testcase> dw_testcases = {
    //8bit -- .db / .dw(8)
    {".dw(8) - 1", "0xEF",       8, Word::WordFormat::FMT_LITTLE, {0xEF}},
    {".dw(8) - 2", "0xFE",       8, Word::WordFormat::FMT_LITTLE, {0xFE}},
    {".dw(8) - 3", "0xDEADBEEF", 8, Word::WordFormat::FMT_LITTLE, {0xEF}},
    {".dw(8) - 4", "0xBADCAFFE", 8, Word::WordFormat::FMT_LITTLE, {0xFE}},

    //16bit -- .dw / .dw(16)
    {".dw(16) - 1", "0xEF",       16, Word::WordFormat::FMT_LITTLE, {0xEF, 0x00}},
    {".dw(16) - 2", "0xFE",       16, Word::WordFormat::FMT_LITTLE, {0xFE, 0x00}},
    {".dw(16) - 3", "0xDEADBEEF", 16, Word::WordFormat::FMT_LITTLE, {0xEF, 0xBE}},
    {".dw(16) - 4", "0xBADCAFFE", 16, Word::WordFormat::FMT_LITTLE, {0xFE, 0xAF}},

    //24bit -- .dw(24)
    {".dw(24) - 1", "0xEF",       24, Word::WordFormat::FMT_LITTLE, {0xEF, 0x00, 0x00}},
    {".dw(24) - 2", "0xFE",       24, Word::WordFormat::FMT_LITTLE, {0xFE, 0x00, 0x00}},
    {".dw(24) - 3", "0xDEADBEEF", 24, Word::WordFormat::FMT_LITTLE, {0xEF, 0xBE, 0xAD}},
    {".dw(24) - 4", "0xBADCAFFE", 24, Word::WordFormat::FMT_LITTLE, {0xFE, 0xAF, 0xDC}},

    //32bit -- .dw(32)
    {".dw(32) - 1", "0xEF",       32, Word::WordFormat::FMT_LITTLE, {0xEF, 0x00, 0x00, 0x00}},
    {".dw(32) - 2", "0xFE",       32, Word::WordFormat::FMT_LITTLE, {0xFE, 0x00, 0x00, 0x00}},
    {".dw(32) - 3", "0xDEADBEEF", 32, Word::WordFormat::FMT_LITTLE, {0xEF, 0xBE, 0xAD, 0xDE}},
    {".dw(32) - 4", "0xBADCAFFE", 32, Word::WordFormat::FMT_LITTLE, {0xFE, 0xAF, 0xDC, 0xBA}},

    // Big endian tests, never implemented on the parser but must be tested anyway since the
    // code is there already, and I'm writing these test cases now.

    //8bit Big endian
    {".dwB(8) - 1", "0xE5",       8, Word::WordFormat::FMT_BIG, {0xE5}},
    {".dwB(8) - 2", "0xBE",       8, Word::WordFormat::FMT_BIG, {0xBE}},
    {".dwB(8) - 3", "0x2B00B1E5", 8, Word::WordFormat::FMT_BIG, {0xE5}},
    {".dwB(8) - 4", "0xCAFEBABE", 8, Word::WordFormat::FMT_BIG, {0xBE}},

    //16bit Big endian
    {".dwB(16) - 1", "0xE5",       16, Word::WordFormat::FMT_BIG, {0x00, 0xE5}},
    {".dwB(16) - 2", "0xBE",       16, Word::WordFormat::FMT_BIG, {0x00, 0xBE}},
    {".dwB(16) - 3", "0x2B00B1E5", 16, Word::WordFormat::FMT_BIG, {0xB1, 0xE5}},
    {".dwB(16) - 4", "0xCAFEBABE", 16, Word::WordFormat::FMT_BIG, {0xBA, 0xBE}},

    //24bit Big endian
    {".dwB(24) - 1", "0xE5",       24, Word::WordFormat::FMT_BIG, {0x00, 0x00, 0xE5}},
    {".dwB(24) - 2", "0xBE",       24, Word::WordFormat::FMT_BIG, {0x00, 0x00, 0xBE}},
    {".dwB(24) - 3", "0x2B00B1E5", 24, Word::WordFormat::FMT_BIG, {0x00, 0xB1, 0xE5}},
    {".dwB(24) - 4", "0xCAFEBABE", 24, Word::WordFormat::FMT_BIG, {0xFE, 0xBA, 0xBE}},

    //32bit Big endian
    {".dwB(32) - 1", "0xE5",       32, Word::WordFormat::FMT_BIG, {0x00, 0x00, 0x00, 0xE5}},
    {".dwB(32) - 2", "0xBE",       32, Word::WordFormat::FMT_BIG, {0x00, 0x00, 0x00, 0xBE}},
    {".dwB(32) - 3", "0x2B00B1E5", 32, Word::WordFormat::FMT_BIG, {0x2B, 0x00, 0xB1, 0xE5}},
    {".dwB(32) - 4", "0xCAFEBABE", 32, Word::WordFormat::FMT_BIG, {0xCA, 0xFE, 0xBA, 0xBE}},
};

int TestClass_Opcode::test_dw()
{
    int error_count = 0;
    //Check .dw-like AST
    for(const auto &testcase : dw_testcases){
        //Build the AST fragment
        YYSTYPE token = {HEXADECIMAL_CONSTANT,0,0,0,0};
        token.s.text = (const char*) malloc(testcase.text.length()+1);
        token.s.size = testcase.text.length();
        strcpy((char*)token.s.text, testcase.text.c_str());
        auto value = std::make_shared<Integer>(&token);
        auto word = std::make_shared<Word>(testcase.bits, testcase.format);
        word << value;

        //Check resulting size
        auto bytes = word->getBytes();
        if(bytes.size() != testcase.expected_bytes.size()){
            error_count++;
            std::cerr 
                << "Size missmatch for dw test <" << testcase.name << ">"
                << " Expected: " << testcase.expected_bytes.size()
                << " Found:" << bytes.size()
                << std::endl;
            continue;
        }

        //Check bytes
        std::cerr << std::hex;
        bool bytes_ok = true;
        for(size_t i = 0; i < bytes.size(); i++){
            if(bytes.at(i) != testcase.expected_bytes.at(i)){
                error_count++;
                bytes_ok = false;
                std::cerr << "0x" << (int) bytes.at(i) << " != 0x" << (int) testcase.expected_bytes.at(i) << std::endl;
            }
        }
        if(!bytes_ok){
            std::cerr << "Bytes missmatch for dw test <" << testcase.name << ">" << std::endl;
        }
        else{
            std::cerr << "Test <" << testcase.name << "> OK!" << std::endl;
        }
        std::cerr << std::dec;
    }
    return error_count;
}

}

int main( /*int argc, char** argv*/ ){
    int rv = 0;
    rv += TestClass_Opcode::test_repeats();
    rv += TestClass_Opcode::test_dw();

    if(rv){
        std::cerr << "--- TEST FINISHED WITH ERRORS ---" << std::endl;
    }
    else{
        std::cerr << "--- TEST OK ---" << std::endl;
    }
    return rv;
}
