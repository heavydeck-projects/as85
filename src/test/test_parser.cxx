/*
This program just opens a given assembler file, passes to the parser and
expects it to successfully parse it.
*/

#include <cstdio>
#include <iostream>
#include <stdpolyfills.hxx>

//Optparse plus implementation
#define OPTPARSE_API static
#define OPTPARSE_IMPLEMENTATION
#include <optparse.h>

#include <ast/ast.hxx>
#include <i8085.tab.h>
#include <i8085.yy.h>
#include <parser.hxx>

#include <as85.hxx>
#include <ast/list.hxx>
#include <ast/statement.hxx>
#include <ast/error.hxx>
#include <ast/warning.hxx>
#include <ast/visitor/visitor.hxx>
#include <ast/opcode.hxx>

//Class for collecting all the bytes
namespace as85{
namespace ast{

class AstBytesVisitor : public visitor::ConstVisitor{
    public:
    AstBytesVisitor() : ConstVisitor(), size(0) {}
    size_t getSize() {return this->size;}
    virtual void visit(const Ast& ast) {
        const auto ast_bytes = dynamic_cast<const as85::ast::Bytes*>(&ast);
        if(ast_bytes){
            auto node_bytes = ast_bytes->getBytes();
            this->size += node_bytes.size();
            this->all_bytes.insert(all_bytes.end(), node_bytes.begin(), node_bytes.end());
        }
        else{
            for(const auto & node : ast.getChildren()){
                node->accept(*this);
            }
        }
    }
    const std::vector<uint8_t>& getBytes() const{
        return this->all_bytes;
    }

    private:
    size_t size;
    std::vector<uint8_t> all_bytes;
};

}}

static void usage(){
    std::cerr << "Usage:" << std::endl << "test_parser [-e EXPECTED_ERR_COUNT | -a] <i8085_ASM_FILE> " <<std::endl;
}

//Command line defaults
static bool verbose = false;
static bool any_errors = false;
static bool handle_exceptions = true;
static std::string in_file;
static std::string out_dir;
static int errors_expected = 0;
static size_t expected_size = 0;

#define VERBOSE verbose

static int inner_main()
{
    //Set initial state
    i8085_ast_root = nullptr;

    //Get out directory
    STD::filesystem::path dot_dir;
    if(out_dir.length()) dot_dir = STD::filesystem::path(out_dir);
    else dot_dir = STD::filesystem::path(in_file).parent_path();
    dot_dir = STD::filesystem::absolute(dot_dir);
    std::string dot_name = STD::filesystem::path(in_file).stem().string();

    //Check dot directory exists, create if not.
    if(! STD::filesystem::exists(dot_dir)){
        STD::filesystem::create_directories(dot_dir);
    }

    //Try opening the file
    FILE* f = fopen(in_file.c_str(), "rb");
    if(f == nullptr){
        std::cerr << "Could not open file: <" << in_file << ">" << std::endl;
        return 1;
    }
    i8085_yyin = f;

    //Parse the file
    if(VERBOSE) std::cerr << "Parsing input file..." << std::flush;
    int rv = i8085_yyparse();
    if(VERBOSE) std::cerr << " done!" << std::endl;

    if(rv){
        std::cerr << "Parsing failed." << std::endl;
    }

    //Copy the root pointer from parser, and invalidate the original
    std::shared_ptr<as85::ast::Ast> ast_root = i8085_ast_root;
    i8085_ast_root.reset();
    if (ast_root == nullptr)
    {
        std::cerr << "AST root node is NULL!" << std::endl;
        return 1;
    }

    //Set a weak ref to the node itself on all of the AST
    //Then set all the nodes parent pointer
    {
        as85::ast::visitor::AstParentSetter visitor_parent;
        
        if(VERBOSE) std::cerr << "Building AST node parent references..." << std::flush;
        ast_root->accept(visitor_parent);
        if(VERBOSE) std::cerr << " done!" << std::endl;
    }

    //Dump the parse tree (Enable if stuff breaks)
    if(false && VERBOSE){
        if(VERBOSE) std::cerr << "Dumping parse tree..." << std::flush;
        as85::ast::visitor::AstGraphviz v;
        v.setNegative(false);
        v.enablePosition(true);
        ast_root->accept(v);
        v.writeGraphviz( (dot_dir / (dot_name + ".parse.dot")).string() );
        if(VERBOSE) std::cerr << " done!" << std::endl;
    }

    //Apply some basic visitors
    //  - Integrity check
    {
        if(VERBOSE) std::cerr << "Integrity check..." << std::flush;
        as85::ast::visitor::AstIntegrity visitor;
        ast_root->accept(visitor);
        if(!visitor.isOk()){
            std::cerr << "Ast references are inconsistent." << std::endl;
            return 1;
        }
        if(VERBOSE) std::cerr << " done!" << std::endl;
    }

    //  - Collect errors
    int error_count = 0;
    {
        if(VERBOSE) std::cerr << "Collect errors..." << std::flush;
        as85::ast::visitor::CollectNodes<as85::ast::Error> collect(false);
        ast_root->accept(collect);
        //Print warnings
        for(const auto &node : collect.getNodes()){
            error_count++;
            std::cerr << node->toString() << std::endl;
        }
        if(VERBOSE) std::cerr << " done! " << error_count << " errors." << std::endl;
    }
    //  - Collect warnings
    {
        if(VERBOSE) std::cerr << "Collect warnings..." << std::flush;
        as85::ast::visitor::CollectNodes<as85::ast::Warning> collect(false);
        ast_root->accept(collect);
        //Print warnings
        for(const auto &node : collect.getNodes()){
            std::cerr << node->toString() << std::endl;
        }
        if(VERBOSE) std::cerr << " done!" << std::endl;
    }
    //  - Flatten lists
    {
        if(VERBOSE) std::cerr << "Flatten..." << std::flush;
        as85::ast::visitor::ListFlattener flattener;
        ast_root->accept(flattener);
        if(VERBOSE) std::cerr << " done!" << std::endl;
    }
    //  - Remove empty statements
    {
        if(VERBOSE) std::cerr << "Remove empty statements..." << std::flush;
        as85::ast::visitor::EmptyStatementCleaner empty_cleaner;
        ast_root->accept(empty_cleaner);
        if(VERBOSE) std::cerr << " done!" << std::endl;
    }

    //Integrity check before dumping.
    {
        if(VERBOSE) std::cerr << "Integrity check..." << std::flush;
        as85::ast::visitor::AstIntegrity visitor;
        ast_root->accept(visitor);
        if(!visitor.isOk()){
            std::cerr << "Ast references are inconsistent." << std::endl;
            return 1;
        }
        if(VERBOSE) std::cerr << " done!" << std::endl;
    }

    //Dump the AST graph
    {
        if(VERBOSE) std::cerr << "Dumping final tree..." << std::flush;
        as85::ast::visitor::AstGraphviz v;
        v.setNegative(false);
        v.enablePosition(true);
        ast_root->accept(v);
        v.writeGraphviz( (dot_dir / (dot_name + ".dot")).string() );
        if(VERBOSE) std::cerr << " done!" << std::endl;
    }

    //Print error count if errors found or expected
    if(error_count || errors_expected){
        std::cerr 
            << "Errors: " << std::to_string(error_count)
            << "; Expected: " << (any_errors ? "Any (errors >= 1)" : std::to_string(errors_expected))
        << std::endl;
    }

    //IF error count/expected missmatch, return error
    //unless "any errors" is set
    if((errors_expected != error_count) && (!any_errors)){
        rv = 1;
    }
    //If no errors, but any is set, return fail
    if(any_errors && (error_count == 0)){
        rv = 1;
    }

    //If errors have happened
    //free up the parser memory pool (don't make it an error)
    if(error_count && i8085_parser_nodes.size()){
        std::cerr 
            << i8085_parser_nodes.size()
            << " nodes freed up from parser memory pool"
            << std::endl
        ;
        i8085_parser_nodes.clear();
    }
    //If errors not expected but memory pool is not empty
    //Parser is leaking memory; Fail test.
    else if(i8085_parser_nodes.size()){
        std::cerr 
            << "Error! parser memory leak; "
            << i8085_parser_nodes.size()
            << " nodes freed up from parser memory pool"
            << std::endl
        ;
        i8085_parser_nodes.clear();
        rv = 1;
    }

    // If  we still have a root node, get all the bytes and if a size s given,
    // check it matches. All in all, it should spew bytes without crashing.
    if(ast_root && (error_count == 0)){
        if(VERBOSE) std::cerr << "Extracting bytes..." << std::flush;
        as85::ast::AstBytesVisitor visitor;
        ast_root->accept(visitor);
        if(expected_size &&  (visitor.getSize() != expected_size)){
            std::cerr 
                << "Error! assembled size missmatch for <" << in_file
                << "> Expected:" << expected_size << " Got:" << visitor.getSize()
                << std::endl;
            rv = 1;
        }
        else if(VERBOSE){
            //Otherwise, if verbose, dump all the bytes to console
            int count = 0;
            std::cerr << std::hex << std::endl;
            for(const auto &b : visitor.getBytes()){
                count++;
                if (b < 0x10) std::cerr << "0";
                std::cerr << (unsigned int)b;
                if(count % 8)
                    std::cerr << " ";
                else
                    std::cerr << std::endl;
            }
            if(count % 8) std::cerr << std::dec << std::endl;
        }
        if(VERBOSE) std::cerr << " done!" << std::endl;
    }

    fclose(f);
    return rv;
}

int main(int argc, char** argv)
{
    // --- Parse CLI ---
    struct optparse_long longopts[] = {
        //Arguments with short + long options
        {"help", 'h', OPTPARSE_NONE},
        {"verbose", 'v', OPTPARSE_NONE},
        {"expected-errors", 'e', OPTPARSE_REQUIRED},
        {"expected-size", 's', OPTPARSE_REQUIRED},
        {"any-errors", 'a', OPTPARSE_NONE},
        {"out-directory", 'd', OPTPARSE_REQUIRED},
        {"no-handle-exceptions", 'x', OPTPARSE_NONE},

        //End of arguments
        {nullptr,  '\0', OPTPARSE_NONE},
    };
    struct optparse options;
    optparse_init(&options, argv);
    int option;
    while ((option = optparse_long(&options, longopts, NULL)) != -1) {
        switch (option){
            //verbose
            case 'v':
                verbose = true;
                break;
            case 'a':
                any_errors = true;
                break;
            case 'e':
                errors_expected = std::stoi(options.optarg);
                break;
            case 's':
                expected_size = std::stoi(options.optarg);
                break;
            case 'd':
                out_dir = std::string(options.optarg);
                break;
            case 'x':
                handle_exceptions = false;
                break;
            case 'h':
                usage();
                return 1;
            //Bad option
            case '?':
                std::cerr << argv[0] << "--" << options.errmsg << std::endl;
                return 1;
        }
    }
    //Parse remaining (unnamed) arguments as input files.
    //Just keep the last one.
    char* arg;
    while ((arg = optparse_arg(&options))){
        in_file = (std::string(arg));
    }

    // --- Call tests ---
    if(handle_exceptions){
        try{
            return inner_main();
        }
        catch(std::exception &e){
            //Print exception, then return with error.
            std::cerr << "-------------------------------------------------------------------------------" << std::endl;
            std::cerr << e.what() << std::endl;
            std::cerr << "Arguments:" << std::endl;
            for(int i = 0; i < argc; i++){
                std::cerr << "  > " << argv[i] << std::endl;
            }
            std::cerr << "-------------------------------------------------------------------------------" << std::endl;
            std::cerr << "---              Unhandled exception reached main() - ABORTING              ---" << std::endl;
            std::cerr << "-------------------------------------------------------------------------------" << std::endl;
            return 1;
        }
    }
    else{
        return inner_main();
    }
    
}
