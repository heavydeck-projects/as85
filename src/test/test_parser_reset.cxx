#include <iostream>
#include <vector>
#include <string>
#include <algorithm>
#include <random>

#include <ast/ast.hxx>
#include <parser.hxx>

static bool parse_file(const std::string &path)
{
    bool rv = true;
    std::cerr << "Parsing " << path << "... " << std::flush;

    //Try opening the file
    FILE* f = fopen(path.c_str(), "rb");
    if(f == nullptr){
        std::cerr << "Could not open file: " << path << std::endl;
        return false;
    }
    //i8085_yyin = f;
    i8085_parser_reset(f);

    //Parse the file
    int parser_fail = i8085_yyparse();
    fclose(f);

    if(parser_fail){
        std::cerr << "Parsing failed." << std::endl;
    }

    //Root must be a valid reference, memory must have no leaks
    if(!i8085_ast_root){
        rv = false;
        std::cerr << "No root node" << std::endl;
    }
    if(i8085_parser_nodes.size()){
        rv = false;
        std::cerr << "Parser leaked memory" << std::endl;
    }

    std::cerr << (rv ? "Done" : "FAILED!") << std::endl << std::flush;
    return rv;
}

int main(int argc, char** argv){
    //Test will try to parse all the given files on argv.
    //Files shall parse successfuly.
    std::cerr << "Parsing " << argc-1 << " files" << std::endl;
    //Put all the input files on a list
    std::vector<std::string> paths;
    for(int i = 1; i < argc; i++){
        paths.push_back(std::string(argv[i]));
    }

    //This shall remain true for the test to succeed
    int error_count = 0;

    //Test the files in the given order
    for(auto &path : paths){
        if(!parse_file(path)) error_count++;
    }

    //Test the files again in random order
    std::shuffle(paths.begin(), paths.end(), std::random_device());
    for(auto &path : paths){
        if(!parse_file(path)) error_count++;
    }
    return error_count;
}