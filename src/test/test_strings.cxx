#include <as85.hxx>

#include <ast/ast.hxx>
#include <i8085.tab.h>

#include <ast/bytes.hxx>
#include <iostream>
#include <list>
#include <set>

#include <cstring>
#include <cassert>

using namespace as85::ast;

namespace as85::ast
{

struct str_testcase {
    std::string name;
    std::string str;
    as85::ast::String::StringType format;
    std::vector<uint8_t> expected_bytes;
};

static const std::list<struct str_testcase> str_testcases = {
    //Simple strings
    {"Simple string", "Hello World!", String::STR_DEFAULT,         {0x48, 0x65, 0x6c, 0x6c, 0x6f, 0x20, 0x57, 0x6f, 0x72, 0x6c, 0x64, 0x21}},
    {"C String",      "Hello World!", String::STR_NULL_TERMINATED, {0x48, 0x65, 0x6c, 0x6c, 0x6f, 0x20, 0x57, 0x6f, 0x72, 0x6c, 0x64, 0x21, 0x00}},
    //Escape sequences
    {"Simple escape", "\\'\\\"\\a\\b\\f\\n\\r\\t\\v\\\\\\0", String::STR_DEFAULT, {0x27, 0x22, 0x07, 0x08, 0x0c, 0x0a, 0x0d, 0x09, 0x0b, 0x5c, 0x00}},
    {"Octal escape",  "\\000\\125\\252\\377",                String::STR_DEFAULT, {0x00, 0x55, 0xAA, 0xFF}},
    {"Hex escape",    "\\x00\\x55\\xAA\\xFF",                String::STR_DEFAULT, {0x00, 0x55, 0xAA, 0xFF}},
    //All together
    {"Mixed", "X\\t\\x0a\\x0E\\125X", String::STR_DEFAULT, {0x58, 0x09, 0x0a, 0x0E, 0x55, 0x58}},
    //Unicode!
    //These test cases require the C++ compiler not to choke on UTF8 source code, or at least be unaware UTF8 exists.
    {"UTF-8 string", "Ãèîöú", String::STR_DEFAULT, {0xc3, 0x83, 0xc3, 0xa8, 0xc3, 0xae, 0xc3, 0xb6, 0xc3, 0xba}},
    {"Naughtycode!", "🍆🍆", String::STR_DEFAULT, {0xf0, 0x9f, 0x8d, 0x86, 0xf0, 0x9f, 0x8d, 0x86}},
};

}

int main( /*int argc, char** argv*/ ){
    int err_count = 0;
    for(const auto &testcase : str_testcases){
        //Build the AST fragment
        YYSTYPE token = {HEXADECIMAL_CONSTANT,0,0,0,0};
        token.s.text = (const char*) malloc(testcase.str.length()+1);
        token.s.size = testcase.str.length();
        strcpy((char*)token.s.text, testcase.str.c_str());
        String ast_str(&token, testcase.format);

        //Check resulting size
        auto bytes = ast_str.getBytes();
        if(bytes.size() != testcase.expected_bytes.size()){
            err_count++;
            std::cerr 
                << "Size missmatch for test <" << testcase.name << ">"
                << " Expected: " << testcase.expected_bytes.size()
                << " Found:" << bytes.size()
                << std::endl;
            continue;
        }

        //Check bytes
        std::cerr << std::hex;
        bool bytes_bad = false;
        for(size_t i = 0; i < bytes.size(); i++){
            if(bytes.at(i) != testcase.expected_bytes.at(i)){
                err_count++;
                bytes_bad = true;
                std::cerr << "0x" << (int) bytes.at(i) << " != 0x" << (int) testcase.expected_bytes.at(i) << std::endl;
            }
        }
        std::cerr << std::dec;

        if(bytes_bad){
            std::cerr << "Bytes missmatch for test test <" << testcase.name << ">" << std::endl;
        }
        else{
            std::cerr << "Test <" << testcase.name << "> OK!" << std::endl;
        }
    }

    if(err_count){
        std::cerr << "--- TEST FINISHED WITH ERRORS ---" << std::endl;
    }
    else{
        std::cerr << "--- TEST OK ---" << std::endl;
    }
    return err_count;
}
