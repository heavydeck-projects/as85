#include <info.hxx>

#include <ast/ast.hxx>
#include <parser.hxx>

#include <sstream>

namespace as85{

std::string version_as85(){
    std::stringstream ss;
    ss << "as85 v" 
        << AS85_VERSION_MAJOR << "." 
        << AS85_VERSION_MINOR << "."
        << AS85_VERSION_PATCH
        ;
    return ss.str();   
}

std::string version_yacc(){
    return i8085_parser_version();
}

}
