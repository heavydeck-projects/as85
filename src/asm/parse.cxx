/**
 * @file parse.cxx
 * @brief parsing methods from Assembler class.
*/

#include <as85.hxx>
#include <ast/ast.hxx>
#include <ast/visitor/visitor.hxx>
#include <ast/list.hxx>
#include <ast/statement.hxx>
#include <ast/error.hxx>
#include <ast/warning.hxx>

#include <asm/asm.hxx>
#include <asm/exception.hxx>

#include <parser.hxx>
#include <cstdio>

#include <iostream>
#include <mutex>

//Define parser externs here
std::shared_ptr<as85::ast::Ast> i8085_ast_root;

namespace as85{
namespace assembler {

/// @brief Parser mutex.
///
/// Because of the way I'm using BYACC/FLEX, parsing is not really thread safe
static std::mutex parser_mutex;

void Assembler::parse(){
    FILE* in_f = fopen(this->src_path.c_str(), "rb");
    if(!in_f){
        throw FileNotFoundException(this->src_path.string(), __FILE__, __LINE__);
    }

    // --- Begin parser critical section ---
    trace() << ">> Parser critical section Lock" << std::endl;
    parser_mutex.lock();
    // This code is noth thread safe so we use a mutex to at least make
    // independent class instances at least mutually thread safe
    try{
        i8085_parser_reset(in_f);
        i8085_yyparse();
        fclose(in_f);
    }
    catch(std::exception &e){
        //Release used resources in case of error
        fclose(in_f);
        parser_mutex.unlock();
        trace() << "<< Parser critical section Unlock; Exitted With exception!" << std::endl;
        throw e;
    }
    //Copy AST from parser
    this->ast_root = i8085_ast_root;
    //Free up the parser AST we just copied
    i8085_ast_root.reset();

    // --- End parser critical section ---
    parser_mutex.unlock();
    trace() << "<< Parser critical section Unlock" << std::endl;

    //If parser root is null, fail parsing
    if(!this->ast_root){
        throw AssemblerException(
            "Failed to parse input file " + 
            this->src_path.relative_path().string(),
            __FILE__, __LINE__);
    }

    //Make the tree internal references valid, namely:
    // - Weak parent reference
    {
        as85::ast::visitor::AstParentSetter visitor;
        this->ast_root->accept(visitor);
    }

    //Collect the warnings and errors
    {
        as85::ast::visitor::CollectNodes<as85::ast::Error> collect_errors;
        as85::ast::visitor::CollectNodes<as85::ast::Warning> collect_warnings;
        collect_errors.setExtract().setShallow();
        collect_warnings.setExtract().setShallow();
        this->ast_root->accept(collect_errors);
        this->ast_root->accept(collect_warnings);
        
        for(const auto &node : collect_warnings.getNodes()){
            this->annotations.push_back(node);
        }
        for(const auto &node : collect_errors.getNodes()){
            this->annotations.push_back(node);
        }
        this->logAnnotations();
    }

    // - Flatten lists
    {
        as85::ast::visitor::ListFlattener visitor;
        this->ast_root->accept(visitor);
    }
    // - Remove empty statements
    {
        as85::ast::visitor::EmptyStatementCleaner visitor;
        this->ast_root->accept(visitor);
    }
    // - Integrity check
    if(Config::debug){
        this->astIntegrityCheck(__FILE__, __LINE__);
    }
}

}//assembler
}//as85
