#include <asm/exception.hxx>

namespace as85{
namespace assembler{

// --- Exceptions ---

AssemblerException::AssemblerException(const std::string &message, const std::string &file, int line) : As85Exception(message, file, line)
{ }

FileNotFoundException::FileNotFoundException(const std::string &path, const std::string &file, int line) : AssemblerException(path, file, line)
{
    this->message = path + " not found";
}

} //assembler
} //as85
