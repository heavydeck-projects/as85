#include <asm/asm.hxx>
#include <asm/exception.hxx>
#include <ast/ast.hxx>
#include <ast/visitor/visitor.hxx>
#include <ast/error.hxx>
#include <ast/warning.hxx>

#include <i8085.tab.h>

#include <functional>
#include <algorithm>
#include <iostream>

namespace as85{
namespace assembler {

Assembler::Assembler(std::string src){
    this->src_path = STD::filesystem::path(src);
    this->src_path = STD::filesystem::absolute(this->src_path);
}

void Assembler::logAnnotations(){
    //Sort the annotations collection by their position
    //within the source file.
    std::sort(
            this->annotations.begin(),
            this->annotations.end(),
            [](const std::shared_ptr<const ast::Ast> &l, const std::shared_ptr<const ast::Ast> &r)
            {
                return l->getPosition() < r->getPosition();
            }
    );
    //For each annotation, print it to the correct log stream
    for(const auto &a : this->annotations){
        if(dynamic_cast<const ast::Error*>(a.get()))
            { as85::critical() << a->toString() << std::endl; }
        else if(dynamic_cast<const ast::Warning*>(a.get()))
            { as85::warning()  << a->toString() << std::endl; }
        else
            { as85::trace()    << a->toString() << std::endl; }
    }
    //Clear the annotations
    this->annotations.clear();
}

void Assembler::astIntegrityCheck(const std::string &file, int line) const{
    as85::ast::visitor::AstIntegrity visitor;
    this->ast_root->accept(visitor);
    if(!visitor.isOk())
        throw AssemblerException("AST integrity check failed", file, line);
}

}//assembler
}//as85
