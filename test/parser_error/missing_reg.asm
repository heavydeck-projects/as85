; These opcodes are missing an 8-bit register argument / MEM.
; All the opcodes MUST generate a missing 8bit reg Error node.
MOV a_label, A
MOV B, other_label

MVI HL, 0xAAAA

; These opcodes are missing a 16-bit register argument.
; All the opcodes MUST generate a missing 16bit reg Error node.
; Registers are: BC DE HL or SP
LDAX AF
STAX C
PUSH SP
POP E
LXI L
