; .db does not take () arguments
.db (8) 0xDE, 0xBA

;Some errors at different places
.dw 0xDEADBEEF, 0xBADCAFFE (7)
.dw (HL) 0xDEADBEEF, 0xBADCAFFE
.dw (16)
.dw (24, 0xDEADBEEF, 0xBADCAFFE)
.dw () 0xDEADBEEF, 0xBADCAFFE
