; This file should be syntatical hell >:3c
; Make as many syntax errors as possible

; Bad opcode
BAD_OP

;two labels with one ':'
one_label another_label:

; Extern without label
.extern
.extern NOP
.extern 0

; Align without constant
.align
.align NOP

; Section without section name
.section text
.section NOP
.section 1234

; Declare words/bytes with bad arguments
.dw
.dw 1,2,3,4,NOP
.db
.db 1,2,3,4,NOP

; Bad strings
.cstr
.cstr NOP
.cstr 1,2,3,4,5,6
.str
.str NOP
.str 1,2,3,4,5,6

; Org without constant
.org
.org NOP

; All opcodes that can generate errors, with errors
    OUT
    IN

    PUSH
    POP

    STAX
    LDAX
    SHLD
    LHLD
    STA
    LDA

    LXI 0, 0x5555
    MVI D, HL

    MOV E, 
    MOV [HL] H
    MOV 0, L

    DAD C
    DCX A
    INX L

    CPI
    ORI
    XRI
    ANI
    SBI
    SUI
    ACI
    ADI

    CMP
    ORA
    XRA
    ANA
    SBB
    SUB
    ADD
    ADC
    DCR
    INR

    RST
    CPO
    CPE
    CM
    CP
    CNC
    CC
    CNZ
    CZ
    CALL

    JPO
    JPE
    JM
    JP 
    JNC
    JC 
    JNZ
    JZ 
    JMP
