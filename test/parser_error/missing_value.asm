; These opcodes are missing an immediate value or a label/identifier.
; All the opcodes MUST generate a missing value rror node.
.section ".text"
LDA A
STA BC
LHLD
SHLD SP
MVI A, B
LXI BC, DE
