; This asm file should assemble but most opcodes and
; directives trigger some sort of warning.

.section ".text"

;Address out of range (>64K)
.org 0x80000

;While this can be constructed, the resulting opcode is `HLT` not `MOV`
MOV MEM, MEM
