;This test has an opcode at the end which is missing a
;trailing newline. This has caused segfaults so a test
;has been made to test for it. Assemble may fail but
;Should not under any case trigger a SEGFAULT.

.section ".text"
;Intentionally missing newline at end.
NOP