;Declare and export:
;   * A byte array
;   * A word array
;   * A string
;   * A C-String

.section ".text"

;Export declarations
.export a_byte_array
.export a_word_array
.export a_string
.export a_c_string
.export segment_end

;Definitions
a_byte_array:                 ;<-- Segment-address shoud be 0x0000
.db 0xAA, 127, 02, 0b01010101

a_word_array:                 ;<-- Segment-address shoud be 0x0004
.dw 0x55AA, 0x8000, 0x0001

a_string:     ;<-- Segment-address shoud be 0x000A
.str "Hello!"

a_c_string:   ;<-- Segment-address shoud be 0x0010
.str "World!"

segment_end:  ;<-- Segment-address shoud be 0x0017
