; A simple program with an absolute-located symbol

;Exported symbols
.export main

;The following opcodes will be located
;at absolute addresses.
.section ".text"

;i8085 Entry point
.org 0x0000
JMP main

;Somewhere else, far from RST vectors.
.org 0x0100

hello_str:
.str "Hello world\n"
.db  0x00

main:
    ;Simple, one-byte opcodes
    NOP
    DI
    EI
    HLT
    SIM
    RIM
    ;Some sort of 'syscall'
    LXI HL, hello_str
    RST 1
end_loop:
    JMP end_loop
