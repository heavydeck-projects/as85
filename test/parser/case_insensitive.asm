;Similar to all-opcodes but mixing case on all opcodes
;Program is nonsense, but syntatically and semantically correct.

.seCtion ".rodata"
some_bytes:
.dB 0x00, 0x01, 0x02, 0x03

some_words:
.Dw 0x00, 0x01, 0x02, 0x03

some_string:
.sTr "Hello world!"
.dB 0x00

some_c_string:
.cstR "Hello again!"

.expOrt main

.exTern puts

.seCtion ".abs"
.org 0x5555
NOP

.sEction ".text"
main:
    ; --------------------------
    ; --- Every known opcode ---
    ; ---  Nonsense program  ---
    ; --------------------------
    .align 128
    NOp
    RiM
    sIM
    HLt
    Ei
    dI

    OuT 0x55
    iN 0x55

    PUsH AF
    PoP  AF

    sPHL
    XTHl
    XChG

    StAX BC
    lDAX DE
    SHLd 0X5555
    LHlD 0x5555
    StA  0X5555
    lDA  0x5555

    LXi BC, 0x5555
    MvI D, 0x55

    mOV E, MeM
    MOv (hl), H
    MoV A, L

    dAD BC
    DCx DE
    InX HL

    dAA
    CMa

    RaR
    rAL
    RRc
    RlC

    cPI 0x55
    ORi 0b01010101 ; <-- 0x55 in decimal
    XrI 0125       ; <-- 0x55 in octal
    aNI 85         ; <-- 0x55 in decimal
    SBi 0x55
    SuI 0x55
    aCI 0x55
    ADi 0B01010101

    CmP c
    oRA d
    XRa H
    AnA e
    sBB h
    SUb l
    AdD MEM  ; <-- i8085-style indirect memory "register".
    aDC (hL) ; <-- z80-style indirect memory "register".
    DCr a
    InR b

    PcHL
    rST 4
    CPo 0x5555
    CpE 0x5555
    cM  0x5555
    Cp  0x5555
    cNC 0x5555
    Cc  0x5555
    CnZ 0x5555
    Cz  0x5555
    CAlL 0x5555

    JpO 0x5555
    JPe 0x5555
    Jm  0x5555
    Jp  0x5555
    JnC 0x5555
    jC  0x5555
    JNz 0x5555
    Jz  0x5555
    JmP 0x5555

    ; --- Labeled statement ---
    mylabel: NoP

    ; --- Label-as-integer ---
    lXI Hl, some_string
