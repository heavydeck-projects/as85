;All i8085 opcodes and pseudo-ops crammed on the same place.
;Program is nonsense, but syntatically and semantically correct.

.section ".rodata"
some_bytes:
.db 0x00, 0x01, 0x02, 0x03

some_words:
.dw 0x00AA, 0x0155, 0x02AA, 0x0355

some_string:
.str "Hello world!"
.db 0x00

some_c_string:
.cstr "Hello again!"

.export main

.extern puts

.section ".abs"
.org 0x5555
NOP

.section ".text"
main:
    ; --------------------------
    ; --- Every known opcode ---
    ; ---  Nonsense program  ---
    ; --------------------------
    .align 128
    NOP
    RIM
    SIM
    HLT
    EI
    DI

    OUT 0x55
    IN 0x55

    PUSH AF
    POP  AF

    SPHL
    XTHL
    XCHG

    STAX BC
    LDAX DE
    SHLD 0x5555
    LHLD 0x5555
    STA  0x5555
    LDA  0x5555

    LXI BC, 0x5555
    MVI D, 0x55

    MOV E, MEM
    MOV (HL), H
    MOV A, L

    DAD BC
    DCX DE
    INX HL

    DAA
    CMA

    RAR
    RAL
    RRC
    RLC

    CPI 0x55
    ORI 0b01010101 ; <-- 0x55 in decimal
    XRI 0125       ; <-- 0x55 in octal
    ANI 85         ; <-- 0x55 in decimal
    SBI 0x55
    SUI 0x55
    ACI 0x55
    ADI 0x55

    CMP C
    ORA D
    XRA H
    ANA E
    SBB H
    SUB L
    ADD MEM  ; <-- i8085-style indirect memory "register".
    ADC (HL) ; <-- z80-style indirect memory "register".
    DCR A
    INR B

    PCHL
    RST 4
    CPO 0x5555
    CPE 0x5555
    CM  0x5555
    CP  0x5555
    CNC 0x5555
    CC  0x5555
    CNZ 0x5555
    CZ  0x5555
    CALL 0x5555

    JPO 0x5555
    JPE 0x5555
    JM  0x5555
    JP  0x5555
    JNC 0x5555
    JC  0x5555
    JNZ 0x5555
    JZ  0x5555
    JMP 0x5555

    ; --- Labeled statement ---
    mylabel: NOP

    ; --- Label-as-integer (16bit) ---
    LXI HL, some_string
    CPO mylabel
    CPE mylabel
    CM  mylabel
    CP  mylabel
    CNC mylabel
    CC  mylabel
    CNZ mylabel
    CZ  mylabel
    CALL mylabel

    JPO mylabel
    JPE mylabel
    JM  mylabel
    JP  mylabel
    JNC mylabel
    JC  mylabel
    JNZ mylabel
    JZ  mylabel
    JMP mylabel

    SHLD mylabel
    LHLD mylabel
    STA  mylabel
    LDA  mylabel

    LXI BC, mylabel

    ; --- Label-as-integer (8bit) ---
    IN  mylabel
    OUT mylabel

    CPI mylabel
    ORI mylabel
    XRI mylabel
    ANI mylabel
    SBI mylabel
    SUI mylabel
    ACI mylabel
    ADI mylabel

    ;MVI A, mylabel
