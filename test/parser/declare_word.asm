;A label for label-as-integer
hello:

;Declare bytes using .db
.db 0xDEADBEEF, 0xBADCAFFE       ; Will truncate and become EF FE (8LE/BE)

;Declare words using .dw 
.dw 0xDEADBEEF, 0xBADCAFFE       ; Will truncate and become EF BE FE AF (16LE)
.dw (8)  0xDEADBEEF, 0xBADCAFFE  ; Will truncate and become EF FE (8LE/BE)
.dw (16) 0xDEADBEEF, 0xBADCAFFE  ; Will truncate and become EF BE FE AF (16LE)
.dw (24) 0xDEADBEEF, 0xBADCAFFE  ; Will truncate and become EF BE AD FE AF DC (24LE)
.dw (32) 0xDEADBEEF, 0xBADCAFFE  ; Becomes EF BE AD DE FE AF DC BA (32LE)

;Declarations with labels as values
.dw 0xAAAA, hello, 0x5555
.dw (16) 0xAAAA, hello, 0x5555
