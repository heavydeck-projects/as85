;Code to detect wether we are running on a Z80 CPU
; or the i8085.

.export z80_detect
.section ".text"

z80_detect:
    MVI A, 0xFF ;
    ANA A       ; Clear Zero flag
    RIM     ;
    INX BC  ;<-- Interpreted as `JR NZ 0x0006` on a Z80
    DCX BC  ;<-- Restore BC (on i8085)
    ; Jump to i8085 path.
    JMP path_intel
    ; --- Z80 execution path ---
    ; The Z80 relative jump lands here
    ; A is 0xFF (true); Return
    RET

path_intel:
    ; --- i8085 execution path ---
    ; Clear A (false) and return.
    XRA A
    RET
