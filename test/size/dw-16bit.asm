;This file should output exactly 16 bytes despite truncation
;
;Bytes should be:
;  EF 00 EF BE EF BE EF BE    EF BE EF BE EF BE EF BE
.section ".text"
.dw 0xEF
.dw 0xBEEF
.dw 0xADBEEF
.dw 0xDEADBEEF
.dw (16) 0xFE
.dw (16) 0xAFFE
.dw (16) 0xDCAFFE
.dw (16) 0xBADCAFFE