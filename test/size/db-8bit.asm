;This file should output exactly 8 bytes despite truncation
;
;Bytes should be:
;  EF EF EF EF FE FE FE FE    .. .. .. .. .. .. .. ..
.section ".text"
.db 0xEF
.db 0xBEEF
.db 0xADBEEF
.db 0xDEADBEEF
.dw (8) 0xFE
.dw (8) 0xAFFE
.dw (8) 0xDCAFFE
.dw (8) 0xBADCAFFE
