;This file should output exactly 32 bytes.
;
;Bytes should be:
;  EF 00 00 00 EF BE 00 00    EF BE AD 00 EF BE AD DE
;  FE 00 00 00 FE AF 00 00    FE AF DC 00 FE AF DC BA
.section ".text"
.dw (32) 0xEF
.dw (32) 0xBEEF
.dw (32) 0xADBEEF
.dw (32) 0xDEADBEEF
.dw (32) 0xFE
.dw (32) 0xAFFE
.dw (32) 0xDCAFFE
.dw (32) 0xBADCAFFE
