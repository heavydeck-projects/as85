;This file should output exactly 24 bytes despite truncation
;
;Bytes should be:
;  EF 00 00 EF BE 00 EF BE    AD EF BE AD FE 00 00 FE
;  AF 00 FE AF DC FE AF DC    .. .. .. .. .. .. .. ..
.section ".text"
.dw (24) 0xEF
.dw (24) 0xBEEF
.dw (24) 0xADBEEF
.dw (24) 0xDEADBEEF
.dw (24) 0xFE
.dw (24) 0xAFFE
.dw (24) 0xDCAFFE
.dw (24) 0xBADCAFFE
