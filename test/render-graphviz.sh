#!/bin/sh
FORMAT="svg"

for DOT_FILE in `find -iname '*.dot'`
do
    OUT_FILE=`echo "$DOT_FILE" | sed 's/dot$/'"$FORMAT/"`
    echo "Rendering $DOT_FILE ..."
    dot -T"$FORMAT" -o"$OUT_FILE" "$DOT_FILE"
done
