#!/usr/bin/env pwsh
$format="svg"

#dot is generally _not_ available on PATH on windows; Add its default path.
#Yes, I do have and use powershell on Linux too.
if($IsWindows -or $ENV:windir)
{
    $ENV:PATH="${ENV:PATH};C:\Program Files\Graphviz\bin"
}

foreach ( $dotfile in $(get-childitem -recurse -filter '*.dot') )
{
    $dotfile = $dotfile.FullName
    Write-Output "Rendering ${dotfile} ..."
    $outfile = $dotfile.substring(0, $dotfile.Length - 3) + "${format}"
    dot -T"${format}" -o"${outfile}" "${dotfile}"
}
