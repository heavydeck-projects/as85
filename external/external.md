# External

This directory will contain external GIT repositories needed for building this
project. Running either of the commands below should populate the directory.

```
git pull --recurse-submodules
```

```
git submodule update --recursive
```

Revert submodules to a pristine status with this commands at repo root:

```
git submodule deinit -f .
git submodule update --init
```
